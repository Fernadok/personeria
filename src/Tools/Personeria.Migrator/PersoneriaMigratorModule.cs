using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using Personeria.EntityFramework;

namespace Personeria.Migrator
{
    [DependsOn(typeof(PersoneriaDataModule))]
    public class PersoneriaMigratorModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer<PersoneriaDbContext>(null);

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}