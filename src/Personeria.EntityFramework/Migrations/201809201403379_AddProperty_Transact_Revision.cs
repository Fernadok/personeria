namespace Personeria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProperty_Transact_Revision : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transacts", "Revision", c => c.Guid());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Transacts", "Revision");
        }
    }
}
