﻿using Personeria.EntityFramework;
using EntityFramework.DynamicFilters;

namespace Personeria.Migrations.SeedData
{
    public class InitialHostDbBuilder
    {
        private readonly PersoneriaDbContext _context;

        public InitialHostDbBuilder(PersoneriaDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            _context.DisableAllFilters();

            new DefaultEditionsCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();
        }
    }
}
