using System.Data.Entity.Migrations;
using System.Linq;
using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.MultiTenancy;
using Personeria.Authorization;
using Personeria.Authorization.Roles;
using Personeria.Authorization.Users;
using Personeria.Domain;
using Personeria.EntityFramework;

namespace Personeria.Migrations.SeedData
{
    public class InitialLocationDepartmentState
    {
        private readonly PersoneriaDbContext _context;

        public InitialLocationDepartmentState(PersoneriaDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            #region Provincias de Argentina
            var stateBuenosAires = new State() { Name = "Buenos Aires" };
            var stateCatamarca = new State() { Name = "Catamarca" };
            var stateChaco = new State() { Name = "Chaco" };
            var stateChubut = new State() { Name = "Chubut" };
            var stateCordoba = new State() { Name = "C�rdoba" };
            var stateCorrientes = new State() { Name = "Corrientes" };
            var stateEntreRios = new State() { Name = "Entre R�os" };
            var stateFormosa = new State() { Name = "Formosa" };
            var stateJujuy = new State() { Name = "Jujuy" };
            var stateLaPampa = new State() { Name = "La Pampa" };
            var stateLaRioja = new State() { Name = "La Rioja" };
            var stateMendoza = new State() { Name = "Mendoza" };
            var stateMisiones = new State() { Name = "Misiones" };
            var stateNeuquen = new State() { Name = "Neuqu�n" };
            var stateRioNegro = new State() { Name = "R�o Negro" };
            var stateSalta = new State() { Name = "Salta" };
            var stateSanJuan = new State() { Name = "San Juan" };
            var stateSanLuis = new State() { Name = "San Luis" };
            var stateSantaCruz = new State() { Name = "Santa Cruz" };
            var stateSantaFe = new State() { Name = "Santa Fe" };
            var stateSantiago = new State() { Name = "Santiago del Estero" };
            var stateTierraDelFuego = new State() { Name = "Tierra del Fuego" };
            var stateTucuman = new State() { Name = "Tucum�n" };
            _context.States.AddOrUpdate(x => x.Name, stateBuenosAires, stateCatamarca, stateChaco, stateChubut, stateCordoba, stateCorrientes, stateEntreRios, stateFormosa, stateJujuy, stateLaPampa, stateLaRioja, stateMendoza, stateMisiones, stateNeuquen, stateRioNegro, stateSalta, stateSanJuan, stateSanLuis, stateSantaCruz, stateSantaFe, stateSantiago, stateTierraDelFuego, stateTucuman);
            _context.SaveChanges();
            #endregion

            #region Departamentos de San Luis
            var department1 = new Department() { Name = "Ayacucho", StateId = stateSanLuis.Id };
            var department2 = new Department() { Name = "Belgrano", StateId = stateSanLuis.Id };
            var department3 = new Department() { Name = "Chacabuco", StateId = stateSanLuis.Id };
            var department4 = new Department() { Name = "Coronel Pringles", StateId = stateSanLuis.Id };
            var department5 = new Department() { Name = "General Pedernera", StateId = stateSanLuis.Id };
            var department6 = new Department() { Name = "Gobernador Dupuy", StateId = stateSanLuis.Id };
            var department7 = new Department() { Name = "Jun�n", StateId = stateSanLuis.Id };
            var department8 = new Department() { Name = "Juan Mart�n de Pueyrred�n", StateId = stateSanLuis.Id };
            var department9 = new Department() { Name = "Libertador General San Mart�n", StateId = stateSanLuis.Id };
            _context.Departments.AddOrUpdate(x => new { x.Name, x.StateId }, department1, department2, department3, department4, department5, department6, department7, department8, department9);
            _context.SaveChanges();
            #endregion

            #region Departamentos de Mendoza
            var department10 = new Department() { Name = "General Alvear", StateId = stateMendoza.Id };
            var department11 = new Department() { Name = "Godoy Cruz", StateId = stateMendoza.Id };
            var department12 = new Department() { Name = "Guaymall�n", StateId = stateMendoza.Id };
            var department13 = new Department() { Name = "Jun�n", StateId = stateMendoza.Id };
            var department14 = new Department() { Name = "La Paz", StateId = stateMendoza.Id };
            var department15 = new Department() { Name = "Las Heras", StateId = stateMendoza.Id };
            var department16 = new Department() { Name = "Lavalle", StateId = stateMendoza.Id };
            var department17 = new Department() { Name = "Luj�n de Cuyo", StateId = stateMendoza.Id };
            var department18 = new Department() { Name = "Maip�", StateId = stateMendoza.Id };
            var department19 = new Department() { Name = "Malargue", StateId = stateMendoza.Id };
            var department20 = new Department() { Name = "Capital", StateId = stateMendoza.Id };
            var department21 = new Department() { Name = "Rivadavia", StateId = stateMendoza.Id };
            var department22 = new Department() { Name = "San Carlos", StateId = stateMendoza.Id };
            var department23 = new Department() { Name = "San Mart�n", StateId = stateMendoza.Id };
            var department24 = new Department() { Name = "San Rafael", StateId = stateMendoza.Id };
            var department25 = new Department() { Name = "Santa Rosa", StateId = stateMendoza.Id };
            var department26 = new Department() { Name = "Tunuy�n", StateId = stateMendoza.Id };
            var department27 = new Department() { Name = "Tupungato", StateId = stateMendoza.Id };
            _context.Departments.AddOrUpdate(x => new { x.Name, x.StateId }, department10,
                department11, department12, department13, department14,
                department15, department16, department17, department18,
                department19, department20, department21, department22,
                department23, department24, department25, department26,
                department27);
            _context.SaveChanges();
            #endregion

            #region Departamentos de Buenos Aires
            var department28 = new Department() { Name = "Azul", StateId = stateBuenosAires.Id };
            var department29 = new Department() { Name = "Bah�a Blanca", StateId = stateBuenosAires.Id };
            var department30 = new Department() { Name = "Dolores", StateId = stateBuenosAires.Id };
            var department31 = new Department() { Name = "Jun�n", StateId = stateBuenosAires.Id };
            var department32 = new Department() { Name = "La Matanza", StateId = stateBuenosAires.Id };
            var department33 = new Department() { Name = "La Plata", StateId = stateBuenosAires.Id };
            var department34 = new Department() { Name = "Lomas de Zamora", StateId = stateBuenosAires.Id };
            var department35 = new Department() { Name = "Mar del Plata", StateId = stateBuenosAires.Id };
            var department36 = new Department() { Name = "Mercedes", StateId = stateBuenosAires.Id };
            var department37 = new Department() { Name = "Mor�n", StateId = stateBuenosAires.Id };
            var department38 = new Department() { Name = "Necochea", StateId = stateBuenosAires.Id };
            var department39 = new Department() { Name = "Pergamino", StateId = stateBuenosAires.Id };
            var department40 = new Department() { Name = "Quilmes", StateId = stateBuenosAires.Id };
            var department41 = new Department() { Name = "San Isidro", StateId = stateBuenosAires.Id };
            var department42 = new Department() { Name = "San Martin", StateId = stateBuenosAires.Id };
            var department43 = new Department() { Name = "San Nicol�s", StateId = stateBuenosAires.Id };
            var department44 = new Department() { Name = "Trenque Lauquen", StateId = stateBuenosAires.Id };
            var department45 = new Department() { Name = "Zarate - Campana", StateId = stateBuenosAires.Id };
            _context.Departments.AddOrUpdate(x => new { x.Name, x.StateId }, department28, department29,
                department30, department31, department32, department33, department34,
                department35, department36, department37, department38, department39,
                department40, department41, department42, department43, department44,
                department45);
            _context.SaveChanges();
            #endregion

            #region Departamentos de Tierra del Fuego
            var department56 = new Department() { Name = "Ant�rtida Argentina", StateId = stateTierraDelFuego.Id };
            var department57 = new Department() { Name = "Islas del Atl�ntico Sur", StateId = stateTierraDelFuego.Id };
            var department58 = new Department() { Name = "R�o Grande", StateId = stateTierraDelFuego.Id };
            var department59 = new Department() { Name = "Ushuaia", StateId = stateTierraDelFuego.Id };
            _context.Departments.AddOrUpdate(x => new { x.Name, x.StateId }, department56,
                department57, department58, department59);
            _context.SaveChanges();
            #endregion

            #region Departamentos de Santa Fe
            var department60 = new Department() { Name = "Belgrano", StateId = stateSantaFe.Id };
            var department61 = new Department() { Name = "Caseros", StateId = stateSantaFe.Id };
            var department62 = new Department() { Name = "Castellanos", StateId = stateSantaFe.Id };
            var department63 = new Department() { Name = "Constituci�n", StateId = stateSantaFe.Id };
            var department64 = new Department() { Name = "Garay", StateId = stateSantaFe.Id };
            var department65 = new Department() { Name = "General L�pez", StateId = stateSantaFe.Id };
            var department66 = new Department() { Name = "General Obligado", StateId = stateSantaFe.Id };
            var department67 = new Department() { Name = "Iriondo", StateId = stateSantaFe.Id };
            var department68 = new Department() { Name = "La Capital", StateId = stateSantaFe.Id };
            var department69 = new Department() { Name = "Las Colonias", StateId = stateSantaFe.Id };
            var department70 = new Department() { Name = "Nueve de Julio", StateId = stateSantaFe.Id };
            var department71 = new Department() { Name = "Rosario", StateId = stateSantaFe.Id };
            var department72 = new Department() { Name = "San Crist�bal", StateId = stateSantaFe.Id };
            var department73 = new Department() { Name = "San Javier", StateId = stateSantaFe.Id };
            var department74 = new Department() { Name = "San Jer�nimo", StateId = stateSantaFe.Id };
            var department75 = new Department() { Name = "San Justo", StateId = stateSantaFe.Id };
            var department76 = new Department() { Name = "San Lorenzo", StateId = stateSantaFe.Id };
            var department77 = new Department() { Name = "San Mart�n", StateId = stateSantaFe.Id };
            var department78 = new Department() { Name = "Vera", StateId = stateSantaFe.Id };
            _context.Departments.AddOrUpdate(x => new { x.Name, x.StateId }, department60,
                department61, department62, department63, department64,
                department65, department66, department67, department68,
                department69, department70, department71, department72,
                department73, department74, department75, department76,
                department77, department78);
            _context.SaveChanges();
            #endregion

            #region Ciudades de San Luis
            var location1 = new Location() { PostalCode = "5713", Name = "Candelaria", DepartmentId = department1.Id };
            var location2 = new Location() { PostalCode = "5703", Name = "La Majada", DepartmentId = department1.Id };
            var location3 = new Location() { PostalCode = "5703", Name = "Leandro N. Alem", DepartmentId = department1.Id };
            var location4 = new Location() { PostalCode = "5709", Name = "Luj�n", DepartmentId = department1.Id };
            var location5 = new Location() { PostalCode = "5711", Name = "Quines", DepartmentId = department1.Id };
            var location6 = new Location() { PostalCode = "5705", Name = "R�o Juan Gomez", DepartmentId = department1.Id };
            var location7 = new Location() { PostalCode = "5705", Name = "San Francisco del Monte de Oro", DepartmentId = department1.Id };

            var location8 = new Location() { PostalCode = "5719", Name = "La Calera", DepartmentId = department2.Id };
            var location9 = new Location() { PostalCode = "5720", Name = "Nogol�", DepartmentId = department2.Id };
            var location10 = new Location() { PostalCode = "5703", Name = "Villa de la Quebrada", DepartmentId = department2.Id };
            var location11 = new Location() { PostalCode = "5703", Name = "Villa General Roca", DepartmentId = department2.Id };

            var location12 = new Location() { PostalCode = "5770", Name = "Concar�n", DepartmentId = department3.Id };
            var location13 = new Location() { PostalCode = "5883", Name = "Cortaderas", DepartmentId = department3.Id };
            var location14 = new Location() { PostalCode = "5759", Name = "Naschel", DepartmentId = department3.Id };
            var location15 = new Location() { PostalCode = "5883", Name = "Papagayos", DepartmentId = department3.Id };
            var location16 = new Location() { PostalCode = "6775", Name = "Renca", DepartmentId = department3.Id };
            var location17 = new Location() { PostalCode = "5773", Name = "San Pablo", DepartmentId = department3.Id };
            var location18 = new Location() { PostalCode = "5773", Name = "Tilisarao", DepartmentId = department3.Id };
            var location19 = new Location() { PostalCode = "5835", Name = "Villa del Carmen", DepartmentId = department3.Id };
            var location20 = new Location() { PostalCode = "5883", Name = "Villa Larca", DepartmentId = department3.Id };

            var location21 = new Location() { PostalCode = "5757", Name = "Carolina", DepartmentId = department4.Id };
            var location22 = new Location() { PostalCode = "5701", Name = "El Trapiche", DepartmentId = department4.Id };
            var location23 = new Location() { PostalCode = "5736", Name = "Fraga", DepartmentId = department4.Id };
            var location24 = new Location() { PostalCode = "5713", Name = "La Bajada", DepartmentId = department4.Id };
            var location25 = new Location() { PostalCode = "5701", Name = "La Florida", DepartmentId = department4.Id };
            var location26 = new Location() { PostalCode = "5750", Name = "La Toma", DepartmentId = department4.Id };
            var location27 = new Location() { PostalCode = "5757", Name = "Riocito", DepartmentId = department4.Id };
            var location28 = new Location() { PostalCode = "5751", Name = "Saladillo", DepartmentId = department4.Id };

            var location29 = new Location() { PostalCode = "5731", Name = "Juan Jorba", DepartmentId = department5.Id };
            var location30 = new Location() { PostalCode = "5735", Name = "Juan Llerena", DepartmentId = department5.Id };
            var location31 = new Location() { PostalCode = "5738", Name = "Justo Daract", DepartmentId = department5.Id };
            var location32 = new Location() { PostalCode = "5831", Name = "La Punilla", DepartmentId = department5.Id };
            var location33 = new Location() { PostalCode = "5730", Name = "Lavaisse", DepartmentId = department5.Id };
            var location34 = new Location() { PostalCode = "5731", Name = "San Jos� del Morro", DepartmentId = department5.Id };
            var location35 = new Location() { PostalCode = "5730", Name = "Villa Mercedes", DepartmentId = department5.Id };
            var location36 = new Location() { PostalCode = "5733", Name = "Villa Reynolds", DepartmentId = department5.Id };
            var location37 = new Location() { PostalCode = "5738", Name = "Villa Salles", DepartmentId = department5.Id };

            var location38 = new Location() { PostalCode = "6216", Name = "Anchorena", DepartmentId = department6.Id };
            var location39 = new Location() { PostalCode = "6389", Name = "Arizona", DepartmentId = department6.Id };
            var location40 = new Location() { PostalCode = "6216", Name = "Bagual", DepartmentId = department6.Id };
            var location41 = new Location() { PostalCode = "6279", Name = "Batavia", DepartmentId = department6.Id };
            var location42 = new Location() { PostalCode = "6277", Name = "Buena Esperanza", DepartmentId = department6.Id };
            var location43 = new Location() { PostalCode = "6279", Name = "Fort�n El Patria", DepartmentId = department6.Id };
            var location44 = new Location() { PostalCode = "6216", Name = "Fortuna", DepartmentId = department6.Id };
            var location45 = new Location() { PostalCode = "6216", Name = "La Maroma", DepartmentId = department6.Id };
            var location46 = new Location() { PostalCode = "6279", Name = "Mart�n de Loyola", DepartmentId = department6.Id };
            var location47 = new Location() { PostalCode = "6279", Name = "Nahuel Map�", DepartmentId = department6.Id };
            var location48 = new Location() { PostalCode = "6279", Name = "Navia", DepartmentId = department6.Id };
            var location49 = new Location() { PostalCode = "6216", Name = "Nueva Galia", DepartmentId = department6.Id };
            var location50 = new Location() { PostalCode = "6216", Name = "Uni�n", DepartmentId = department6.Id };

            var location51 = new Location() { PostalCode = "5883", Name = "Carpinter�a", DepartmentId = department7.Id };
            var location52 = new Location() { PostalCode = "5881", Name = "Cerro de Oro", DepartmentId = department7.Id };
            var location53 = new Location() { PostalCode = "5871", Name = "Lafinur", DepartmentId = department7.Id };
            var location54 = new Location() { PostalCode = "5871", Name = "Los Cajones", DepartmentId = department7.Id };
            var location55 = new Location() { PostalCode = "5883", Name = "Los Molles", DepartmentId = department7.Id };
            var location56 = new Location() { PostalCode = "5881", Name = "Merlo", DepartmentId = department7.Id };
            var location57 = new Location() { PostalCode = "5777", Name = "Santa Rosa del Conlara", DepartmentId = department7.Id };
            var location58 = new Location() { PostalCode = "5711", Name = "Talita", DepartmentId = department7.Id };

            var location59 = new Location() { PostalCode = "5721", Name = "Alto Pelado", DepartmentId = department8.Id };
            var location60 = new Location() { PostalCode = "5724", Name = "Alto Pencoso", DepartmentId = department8.Id };
            var location61 = new Location() { PostalCode = "5724", Name = "Balde", DepartmentId = department8.Id };
            var location62 = new Location() { PostalCode = "5721", Name = "Beazley", DepartmentId = department8.Id };
            var location63 = new Location() { PostalCode = "5721", Name = "Cazador", DepartmentId = department8.Id };
            var location64 = new Location() { PostalCode = "5724", Name = "Chosmes", DepartmentId = department8.Id };
            var location65 = new Location() { PostalCode = "5598", Name = "Desaguadero", DepartmentId = department8.Id };
            var location66 = new Location() { PostalCode = "5701", Name = "El Volc�n", DepartmentId = department8.Id };
            var location67 = new Location() { PostalCode = "5721", Name = "Jarilla", DepartmentId = department8.Id };
            var location68 = new Location() { PostalCode = "5701", Name = "Juana Koslay", DepartmentId = department8.Id };
            var location69 = new Location() { PostalCode = "5710", Name = "La Punta", DepartmentId = department8.Id };
            var location70 = new Location() { PostalCode = "5721", Name = "Mosmota", DepartmentId = department8.Id };
            var location71 = new Location() { PostalCode = "5701", Name = "Potrero de los Funes", DepartmentId = department8.Id };
            var location72 = new Location() { PostalCode = "5701", Name = "Salinas del Bebedero", DepartmentId = department8.Id };
            var location73 = new Location() { PostalCode = "5719", Name = "San Ger�nimo", DepartmentId = department8.Id };
            var location74 = new Location() { PostalCode = "5700", Name = "San Luis", DepartmentId = department8.Id };
            var location75 = new Location() { PostalCode = "5721", Name = "Zanjitas", DepartmentId = department8.Id };

            var location76 = new Location() { PostalCode = "5771", Name = "Las Aguadas", DepartmentId = department9.Id };
            var location77 = new Location() { PostalCode = "5753", Name = "Las Chacras", DepartmentId = department9.Id };
            var location78 = new Location() { PostalCode = "5773", Name = "Las Lagunas", DepartmentId = department9.Id };
            var location79 = new Location() { PostalCode = "5757", Name = "La Vertiente", DepartmentId = department9.Id };
            var location80 = new Location() { PostalCode = "5753", Name = "Paso Grande", DepartmentId = department9.Id };
            var location81 = new Location() { PostalCode = "5755", Name = "San Mart�n", DepartmentId = department9.Id };
            var location82 = new Location() { PostalCode = "5753", Name = "Villa de Praga", DepartmentId = department9.Id };




            _context.Locations.AddOrUpdate(x => new { x.Name, x.DepartmentId },
                location1, location2, location3, location4, location5, location6, location7, location8, location9, location10,
                location11, location12, location13, location14, location15, location16, location17, location18, location19, location20,
                location21, location22, location23, location24, location25, location26, location27, location28, location29, location30,
                location31, location32, location33, location34, location35, location36, location37, location38, location39, location40,
                location41, location42, location43, location44, location45, location46, location47, location48, location49, location50,
                location51, location52, location53, location54, location55, location56, location57, location58, location59, location60,
                location61, location62, location63, location64, location65, location66, location67, location68, location69, location70,
                location71, location72, location73, location74, location75, location76, location77, location78, location79, location80,
                location81, location82);
            _context.SaveChanges();
            #endregion

            #region Ciudades de Mendoza
            // -- General Alvear
            var location83 = new Location() { PostalCode = "5636", Name = "Canalejas", DepartmentId = department10.Id };
            var location84 = new Location() { PostalCode = "5621", Name = "Carmensa", DepartmentId = department10.Id };
            var location85 = new Location() { PostalCode = "5621", Name = "Cochic�", DepartmentId = department10.Id };
            var location86 = new Location() { PostalCode = "5637", Name = "Corral de Lorca", DepartmentId = department10.Id };
            var location87 = new Location() { PostalCode = "5621", Name = "El Ceibo", DepartmentId = department10.Id };
            var location88 = new Location() { PostalCode = "5620", Name = "El Juncalito", DepartmentId = department10.Id };
            var location89 = new Location() { PostalCode = "5609", Name = "Gaspar Campos", DepartmentId = department10.Id };
            var location90 = new Location() { PostalCode = "5609", Name = "Goico", DepartmentId = department10.Id };
            var location91 = new Location() { PostalCode = "5634", Name = "La Escandinava", DepartmentId = department10.Id };
            var location92 = new Location() { PostalCode = "5636", Name = "La Mora", DepartmentId = department10.Id };
            var location93 = new Location() { PostalCode = "5634", Name = "Los campamentos", DepartmentId = department10.Id };
            var location94 = new Location() { PostalCode = "5621", Name = "Los Compartos", DepartmentId = department10.Id };
            var location95 = new Location() { PostalCode = "5679", Name = "Media Luna", DepartmentId = department10.Id };
            var location96 = new Location() { PostalCode = "5637", Name = "Ovejer�a", DepartmentId = department10.Id };
            var location97 = new Location() { PostalCode = "5637", Name = "Pampa del Tigre", DepartmentId = department10.Id };
            var location98 = new Location() { PostalCode = "5620", Name = "El Nevado", DepartmentId = department10.Id };
            var location99 = new Location() { PostalCode = "5621", Name = "El Desv�o", DepartmentId = department10.Id };
            var location100 = new Location() { PostalCode = "5634", Name = "Bowen", DepartmentId = department10.Id };

            // -- Godoy Cruz
            var location101 = new Location() { PostalCode = "5544", Name = "Gobernador Benegas", DepartmentId = department11.Id };
            var location102 = new Location() { PostalCode = "5501", Name = "Godoy Cruz", DepartmentId = department11.Id };
            var location103 = new Location() { PostalCode = "5501", Name = "Las Tortugas", DepartmentId = department11.Id };
            var location104 = new Location() { PostalCode = "5501", Name = "Presidente Sarmiento", DepartmentId = department11.Id };
            var location105 = new Location() { PostalCode = "5503", Name = "San Francisco del Monte", DepartmentId = department11.Id };

            // -- Guamallen
            //var location106 = new Location() { PostalCode = "5523", Name = "Belgrano", DepartmentId = department12.Id };
            //var location106 = new Location() { PostalCode = "5523", Name = "El Bermejo", DepartmentId = department12.Id };
            //var location106 = new Location() { PostalCode = "5523", Name = "Buena Nueva", DepartmentId = department12.Id };
            //var location106 = new Location() { PostalCode = "5523", Name = "Capilla del Rosario", DepartmentId = department12.Id };
            //var location106 = new Location() { PostalCode = "5523", Name = "Belgrano", DepartmentId = department12.Id };
            //var location106 = new Location() { PostalCode = "5523", Name = "Belgrano", DepartmentId = department12.Id };
            //var location106 = new Location() { PostalCode = "5523", Name = "Belgrano", DepartmentId = department12.Id };

            // -- San Rafael
            var location112 = new Location() { PostalCode = "5600", Name = "San Rafael", DepartmentId = department24.Id };

            // -- Maipu department18
            var location118 = new Location() { PostalCode = "5511", Name = "General Guti�rrez", DepartmentId = department18.Id };

            _context.Locations.AddOrUpdate(x => new { x.Name, x.DepartmentId },
                location83, location84, location85, location86, location87, location88, location89,
                location90, location91, location92, location93, location94, location95, location96,
                location97, location98, location99, location100, location101, location102, location103,
                location104, location105, location112, location118);
            _context.SaveChanges();
            #endregion

            #region Ciudades de Buenos Aires
            var location108 = new Location() { PostalCode = "8000", Name = "Bah�a Blanca", DepartmentId = department29.Id };
            var location111 = new Location() { PostalCode = "1757", Name = "Gregorio de Laferrere", DepartmentId = department32.Id };


            _context.Locations.AddOrUpdate(x => new { x.Name, x.DepartmentId },
                location108, location111);
            _context.SaveChanges();
            #endregion

            #region Ciudades de Tierra del Fuego
            var location110 = new Location() { PostalCode = "9410", Name = "Ushuaia", DepartmentId = department59.Id };


            _context.Locations.AddOrUpdate(x => new { x.Name, x.DepartmentId },
                location110);
            _context.SaveChanges();
            #endregion

            #region Ciudades de Santa Fe
            var location119 = new Location() { PostalCode = "2000", Name = "Rosario", DepartmentId = department71.Id };


            _context.Locations.AddOrUpdate(x => new { x.Name, x.DepartmentId },
                location119);
            _context.SaveChanges();
            #endregion
        }


    }
}