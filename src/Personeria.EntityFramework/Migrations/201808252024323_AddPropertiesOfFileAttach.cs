namespace Personeria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPropertiesOfFileAttach : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FileAttaches", "Title", c => c.String());
            AddColumn("dbo.FileAttaches", "Link", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FileAttaches", "Link");
            DropColumn("dbo.FileAttaches", "Title");
        }
    }
}
