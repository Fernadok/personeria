using System.Data.Entity.Migrations;
using Abp.MultiTenancy;
using Abp.Zero.EntityFramework;
using Personeria.Migrations.SeedData;
using EntityFramework.DynamicFilters;
using Personeria.Domain;

namespace Personeria.Migrations
{
    public sealed class Configuration : DbMigrationsConfiguration<EntityFramework.PersoneriaDbContext>, IMultiTenantSeed
    {
        public AbpTenantBase Tenant { get; set; }

        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Personeria";
        }

        protected override void Seed(EntityFramework.PersoneriaDbContext context)
        {
            context.DisableAllFilters();

            if (Tenant == null)
            {
                //Host seed
                new InitialHostDbBuilder(context).Create();

                //Default tenant seed (in host database).
                new DefaultTenantCreator(context).Create();
                new TenantRoleAndUserBuilder(context, 1).Create();

                new InitialLocationDepartmentState(context).Create();
            }
            else
            {
                //You can add seed for tenant databases and use Tenant property...
            }

            context.SaveChanges();

            var fileAttachConfig1 = new FileAttach()
            {
                Title = "Equipo de Personas Juridicas",
                FileName = "01.jpg",
                Path = "/files/01.jpg",
                Size = 1024,
                Store = PersoneriaConsts.ConfigKey
            };
            var fileAttachConfig2 = new FileAttach()
            {
                Title = "Con el ministro",
                FileName = "02.jpg",
                Path = "/files/02.jpg",
                Size = 1024,
                Store = PersoneriaConsts.ConfigKey
            };
            var fileAttachConfig3 = new FileAttach()
            {
                Title = "Reuniones informativas",
                FileName = "03.jpg",
                Path = "/files/03.jpg",
                Size = 1024,
                Store = PersoneriaConsts.ConfigKey
            };
            var fileAttachConfig4 = new FileAttach()
            {
                Title = "Entrega de juguetes",
                FileName = "04.jpg",
                Path = "/files/04.jpg",
                Size = 1024,
                Store = PersoneriaConsts.ConfigKey
            };
            var fileAttachConfig5 = new FileAttach()
            {
                Title = "D�a del ni�o",
                FileName = "05.jpg",
                Path = "/files/05.jpg",
                Size = 1024,
                Store = PersoneriaConsts.ConfigKey
            };
            context.FileAttachs.AddOrUpdate(x => x.FileName, fileAttachConfig1, fileAttachConfig2, fileAttachConfig3, fileAttachConfig4, fileAttachConfig5);
            context.SaveChanges();

            ////State
            //var stateSanLuis = new State() { Name = "San Luis" };
            //context.States.AddOrUpdate(x => x.Name, stateSanLuis);
            //context.SaveChanges();

            //#region Departamentos de San Luis
            //var department1 = new Department() { Name = "Ayacucho", StateId = stateSanLuis.Id };
            //var department2 = new Department() { Name = "Belgrano", StateId = stateSanLuis.Id };
            //var department3 = new Department() { Name = "Chacabuco", StateId = stateSanLuis.Id };
            //var department4 = new Department() { Name = "Coronel Pringles", StateId = stateSanLuis.Id };
            //var department5 = new Department() { Name = "General Pedernera", StateId = stateSanLuis.Id };
            //var department6 = new Department() { Name = "Gobernador Dupuy", StateId = stateSanLuis.Id };
            //var department7 = new Department() { Name = "Jun�n", StateId = stateSanLuis.Id };
            //var department8 = new Department() { Name = "Juan Mart�n de Pueyrred�n", StateId = stateSanLuis.Id };
            //var department9 = new Department() { Name = "Libertador General San Mart�n", StateId = stateSanLuis.Id };
            //context.Departments.AddOrUpdate(x => new { x.Name, x.StateId }, department1, department2, department3, department4, department5, department6, department7, department8, department9);
            //context.SaveChanges();
            //#endregion

            //#region Ciudades de San Luis
            //var location1 = new Location() { PostalCode = "5713", Name = "Candelaria", DepartmentId = department1.Id };
            //var location2 = new Location() { PostalCode = "5703", Name = "La Majada", DepartmentId = department1.Id };
            //var location3 = new Location() { PostalCode = "5703", Name = "Leandro N. Alem", DepartmentId = department1.Id };
            //var location4 = new Location() { PostalCode = "5709", Name = "Luj�n", DepartmentId = department1.Id };
            //var location5 = new Location() { PostalCode = "5711", Name = "Quines", DepartmentId = department1.Id };
            //var location6 = new Location() { PostalCode = "5705", Name = "R�o Juan Gomez", DepartmentId = department1.Id };
            //var location7 = new Location() { PostalCode = "5705", Name = "San Francisco del Monte de Oro", DepartmentId = department1.Id };

            //var location8 = new Location() { PostalCode = "5719", Name = "La Calera", DepartmentId = department2.Id };
            //var location9 = new Location() { PostalCode = "5720", Name = "Nogol�", DepartmentId = department2.Id };
            //var location10 = new Location() { PostalCode = "5703", Name = "Villa de la Quebrada", DepartmentId = department2.Id };
            //var location11 = new Location() { PostalCode = "5703", Name = "Villa General Roca", DepartmentId = department2.Id };

            //var location12 = new Location() { PostalCode = "5770", Name = "Concar�n", DepartmentId = department3.Id };
            //var location13 = new Location() { PostalCode = "5883", Name = "Cortaderas", DepartmentId = department3.Id };
            //var location14 = new Location() { PostalCode = "5759", Name = "Naschel", DepartmentId = department3.Id };
            //var location15 = new Location() { PostalCode = "5883", Name = "Papagayos", DepartmentId = department3.Id };
            //var location16 = new Location() { PostalCode = "6775", Name = "Renca", DepartmentId = department3.Id };
            //var location17 = new Location() { PostalCode = "5773", Name = "San Pablo", DepartmentId = department3.Id };
            //var location18 = new Location() { PostalCode = "5773", Name = "Tilisarao", DepartmentId = department3.Id };
            //var location19 = new Location() { PostalCode = "5835", Name = "Villa del Carmen", DepartmentId = department3.Id };
            //var location20 = new Location() { PostalCode = "5883", Name = "Villa Larca", DepartmentId = department3.Id };

            //var location21 = new Location() { PostalCode = "5757", Name = "Carolina", DepartmentId = department4.Id };
            //var location22 = new Location() { PostalCode = "5701", Name = "El Trapiche", DepartmentId = department4.Id };
            //var location23 = new Location() { PostalCode = "5736", Name = "Fraga", DepartmentId = department4.Id };
            //var location24 = new Location() { PostalCode = "5713", Name = "La Bajada", DepartmentId = department4.Id };
            //var location25 = new Location() { PostalCode = "5701", Name = "La Florida", DepartmentId = department4.Id };
            //var location26 = new Location() { PostalCode = "5750", Name = "La Toma", DepartmentId = department4.Id };
            //var location27 = new Location() { PostalCode = "5757", Name = "Riocito", DepartmentId = department4.Id };
            //var location28 = new Location() { PostalCode = "5751", Name = "Saladillo", DepartmentId = department4.Id };

            //var location29 = new Location() { PostalCode = "5731", Name = "Juan Jorba", DepartmentId = department5.Id };
            //var location30 = new Location() { PostalCode = "5735", Name = "Juan Llerena", DepartmentId = department5.Id };
            //var location31 = new Location() { PostalCode = "5738", Name = "Justo Daract", DepartmentId = department5.Id };
            //var location32 = new Location() { PostalCode = "5831", Name = "La Punilla", DepartmentId = department5.Id };
            //var location33 = new Location() { PostalCode = "5730", Name = "Lavaisse", DepartmentId = department5.Id };
            //var location34 = new Location() { PostalCode = "5731", Name = "San Jos� del Morro", DepartmentId = department5.Id };
            //var location35 = new Location() { PostalCode = "5730", Name = "Villa Mercedes", DepartmentId = department5.Id };
            //var location36 = new Location() { PostalCode = "5733", Name = "Villa Reynolds", DepartmentId = department5.Id };
            //var location37 = new Location() { PostalCode = "5738", Name = "Villa Salles", DepartmentId = department5.Id };

            //var location38 = new Location() { PostalCode = "6216", Name = "Anchorena", DepartmentId = department6.Id };
            //var location39 = new Location() { PostalCode = "6389", Name = "Arizona", DepartmentId = department6.Id };
            //var location40 = new Location() { PostalCode = "6216", Name = "Bagual", DepartmentId = department6.Id };
            //var location41 = new Location() { PostalCode = "6279", Name = "Batavia", DepartmentId = department6.Id };
            //var location42 = new Location() { PostalCode = "6277", Name = "Buena Esperanza", DepartmentId = department6.Id };
            //var location43 = new Location() { PostalCode = "6279", Name = "Fort�n El Patria", DepartmentId = department6.Id };
            //var location44 = new Location() { PostalCode = "6216", Name = "Fortuna", DepartmentId = department6.Id };
            //var location45 = new Location() { PostalCode = "6216", Name = "La Maroma", DepartmentId = department6.Id };
            //var location46 = new Location() { PostalCode = "6279", Name = "Mart�n de Loyola", DepartmentId = department6.Id };
            //var location47 = new Location() { PostalCode = "6279", Name = "Nahuel Map�", DepartmentId = department6.Id };
            //var location48 = new Location() { PostalCode = "6279", Name = "Navia", DepartmentId = department6.Id };
            //var location49 = new Location() { PostalCode = "6216", Name = "Nueva Galia", DepartmentId = department6.Id };
            //var location50 = new Location() { PostalCode = "6216", Name = "Uni�n", DepartmentId = department6.Id };

            //var location51 = new Location() { PostalCode = "5883", Name = "Carpinter�a", DepartmentId = department7.Id };
            //var location52 = new Location() { PostalCode = "5881", Name = "Cerro de Oro", DepartmentId = department7.Id };
            //var location53 = new Location() { PostalCode = "5871", Name = "Lafinur", DepartmentId = department7.Id };
            //var location54 = new Location() { PostalCode = "5871", Name = "Los Cajones", DepartmentId = department7.Id };
            //var location55 = new Location() { PostalCode = "5883", Name = "Los Molles", DepartmentId = department7.Id };
            //var location56 = new Location() { PostalCode = "5881", Name = "Merlo", DepartmentId = department7.Id };
            //var location57 = new Location() { PostalCode = "5777", Name = "Santa Rosa del Conlara", DepartmentId = department7.Id };
            //var location58 = new Location() { PostalCode = "5711", Name = "Talita", DepartmentId = department7.Id };

            //var location59 = new Location() { PostalCode = "5721", Name = "Alto Pelado", DepartmentId = department8.Id };
            //var location60 = new Location() { PostalCode = "5724", Name = "Alto Pencoso", DepartmentId = department8.Id };
            //var location61 = new Location() { PostalCode = "5724", Name = "Balde", DepartmentId = department8.Id };
            //var location62 = new Location() { PostalCode = "5721", Name = "Beazley", DepartmentId = department8.Id };
            //var location63 = new Location() { PostalCode = "5721", Name = "Cazador", DepartmentId = department8.Id };
            //var location64 = new Location() { PostalCode = "5724", Name = "Chosmes", DepartmentId = department8.Id };
            //var location65 = new Location() { PostalCode = "5598", Name = "Desaguadero", DepartmentId = department8.Id };
            //var location66 = new Location() { PostalCode = "5701", Name = "El Volc�n", DepartmentId = department8.Id };
            //var location67 = new Location() { PostalCode = "5721", Name = "Jarilla", DepartmentId = department8.Id };
            //var location68 = new Location() { PostalCode = "5701", Name = "Juana Koslay", DepartmentId = department8.Id };
            //var location69 = new Location() { PostalCode = "5710", Name = "La Punta", DepartmentId = department8.Id };
            //var location70 = new Location() { PostalCode = "5721", Name = "Mosmota", DepartmentId = department8.Id };
            //var location71 = new Location() { PostalCode = "5701", Name = "Potrero de los Funes", DepartmentId = department8.Id };
            //var location72 = new Location() { PostalCode = "5701", Name = "Salinas del Bebedero", DepartmentId = department8.Id };
            //var location73 = new Location() { PostalCode = "5719", Name = "San Ger�nimo", DepartmentId = department8.Id };
            //var location74 = new Location() { PostalCode = "5700", Name = "San Luis", DepartmentId = department8.Id };
            //var location75 = new Location() { PostalCode = "5721", Name = "Zanjitas", DepartmentId = department8.Id };

            //var location76 = new Location() { PostalCode = "5771", Name = "Las Aguadas", DepartmentId = department9.Id };
            //var location77 = new Location() { PostalCode = "5753", Name = "Las Chacras", DepartmentId = department9.Id };
            //var location78 = new Location() { PostalCode = "5773", Name = "Las Lagunas", DepartmentId = department9.Id };
            //var location79 = new Location() { PostalCode = "5757", Name = "La Vertiente", DepartmentId = department9.Id };
            //var location80 = new Location() { PostalCode = "5753", Name = "Paso Grande", DepartmentId = department9.Id };
            //var location81 = new Location() { PostalCode = "5755", Name = "San Mart�n", DepartmentId = department9.Id };
            //var location82 = new Location() { PostalCode = "5753", Name = "Villa de Praga", DepartmentId = department9.Id };




            //context.Locations.AddOrUpdate(x => new { x.Name, x.DepartmentId },
            //    location1, location2, location3, location4, location5, location6, location7, location8, location9, location10,
            //    location11, location12, location13, location14, location15, location16, location17, location18, location19, location20,
            //    location21, location22, location23, location24, location25, location26, location27, location28, location29, location30,
            //    location31, location32, location33, location34, location35, location36, location37, location38, location39, location40,
            //    location41, location42, location43, location44, location45, location46, location47, location48, location49, location50,
            //    location51, location52, location53, location54, location55, location56, location57, location58, location59, location60,
            //    location61, location62, location63, location64, location65, location66, location67, location68, location69, location70,
            //    location71, location72, location73, location74, location75, location76, location77, location78, location79, location80,
            //    location81, location82);
            //context.SaveChanges();
            //#endregion
        }
    }
}
