namespace Personeria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProperty_Transact_LockedAreaId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transacts", "LockedAreaId", c => c.Long());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Transacts", "LockedAreaId");
        }
    }
}
