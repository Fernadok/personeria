namespace Personeria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangesForTransact : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Histories", new[] { "UserId" });
            AddColumn("dbo.Transacts", "AreaId", c => c.Long(nullable: false));
            AlterColumn("dbo.Histories", "UserId", c => c.Long());
            CreateIndex("dbo.Transacts", "AreaId");
            CreateIndex("dbo.Histories", "UserId");
            AddForeignKey("dbo.Transacts", "AreaId", "dbo.Areas", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Transacts", "AreaId", "dbo.Areas");
            DropIndex("dbo.Histories", new[] { "UserId" });
            DropIndex("dbo.Transacts", new[] { "AreaId" });
            AlterColumn("dbo.Histories", "UserId", c => c.Long(nullable: false));
            DropColumn("dbo.Transacts", "AreaId");
            CreateIndex("dbo.Histories", "UserId");
        }
    }
}
