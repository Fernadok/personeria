namespace Personeria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitModels : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AbpFeatures", "EditionId", "dbo.AbpEditions");
            DropForeignKey("dbo.AbpPermissions", "RoleId", "dbo.AbpRoles");
            DropForeignKey("dbo.AbpUserClaims", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.AbpUserLogins", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.AbpPermissions", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.AbpUserRoles", "UserId", "dbo.AbpUsers");
            CreateTable(
                "dbo.Areas",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TransactTypeAreas",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        TransactTypeId = c.Long(nullable: false),
                        AreaId = c.Long(nullable: false),
                        Order = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Areas", t => t.AreaId)
                .ForeignKey("dbo.TransactTypes", t => t.TransactTypeId)
                .Index(t => t.TransactTypeId)
                .Index(t => t.AreaId);
            
            CreateTable(
                "dbo.TransactTypes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Transacts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Subject = c.String(),
                        State = c.Int(nullable: false),
                        Observation = c.String(),
                        PersonId = c.Long(nullable: false),
                        TransactTypeId = c.Long(nullable: false),
                        LocationId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationId)
                .ForeignKey("dbo.People", t => t.PersonId)
                .ForeignKey("dbo.TransactTypes", t => t.TransactTypeId)
                .Index(t => t.PersonId)
                .Index(t => t.TransactTypeId)
                .Index(t => t.LocationId);
            
            CreateTable(
                "dbo.FileAttaches",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Path = c.String(),
                        Size = c.Long(nullable: false),
                        FileName = c.String(),
                        Store = c.String(),
                        TransactId = c.Long(),
                        TransactTypeFileAttachTypeId = c.Long(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Transacts", t => t.TransactId)
                .ForeignKey("dbo.TransactTypeFileAttachTypes", t => t.TransactTypeFileAttachTypeId)
                .Index(t => t.TransactId)
                .Index(t => t.TransactTypeFileAttachTypeId);
            
            CreateTable(
                "dbo.TransactTypeFileAttachTypes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        FileAttachTypeId = c.Long(nullable: false),
                        TransactTypeId = c.Long(nullable: false),
                        IsRequired = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FileAttachTypes", t => t.FileAttachTypeId)
                .ForeignKey("dbo.TransactTypes", t => t.TransactTypeId)
                .Index(t => t.FileAttachTypeId)
                .Index(t => t.TransactTypeId);
            
            CreateTable(
                "dbo.FileAttachTypes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Histories",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CreateDate = c.DateTime(nullable: false),
                        UserId = c.Long(nullable: false),
                        TransactId = c.Long(nullable: false),
                        State = c.Int(nullable: false),
                        Observation = c.String(),
                        Ip = c.String(),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Transacts", t => t.TransactId)
                .ForeignKey("dbo.AbpUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.TransactId);
            
            CreateTable(
                "dbo.UserAreas",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.Long(nullable: false),
                        AreaId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Areas", t => t.AreaId)
                .ForeignKey("dbo.AbpUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.AreaId);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        PostalCode = c.String(),
                        DepartmentId = c.Long(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Departments", t => t.DepartmentId)
                .Index(t => t.DepartmentId);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        StateId = c.Long(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.States", t => t.StateId)
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.States",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.People",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Surname = c.String(),
                        NumberIdentity = c.String(),
                        Email = c.String(),
                        Phone = c.String(),
                        Adreess = c.String(),
                        LocationId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationId)
                .Index(t => t.LocationId);
            
            AddForeignKey("dbo.AbpFeatures", "EditionId", "dbo.AbpEditions", "Id");
            AddForeignKey("dbo.AbpPermissions", "RoleId", "dbo.AbpRoles", "Id");
            AddForeignKey("dbo.AbpUserClaims", "UserId", "dbo.AbpUsers", "Id");
            AddForeignKey("dbo.AbpUserLogins", "UserId", "dbo.AbpUsers", "Id");
            AddForeignKey("dbo.AbpPermissions", "UserId", "dbo.AbpUsers", "Id");
            AddForeignKey("dbo.AbpUserRoles", "UserId", "dbo.AbpUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AbpUserRoles", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.AbpPermissions", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.AbpUserLogins", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.AbpUserClaims", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.AbpPermissions", "RoleId", "dbo.AbpRoles");
            DropForeignKey("dbo.AbpFeatures", "EditionId", "dbo.AbpEditions");
            DropForeignKey("dbo.TransactTypeAreas", "TransactTypeId", "dbo.TransactTypes");
            DropForeignKey("dbo.Transacts", "TransactTypeId", "dbo.TransactTypes");
            DropForeignKey("dbo.Transacts", "PersonId", "dbo.People");
            DropForeignKey("dbo.Transacts", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.People", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.Locations", "DepartmentId", "dbo.Departments");
            DropForeignKey("dbo.Departments", "StateId", "dbo.States");
            DropForeignKey("dbo.Histories", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.UserAreas", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.UserAreas", "AreaId", "dbo.Areas");
            DropForeignKey("dbo.Histories", "TransactId", "dbo.Transacts");
            DropForeignKey("dbo.FileAttaches", "TransactTypeFileAttachTypeId", "dbo.TransactTypeFileAttachTypes");
            DropForeignKey("dbo.TransactTypeFileAttachTypes", "TransactTypeId", "dbo.TransactTypes");
            DropForeignKey("dbo.TransactTypeFileAttachTypes", "FileAttachTypeId", "dbo.FileAttachTypes");
            DropForeignKey("dbo.FileAttaches", "TransactId", "dbo.Transacts");
            DropForeignKey("dbo.TransactTypeAreas", "AreaId", "dbo.Areas");
            DropIndex("dbo.People", new[] { "LocationId" });
            DropIndex("dbo.Departments", new[] { "StateId" });
            DropIndex("dbo.Locations", new[] { "DepartmentId" });
            DropIndex("dbo.UserAreas", new[] { "AreaId" });
            DropIndex("dbo.UserAreas", new[] { "UserId" });
            DropIndex("dbo.Histories", new[] { "TransactId" });
            DropIndex("dbo.Histories", new[] { "UserId" });
            DropIndex("dbo.TransactTypeFileAttachTypes", new[] { "TransactTypeId" });
            DropIndex("dbo.TransactTypeFileAttachTypes", new[] { "FileAttachTypeId" });
            DropIndex("dbo.FileAttaches", new[] { "TransactTypeFileAttachTypeId" });
            DropIndex("dbo.FileAttaches", new[] { "TransactId" });
            DropIndex("dbo.Transacts", new[] { "LocationId" });
            DropIndex("dbo.Transacts", new[] { "TransactTypeId" });
            DropIndex("dbo.Transacts", new[] { "PersonId" });
            DropIndex("dbo.TransactTypeAreas", new[] { "AreaId" });
            DropIndex("dbo.TransactTypeAreas", new[] { "TransactTypeId" });
            DropTable("dbo.People");
            DropTable("dbo.States");
            DropTable("dbo.Departments");
            DropTable("dbo.Locations");
            DropTable("dbo.UserAreas");
            DropTable("dbo.Histories");
            DropTable("dbo.FileAttachTypes");
            DropTable("dbo.TransactTypeFileAttachTypes");
            DropTable("dbo.FileAttaches");
            DropTable("dbo.Transacts");
            DropTable("dbo.TransactTypes");
            DropTable("dbo.TransactTypeAreas");
            DropTable("dbo.Areas");
            AddForeignKey("dbo.AbpUserRoles", "UserId", "dbo.AbpUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AbpPermissions", "UserId", "dbo.AbpUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AbpUserLogins", "UserId", "dbo.AbpUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AbpUserClaims", "UserId", "dbo.AbpUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AbpPermissions", "RoleId", "dbo.AbpRoles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AbpFeatures", "EditionId", "dbo.AbpEditions", "Id", cascadeDelete: true);
        }
    }
}
