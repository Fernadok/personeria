namespace Personeria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPrint : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Prints",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Ip = c.String(),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.AbpUsers", "PrintTicketDefaultId", c => c.Long());
            CreateIndex("dbo.AbpUsers", "PrintTicketDefaultId");
            AddForeignKey("dbo.AbpUsers", "PrintTicketDefaultId", "dbo.Prints", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AbpUsers", "PrintTicketDefaultId", "dbo.Prints");
            DropIndex("dbo.AbpUsers", new[] { "PrintTicketDefaultId" });
            DropColumn("dbo.AbpUsers", "PrintTicketDefaultId");
            DropTable("dbo.Prints");
        }
    }
}
