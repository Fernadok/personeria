﻿using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using Abp.Zero.EntityFramework;
using Personeria.EntityFramework;

namespace Personeria
{
    [DependsOn(typeof(AbpZeroEntityFrameworkModule), typeof(PersoneriaCoreModule))]
    public class PersoneriaDataModule : AbpModule
    {
        public override void PreInitialize()
        {
            //Database.SetInitializer(new CreateDatabaseIfNotExists<PersoneriaDbContext>());
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<PersoneriaDbContext, Migrations.Configuration>());
            Configuration.DefaultNameOrConnectionString = "Default";
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
