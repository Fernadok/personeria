﻿using Personeria.Domain;
using Personeria.Domain.Relation;
using System.Data.Entity;

namespace Personeria.EntityFramework
{
    public interface IPersoneriaDbContext
    {
        IDbSet<Area> Areas { get; set; }
        IDbSet<Department> Departments { get; set; }
        IDbSet<FileAttach> FileAttachs { get; set; }
        IDbSet<FileAttachType> FileAttachTypes { get; set; }
        IDbSet<History> Histories { get; set; }
        IDbSet<Location> Locations { get; set; }
        IDbSet<Person> Persons { get; set; }
        IDbSet<State> States { get; set; }
        IDbSet<Transact> Transacts { get; set; }
        IDbSet<TransactType> TransactTypes { get; set; }
        IDbSet<TransactTypeArea> TransactTypeAreaRelations { get; set; }
        IDbSet<TransactTypeFileAttachType> TransactTypeFileAttachTypeRelations { get; set; }
        IDbSet<UserArea> UserAreaRelations { get; set; }
        IDbSet<Print> PrintTickets { get; set; }
        int SaveChanges();
    }
}
