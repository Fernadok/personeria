﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;

namespace Personeria.EntityFramework.Repositories
{
    public abstract class PersoneriaRepositoryBase<TEntity, TPrimaryKey> : EfRepositoryBase<PersoneriaDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected PersoneriaRepositoryBase(IDbContextProvider<PersoneriaDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //add common methods for all repositories
    }

    public abstract class PersoneriaRepositoryBase<TEntity> : PersoneriaRepositoryBase<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        protected PersoneriaRepositoryBase(IDbContextProvider<PersoneriaDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //do not add any method here, add to the class above (since this inherits it)
    }
}
