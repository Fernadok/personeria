﻿using System;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Abp.Zero.EntityFramework;
using Personeria.Authorization.Roles;
using Personeria.Authorization.Users;
using Personeria.Base;
using Personeria.Domain;
using Personeria.Domain.Relation;
using Personeria.EntityFramework.Configurations;
using Personeria.EntityFramework.Configurations.Relation;
using Personeria.MultiTenancy;

namespace Personeria.EntityFramework
{
    public class PersoneriaDbContext : AbpZeroDbContext<Tenant, Role, User>, IPersoneriaDbContext
    {
        //TODO: Define an IDbSet for your Entities...
        public virtual IDbSet<Area> Areas { get; set; }
        public virtual IDbSet<Department> Departments { get; set; }
        public virtual IDbSet<FileAttach> FileAttachs { get; set; }
        public virtual IDbSet<FileAttachType> FileAttachTypes { get; set; }
        public virtual IDbSet<History> Histories { get; set; }
        public virtual IDbSet<Location> Locations { get; set; }
        public virtual IDbSet<Person> Persons { get; set; }
        public virtual IDbSet<State> States { get; set; }
        public virtual IDbSet<Transact> Transacts { get; set; }
        public virtual IDbSet<TransactType> TransactTypes { get; set; }
        public virtual IDbSet<TransactTypeArea> TransactTypeAreaRelations { get; set; }
        public virtual IDbSet<TransactTypeFileAttachType> TransactTypeFileAttachTypeRelations { get; set; }
        public virtual IDbSet<UserArea> UserAreaRelations { get; set; }
        public virtual IDbSet<Print> PrintTickets { get; set; }

        /* NOTE: 
         *   Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         *   But it may cause problems when working Migrate.exe of EF. If you will apply migrations on command line, do not
         *   pass connection string name to base classes. ABP works either way.
         */
        public PersoneriaDbContext()
            : base("Default")
        {

        }

        /* NOTE:
         *   This constructor is used by ABP to pass connection string defined in PersoneriaDataModule.PreInitialize.
         *   Notice that, actually you will not directly create an instance of PersoneriaDbContext since ABP automatically handles it.
         */
        public PersoneriaDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        //This constructor is used in tests
        public PersoneriaDbContext(DbConnection existingConnection)
         : base(existingConnection, false)
        {

        }

        public PersoneriaDbContext(DbConnection existingConnection, bool contextOwnsConnection)
         : base(existingConnection, contextOwnsConnection)
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Configurations.Add(new AreaConfiguration());
            modelBuilder.Configurations.Add(new DepartmentConfiguration());
            modelBuilder.Configurations.Add(new FileAttachConfiguration());
            modelBuilder.Configurations.Add(new FileAttachTypeConfiguration());
            modelBuilder.Configurations.Add(new HistoryConfiguration());
            modelBuilder.Configurations.Add(new LocationConfiguration());
            modelBuilder.Configurations.Add(new PersonConfiguration());
            modelBuilder.Configurations.Add(new StateConfiguration());
            modelBuilder.Configurations.Add(new TransactConfiguration());
            modelBuilder.Configurations.Add(new TransactTypeConfiguration());
            modelBuilder.Configurations.Add(new TransactTypeAreaConfiguration());
            modelBuilder.Configurations.Add(new TransactTypeFileAttachTypeConfiguration());
            modelBuilder.Configurations.Add(new UserAreaConfiguration());
            modelBuilder.Configurations.Add(new PrintConfiguration());

            //-- Config class User
            modelBuilder.Entity<User>()
                        .HasOptional(x => x.PrintTicketDefault)
                        .WithMany(x => x.Users)
                        .HasForeignKey(x => x.PrintTicketDefaultId);

            base.OnModelCreating(modelBuilder);

        }

        public override async Task<int> SaveChangesAsync()
        {
            ChangesAuditable();
            return await base.SaveChangesAsync();
        }

        public override int SaveChanges()
        {
            ChangesAuditable();
            return base.SaveChanges();
        }

        private void ChangesAuditable()
        {
            var modifiedEntries = ChangeTracker.Entries()
                            .Where(x => (x.Entity is IAuditableEntity || x.Entity is Abp.Auditing.AuditLog)
                                   && (x.State == EntityState.Added || x.State == EntityState.Modified));



            foreach (var entry in modifiedEntries)
            {
                if (entry.Entity is IAuditableEntity entity)
                {
                    string identityName = Thread.CurrentPrincipal.Identity.Name;
                    var now = DateTime.Now;

                    //foreach (var item in entity.GetType().GetProperties())
                    //{
                    //    var type = item.PropertyType.Name;
                    //    if (type.Equals("String") && item.GetValue(entity) != null && item.CanWrite)
                    //    {
                    //        var value = item.GetValue(entity).ToString().ToUpper();
                    //        item.SetValue(entity, value);
                    //    }
                    //}

                    if (entry.State == EntityState.Added)
                    {
                        entity.CreatedBy = identityName;
                        entity.CreatedDate = now;
                    }
                    else
                    {
                        base.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
                        base.Entry(entity).Property(x => x.CreatedDate).IsModified = false;
                    }

                    entity.UpdatedBy = identityName;
                    entity.UpdatedDate = now;
                }
            }
        }
    }
}
