﻿using Personeria.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Personeria.EntityFramework.Configurations
{
    public class FileAttachConfiguration : EntityTypeConfiguration<FileAttach>
    {
        public FileAttachConfiguration()
        {
            HasKey(x => x.Id);

            HasOptional(x => x.Transact)
                .WithMany(x => x.FileAttachs)
                .HasForeignKey(x => x.TransactId);

            HasOptional(x => x.TransactTypeFileAttachType)
                .WithMany(x => x.FileAttachs)
                .HasForeignKey(x => x.TransactTypeFileAttachTypeId);
        }
    }
}
