﻿using Personeria.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Personeria.EntityFramework.Configurations
{
    public class FileAttachTypeConfiguration : EntityTypeConfiguration<FileAttachType>
    {
        public FileAttachTypeConfiguration()
        {
            HasKey(x => x.Id);
        }
    }
}
