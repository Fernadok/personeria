﻿using Personeria.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Personeria.EntityFramework.Configurations
{
    public class HistoryConfiguration : EntityTypeConfiguration<History>
    {
        public HistoryConfiguration()
        {
            HasKey(x => x.Id);

            HasOptional(x => x.User)
                .WithMany(x => x.Histories)
                .HasForeignKey(x => x.UserId);

            HasRequired(x => x.Transact)
               .WithMany(x => x.Histories)
               .HasForeignKey(x => x.TransactId);

        }
    }
}
