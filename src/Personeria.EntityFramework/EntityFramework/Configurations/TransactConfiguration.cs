﻿using Personeria.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Personeria.EntityFramework.Configurations
{
    public class TransactConfiguration : EntityTypeConfiguration<Transact>
    {
        public TransactConfiguration()
        {
            HasKey(x => x.Id);

            HasRequired(x => x.Person)
                .WithMany(x => x.Transacts)
                .HasForeignKey(x => x.PersonId);

            HasRequired(x => x.TransactType)
                .WithMany(x => x.Transacts)
                .HasForeignKey(x => x.TransactTypeId);

            HasRequired(x => x.Location)
                .WithMany(x => x.Transacts)
                .HasForeignKey(x => x.LocationId);

            HasRequired(x => x.Area)
                .WithMany(x => x.Transacts)
                .HasForeignKey(x => x.AreaId);

              
        }
    }
}
