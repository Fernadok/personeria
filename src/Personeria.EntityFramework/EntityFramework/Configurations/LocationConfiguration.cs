﻿using Personeria.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Personeria.EntityFramework.Configurations
{
    public class LocationConfiguration : EntityTypeConfiguration<Location>
    {
        public LocationConfiguration()
        {
            HasKey(x => x.Id);

            HasRequired(x => x.Department)
                .WithMany(x => x.Locations)
                .HasForeignKey(x => x.DepartmentId);

        }
    }
}
