﻿using Personeria.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Personeria.EntityFramework.Configurations
{
    public class PersonConfiguration : EntityTypeConfiguration<Person>
    {
        public PersonConfiguration()
        {
            HasKey(x => x.Id);            
            
            HasRequired(x => x.Location)
                .WithMany(x => x.Persons)
                .HasForeignKey(x => x.LocationId);
        
        }
    }
}
