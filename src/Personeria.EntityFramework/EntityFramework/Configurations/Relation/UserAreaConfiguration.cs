﻿using Personeria.Domain.Relation;
using System.Data.Entity.ModelConfiguration;

namespace Personeria.EntityFramework.Configurations.Relation
{
    public class UserAreaConfiguration : EntityTypeConfiguration<UserArea>
    {
        public UserAreaConfiguration()
        {
            HasKey(x => x.Id);

            HasRequired(x => x.User)
              .WithMany(x => x.UserAreaRelations)
              .HasForeignKey(x => x.UserId);


            HasRequired(x => x.Area)
              .WithMany(x => x.UserAreaRelations)
              .HasForeignKey(x => x.AreaId);
        }
    }
}
