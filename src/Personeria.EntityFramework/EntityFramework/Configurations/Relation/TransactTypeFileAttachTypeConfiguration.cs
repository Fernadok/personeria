﻿using Personeria.Domain.Relation;
using System.Data.Entity.ModelConfiguration;

namespace Personeria.EntityFramework.Configurations.Relation
{
    public class TransactTypeFileAttachTypeConfiguration : EntityTypeConfiguration<TransactTypeFileAttachType>
    {
        public TransactTypeFileAttachTypeConfiguration()
        {
            HasKey(x => x.Id);

            HasRequired(x => x.FileAttachType)
              .WithMany(x => x.TransactTypeFileAttachTypeRelations)
              .HasForeignKey(x => x.FileAttachTypeId);


            HasRequired(x => x.TransactType)
              .WithMany(x => x.TransactTypeFileAttachTypeRelations)
              .HasForeignKey(x => x.TransactTypeId);
        }
    }
}
