﻿using Personeria.Domain.Relation;
using System.Data.Entity.ModelConfiguration;

namespace Personeria.EntityFramework.Configurations.Relation
{
    public class TransactTypeAreaConfiguration : EntityTypeConfiguration<TransactTypeArea>
    {
        public TransactTypeAreaConfiguration()
        {
            HasKey(x => x.Id);

            HasRequired(x => x.TransactType)
              .WithMany(x => x.TransactTypeAreaRelations)
              .HasForeignKey(x => x.TransactTypeId);


            HasRequired(x => x.Area)
              .WithMany(x => x.TransactTypeAreaRelations)
              .HasForeignKey(x => x.AreaId);
        }
    }
}
