﻿using Personeria.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Personeria.EntityFramework.Configurations
{
    public class PrintConfiguration : EntityTypeConfiguration<Print>
    {
        public PrintConfiguration()
        {
            HasKey(x => x.Id);
        }
    }
}
