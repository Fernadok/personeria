﻿using Personeria.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Personeria.EntityFramework.Configurations
{
    public class DepartmentConfiguration : EntityTypeConfiguration<Department>
    {
        public DepartmentConfiguration()
        {
            HasKey(x => x.Id);

            HasRequired(x => x.State)
                .WithMany(x => x.Departments)
                .HasForeignKey(x => x.StateId);
        }
    }
}
