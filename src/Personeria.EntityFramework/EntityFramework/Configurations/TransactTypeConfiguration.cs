﻿using Personeria.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Personeria.EntityFramework.Configurations
{
    public class TransactTypeConfiguration : EntityTypeConfiguration<TransactType>
    {
        public TransactTypeConfiguration()
        {
            HasKey(x => x.Id);
        }
    }
}
