﻿using Abp.WebApi.Controllers;
using Personeria.Providers;
using System;
using System.IO;
using System.Web.Http;
using System.Web.Http.Results;

namespace Personeria.Api.Controllers
{
    public class FileController : AbpApiController
    {
        private readonly INetWorkProvider _netWorkProvider;
        private readonly string _pathFolderMedia;
        private readonly string _pathWebFolderMedia;

        public FileController(INetWorkProvider netWorkProvider)
        {
            _netWorkProvider = netWorkProvider;

            _pathWebFolderMedia = _netWorkProvider.GetAppSettings("FilesFolder");
            _pathFolderMedia = _netWorkProvider.MapPath(_pathWebFolderMedia);
        }

        [HttpGet]
        [Route("CreateFile")]
        public string CreateFile()
        {
            try
            {
                var fileName = string.Format("test_{0}.txt", DateTime.Now.ToString("yyyyMMddHHmmss"));
                var pathAndFile = Path.Combine(_pathFolderMedia, fileName);

                string path = pathAndFile;

                if (!File.Exists(path))
                {
                    // Create a file to write to.
                    using (StreamWriter sw = File.CreateText(path))
                    {
                        sw.WriteLine("Hello");
                        sw.WriteLine("And");
                        sw.WriteLine("Welcome");
                    }
                }

                using (StreamReader sr = File.OpenText(path))
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                    {
                        Console.WriteLine(s);
                    }
                }

                return "Se generó el archivo con exito!";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}
