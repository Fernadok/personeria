﻿using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using NinjaNye.SearchExtensions;
using PagedList;
using System.Collections.Generic;
using System.Linq;
using Personeria.Base;
using Personeria.Domain;
using Personeria.Extensions;
using Personeria.Histories.Dto;

namespace Personeria.Histories
{
   // [AbpAuthorize]
    public class HistoryService: PersoneriaAppServiceBase, IHistoryService
    {
        private readonly IRepository<History, long> _HistoryRepository;

        public HistoryService(IRepository<History, long> HistoryRepository)
        {
            _HistoryRepository = HistoryRepository;
        }

        [UnitOfWork(isTransactional: true)]
        public long AddOrUpdate(HistoryDto dto)
        {
            var isnew = _HistoryRepository.FirstOrDefault(x => x.Id == dto.Id);

            var ar = dto.MapTo(isnew);
            _HistoryRepository.InsertOrUpdate(ar);
            CurrentUnitOfWork.SaveChanges();

            return ar.Id;
        }

        [UnitOfWork(isTransactional: true)]
        public void Delete(long id)
        {
            var History = _HistoryRepository.FirstOrDefault(x => x.Id == id);
            if (History != null)
            {
                History.IsActive = false;
                _HistoryRepository.Update(History);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public ContextDto<HistoryDto> GetAll(SearchDto filters)
        {
            filters.Search = string.IsNullOrEmpty(filters.Search) ? "" : filters.Search.ToLower();
            var resultList = _HistoryRepository
                    .GetAll()
                    .Where(x => x.IsActive)
                    .Search(x => x.Observation)
                    .Containing(filters.Search)
                    .OrderByTo(filters.OrderColumn, filters.Order.ToLower() == "desc")
                    .ToPagedList(filters.Page, filters.PageSize);

            return new ContextDto<HistoryDto>
            {
                Data = resultList.Select(x => x.MapTo<HistoryDto>()),
                MetaData = resultList.GetMetaData()
            };
        }

        public HistoryDto GetById(long id)
        {
            var pre = _HistoryRepository.FirstOrDefault(x => x.Id == id);
            return pre?.MapTo<HistoryDto>();
        }

        public List<IdDescription> HistoryList(long TransactId)
        {
            return _HistoryRepository.GetAll()
              .Where(x => x.IsActive && x.TransactId == TransactId)
              .OrderByDescending(x => x.CreateDate)
              .ToList()
              .Select(x => new IdDescription
              {
                  Id = x.Id,
                  Created = x.CreateDate.ToString("dd/MM/yyyy HH:mm"),
                  Description = x.Observation
              }).ToList();
        }
    }
}
