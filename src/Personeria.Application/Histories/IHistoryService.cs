﻿using Abp.Application.Services;
using System.Collections.Generic;
using Personeria.Base;
using Personeria.Histories.Dto;

namespace Personeria.Histories
{
    public interface IHistoryService : IApplicationService
    {
        HistoryDto GetById(long id);

        ContextDto<HistoryDto> GetAll(SearchDto filters);

        long AddOrUpdate(HistoryDto dto);

        void Delete(long id);

        List<IdDescription> HistoryList(long TransactId);
    }
}
