﻿using Abp.AutoMapper;
using Abp.Domain.Entities;
using Personeria.Domain;
using Personeria.Enums;
using System;

namespace Personeria.Histories.Dto
{
    [AutoMap(typeof(History))]
    public class HistoryDto : Entity<long>
    {
        public DateTime CreateDate { get; set; }

        public long? UserId { get; set; }

        public long TransactId { get; set; }

        public TransactState State { get; set; }
        public string Observation { get; set; }
        public string Ip { get; set; }
    }
}
