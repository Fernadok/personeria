﻿using Abp.Auditing;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Runtime.Session;
using Abp.UI;
using MoreLinq;
using Personeria.Authorization;
using Personeria.Configuration.Dto;
using Personeria.FileAttachs.Dto;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Personeria.Configuration
{

    public class ConfigurationAppService : PersoneriaAppServiceBase, IConfigurationAppService
    {
        [AbpAuthorize]
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }

        [DisableAuditing]
        public async Task<HashSet<FileAttachForConfig>> GetHomePhotos()
        {
            var files = await FileAttachRepository.GetAllListAsync(x => x.Store == PersoneriaConsts.ConfigKey);
            return files.OrderBy(x => x.FileName).Select(x => x.MapTo<FileAttachForConfig>()).ToHashSet();
        }

        [DisableAuditing]
        public async Task<bool> IsSuscribeNewTransact()
        {
            return await NotificationService.IsSuscribe_SentNotForNewTransact(AbpSession.UserId.Value);
        }
        [DisableAuditing]
        public async Task SaveNotificationNewTransact(bool isNotification)
        {
            if (isNotification)
            {
                await NotificationService.Subscribe_SentNotForNewTransact(AbpSession.UserId.Value);
            }
            else
            {
                await NotificationService.Desubscribe_SentNotForNewTransact(AbpSession.UserId.Value);
            }
        }


        [DisableAuditing]
        public async Task<bool> IsSuscribeChangeTransact()
        {
            return await NotificationService.IsSuscribe_SentNotForChangeTransact(AbpSession.UserId.Value);
        }
        [DisableAuditing]
        public async Task SaveNotificationChangeTransact(bool isNotification)
        {
            if (isNotification)
            {
                await NotificationService.Subscribe_SentNotForChangeTransact(AbpSession.UserId.Value);
            }
            else
            {
                await NotificationService.Desubscribe_SentNotForChangeTransact(AbpSession.UserId.Value);
            }
        }

        [AbpAuthorize(PermissionNames.Pages_Config)]
        public async Task SavePhoto(FileAttachConfigDto photo)
        {
            var file = await FileAttachRepository.FirstOrDefaultAsync(x => x.Id == photo.Id && x.Store == PersoneriaConsts.ConfigKey);
            var fileIn = photo.MapTo(file);
            await FileAttachRepository.UpdateAsync(fileIn);
            await CurrentUnitOfWork.SaveChangesAsync();
        }

        [AbpAuthorize(PermissionNames.Pages_Config)]
        public async Task DeletePhoto(long photoId)
        {
            await FileAttachRepository.DeleteAsync(x => x.Id == photoId && x.Store == PersoneriaConsts.ConfigKey);
            await CurrentUnitOfWork.SaveChangesAsync();
        }

        [DisableAuditing]
        [AbpAuthorize]
        public async Task<HashSet<PrintDto>> GetPrints()
        {
            var prints = await PrintRepository.GetAllListAsync(x => x.IsActive);
            return prints.OrderBy(x => x.Name).Select(x => x.MapTo<PrintDto>()).ToHashSet();
        }

        [AbpAuthorize(PermissionNames.Pages_Config)]
        public async Task<PrintDto> AddOrUpdatePrint(PrintDto printInput)
        {
            var print = await PrintRepository.FirstOrDefaultAsync(x => x.Id == printInput.Id && x.IsActive);
            print = printInput.MapTo(print);
            var isNew = print == null;
            if (CheckIfExist(printInput, isNew))
            {
                throw new UserFriendlyException("¡Puede ser que ya existe este nombre de impresora o este número de IP, por favor intentelo nuevamente!");
            }

            await PrintRepository.InsertOrUpdateAsync(print);
            await CurrentUnitOfWork.SaveChangesAsync();

            return print.MapTo<PrintDto>();
        }

        [AbpAuthorize(PermissionNames.Pages_Config)]
        public async Task DeletePrint(long printId)
        {
            var print = await PrintRepository.FirstOrDefaultAsync(x => x.Id == printId && x.IsActive);

            if (print == null)
            {
                throw new UserFriendlyException("¡No se puede eliminar esta impresora porque no existe!");
            }

            if (print.Users.Any())
            {
                var oldPrint = await PrintRepository.FirstOrDefaultAsync(x => x.Id != printId && x.IsActive);
                foreach (var user in print.Users)
                {
                    user.PrintTicketDefaultId = oldPrint.Id;
                    await UserRepository.UpdateAsync(user);
                }
                await CurrentUnitOfWork.SaveChangesAsync();
            }

            await PrintRepository.DeleteAsync(print.Id);
            await CurrentUnitOfWork.SaveChangesAsync();
        }

        [AbpAuthorize]
        public async Task AsignPrint(PrintInputDto print)
        {
            var user = await UserRepository.FirstOrDefaultAsync(x => x.Id == AbpSession.UserId.Value);

            if (user == null)
            {
                throw new UserFriendlyException("¡No se puede asignar esta impresora porque no existe este usuario o puede ser que no esta logueado!");
            }

            if (print.Id != null)
            {
                var exist = await PrintRepository.FirstOrDefaultAsync(x => x.Id == print.Id.Value && x.IsActive);
                if (exist == null)
                {
                    throw new UserFriendlyException("¡No se puede asignar esta impresora porque no existe!");
                }
            }

            user.PrintTicketDefaultId = print.Id;
            await UserRepository.UpdateAsync(user);
            await CurrentUnitOfWork.SaveChangesAsync();
        }


        private bool CheckIfExist(PrintDto print, bool isNew = true)
        {
            return PrintRepository.GetAll().Any(x => (isNew || (!isNew && x.Id != print.Id)) && x.IsActive && (x.Name.ToLower() == print.Name.Trim().ToLower() || x.Ip == print.Ip.Trim()));
        }
    }
}
