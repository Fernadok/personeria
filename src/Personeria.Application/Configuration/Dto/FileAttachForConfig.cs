﻿using Abp.AutoMapper;
using Personeria.Domain;

namespace Personeria.Configuration.Dto
{
    [AutoMapFrom(typeof(FileAttach))]
    public class FileAttachForConfig
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        public string Path { get; set; }
    }
}
