﻿using Abp.AutoMapper;
using Personeria.Base;
using Personeria.Domain;

namespace Personeria.Configuration.Dto
{
    [AutoMap(typeof(Print))]
    public class PrintDto : EntityDto
    {        
        public string Name { get; set; }
        public string Ip { get; set; }

        public int UsersCount { get; set; }
    }
}
