﻿using System.ComponentModel.DataAnnotations;

namespace Personeria.Configuration.Dto
{
    public class ChangeUiThemeInput
    {
        [Required]
        [MaxLength(32)]
        public string Theme { get; set; }
    }
}