﻿using Abp.Application.Services;
using Personeria.Configuration.Dto;
using Personeria.FileAttachs.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Personeria.Configuration
{
    public interface IConfigurationAppService : IApplicationService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
        Task<HashSet<FileAttachForConfig>> GetHomePhotos();

        Task<bool> IsSuscribeNewTransact();
        Task SaveNotificationNewTransact(bool isNotification);

        Task<bool> IsSuscribeChangeTransact();
        Task SaveNotificationChangeTransact(bool isNotification);

        Task SavePhoto(FileAttachConfigDto photo);
        Task DeletePhoto(long photoId);

        Task<HashSet<PrintDto>> GetPrints();
        Task<PrintDto> AddOrUpdatePrint(PrintDto printInput);
        Task DeletePrint(long printId);
        Task AsignPrint(PrintInputDto print);
    }
}