﻿using Abp.AutoMapper;
using Abp.Domain.Entities;
using Personeria.Areas.Dto;
using Personeria.Domain.Relation;
using Personeria.FileAttachTypes.Dto;
using Personeria.TransactTypes.Dto;

namespace Personeria.TransactTypeFileAttachTypes.Dto
{
    [AutoMapFrom(typeof(TransactTypeFileAttachType))]
    public class TransactTypeFileAttachTypeGetDto : Entity<long>
    {
        public TransactTypeFileAttachTypeGetDto()
        {
            IsRequired = false;
        }

        public long FileAttachTypeId { get; set; }
        public virtual FileAttachTypeDto FileAttachType { get; set; }

        public long TransactTypeId { get; set; }
        public virtual TransactTypeDto TransactType { get; set; }

        public bool IsRequired { get; set; }
    }
}
