﻿using Abp.AutoMapper;
using Abp.Domain.Entities;
using Personeria.Domain.Relation;

namespace Personeria.TransactTypeFileAttachTypes.Dto
{
    [AutoMap(typeof(TransactTypeFileAttachType))]
    public class TransactTypeFileAttachTypeDto : Entity<long>
    {
        public TransactTypeFileAttachTypeDto()
        {
            IsRequired = false;
        }

        public long FileAttachTypeId { get; set; }

        public long TransactTypeId { get; set; }

        public string FileAttachTypeName { get; set; }

        public bool IsRequired { get; set; }
    }
}
