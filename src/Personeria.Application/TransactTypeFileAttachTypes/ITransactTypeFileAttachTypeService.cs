﻿using Abp.Application.Services;
using Personeria.Base;
using Personeria.TransactTypeFileAttachTypes.Dto;
using System.Collections.Generic;

namespace Personeria.TransactTypeFileAttachTypes
{
    public interface ITransactTypeFileAttachTypeService:IApplicationService
    {
        TransactTypeFileAttachTypeDto GetById(long id);

        List<TransactTypeFileAttachTypeDto> GetByTransactTypeId(long TransactTypeId);

        ContextDto<TransactTypeFileAttachTypeGetDto> GetAll(SearchDto filters);

        long AddOrUpdate(TransactTypeFileAttachTypeDto dto);

        void Delete(long id);

    }
}
