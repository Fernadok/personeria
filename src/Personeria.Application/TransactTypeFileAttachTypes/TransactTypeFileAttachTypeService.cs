﻿using System;
using System.Collections.Generic;
using System.Linq;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using NinjaNye.SearchExtensions;
using PagedList;
using Personeria.Base;
using Personeria.Domain.Relation;
using Personeria.Extensions;
using Personeria.TransactTypeFileAttachTypes.Dto;

namespace Personeria.TransactTypeFileAttachTypes
{
    public class TransactTypeFileAttachTypeService : PersoneriaAppServiceBase, ITransactTypeFileAttachTypeService
    {
        private readonly IRepository<TransactTypeFileAttachType, long> _TransactTypeFileAttachTypeRepository;

        public TransactTypeFileAttachTypeService(IRepository<TransactTypeFileAttachType, long> TransactTypeFileAttachTypeRepository)
        {
            _TransactTypeFileAttachTypeRepository = TransactTypeFileAttachTypeRepository;
        }

        [UnitOfWork(isTransactional: true)]
        public long AddOrUpdate(TransactTypeFileAttachTypeDto dto)
        {
            var isnew = _TransactTypeFileAttachTypeRepository.FirstOrDefault(x => x.Id == dto.Id);

            var ar = dto.MapTo(isnew);
            _TransactTypeFileAttachTypeRepository.InsertOrUpdate(ar);
            CurrentUnitOfWork.SaveChanges();

            return ar.Id;
        }

        [UnitOfWork(isTransactional: true)]
        public void Delete(long id)
        {
            var trans = _TransactTypeFileAttachTypeRepository.FirstOrDefault(x => x.Id == id);
            if (trans != null)
            {
                trans.IsActive = false;
                _TransactTypeFileAttachTypeRepository.Update(trans);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public ContextDto<TransactTypeFileAttachTypeGetDto> GetAll(SearchDto filters)
        {
            filters.Search = string.IsNullOrEmpty(filters.Search) ? "" : filters.Search.ToLower();
            var resultList = _TransactTypeFileAttachTypeRepository
                    .GetAllIncluding(c => c.FileAttachType, a => a.TransactType, b => b.TransactType.TransactTypeAreaRelations)
                    .Where(x => x.IsActive)
                    .Search(x => x.FileAttachType.Name)
                    .Search(x => x.TransactType.Name)
                    .OrderByTo(filters.OrderColumn, filters.Order.ToLower() == "desc")
                    .ToPagedList(filters.Page, filters.PageSize);

            return new ContextDto<TransactTypeFileAttachTypeGetDto>
            {
                Data = resultList.Select(x => x.MapTo<TransactTypeFileAttachTypeGetDto>()),
                MetaData = resultList.GetMetaData()
            };
        }

        public TransactTypeFileAttachTypeDto GetById(long id)
        {
            var trans = _TransactTypeFileAttachTypeRepository.FirstOrDefault(x => x.Id == id);
            return trans?.MapTo<TransactTypeFileAttachTypeDto>();
        }

        public List<TransactTypeFileAttachTypeDto> GetByTransactTypeId(long TransactTypeId)
        {
            var trans = _TransactTypeFileAttachTypeRepository
                        .GetAllIncluding(s => s.FileAttachType)
                        .Where(x => x.TransactTypeId == TransactTypeId)
                        .ToList();

            var ret = trans.Select(x => x.MapTo<TransactTypeFileAttachTypeDto>());

            return ret.ToList();
        }

    }
}
