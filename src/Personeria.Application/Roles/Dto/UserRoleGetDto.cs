﻿using Abp.Authorization.Users;
using Abp.AutoMapper;

namespace Personeria.Roles.Dto
{
    [AutoMap(typeof(UserRole))]
    public class UserRoleGetDto
    {
        public int RoleId { get; set; }
        public long UserId { get; set; }
    }
}
