﻿using Abp.AutoMapper;
using Abp.Domain.Entities;

namespace Personeria.UserArea.Dto
{
    [AutoMap(typeof(Personeria.Domain.Relation.UserArea))]
    public class UserAreaDto : Entity<long>
    {
        public long UserId { get; set; }

        public long AreaId { get; set; }
    }
}
