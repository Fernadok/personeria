﻿using Abp.Application.Services;
using Personeria.Base;
using Personeria.UserArea.Dto;
using System.Collections.Generic;

namespace Personeria.UserArea
{
    public interface IUserAreaService: IApplicationService
    {
        List<Personeria.Domain.Relation.UserArea> GetAllByUserId(long id);

        ContextDto<UserAreaDto> GetAll(SearchDto filters);

        long AddOrUpdate(UserAreaDto dto);

        void Delete(long id);
    }
}
