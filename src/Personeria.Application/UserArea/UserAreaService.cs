﻿using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Personeria.Base;
using Personeria.UserArea.Dto;
using System.Collections.Generic;
using System.Linq;

namespace Personeria.UserArea
{
    [AbpAuthorize]
    public class UserAreaService : PersoneriaAppServiceBase, IUserAreaService
    {
        private readonly IRepository<Personeria.Domain.Relation.UserArea, long> _UserAreaRepository;

        public UserAreaService(IRepository<Personeria.Domain.Relation.UserArea, long> userAreaRepository)
        {
            _UserAreaRepository = userAreaRepository;
        }

        [UnitOfWork(isTransactional: true)]
        public long AddOrUpdate(UserAreaDto dto)
        {
            var isnew = _UserAreaRepository.FirstOrDefault(x => x.Id == dto.Id);

            var ar = dto.MapTo(isnew);
            _UserAreaRepository.InsertOrUpdate(ar);
            CurrentUnitOfWork.SaveChanges();

            return ar.Id;
        }

        [UnitOfWork(isTransactional: true)]
        public void Delete(long id)
        {
            throw new System.NotImplementedException();
        }

        public ContextDto<UserAreaDto> GetAll(SearchDto filters)
        {
            throw new System.NotImplementedException();
        }

        public List<Domain.Relation.UserArea> GetAllByUserId(long id)
        {
            var ar = _UserAreaRepository.GetAll()
                          .Where(x => x.UserId == id)
                          .ToList();
            return ar;
        }
    }
}
