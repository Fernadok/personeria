﻿using Abp.Auditing;
using Abp.Authorization;
using Abp.Dependency;
using Abp.Domain.Repositories;
using System;
using System.IO;
using Personeria.Domain;
using Personeria.Extensions;
using Personeria.FileAttachs.Dto;
using Personeria.Providers;
using System.Drawing;
using System.Drawing.Imaging;
using Abp.Domain.Uow;

namespace Personeria.FileAttachs
{
    //[AbpAuthorize]
    public class FileAttachService : IFileAttachService, ISingletonDependency
    {
        private readonly INetWorkProvider _netWorkProvider;
        private readonly string _pathFolderMedia;
        private readonly string _pathWebFolderMedia;
        private readonly IRepository<FileAttach,long> _fileAttach;

        public FileAttachService(INetWorkProvider netWorkProvider, IRepository<FileAttach, long> fileAttach)
        {
            _netWorkProvider = netWorkProvider;

            _pathWebFolderMedia = _netWorkProvider.GetAppSettings("FilesFolder");
            _pathFolderMedia = _netWorkProvider.MapPath(_pathWebFolderMedia);
            _fileAttach = fileAttach;
        }

        [DisableAuditing]
        public FileAttachDto GetFileAttachFromBase64(string base64Picture)
        {
            if (!string.IsNullOrWhiteSpace(base64Picture) && base64Picture.StartsWith("data:image"))
            {
                long size;
                var file = Base64ToImage(base64Picture, out size);
                if (file != null)
                {
                    var pathServer = _netWorkProvider.GetAppSettings("FilesFolder");

                    var fileName = Guid.NewGuid() + ".jpg";
                    var path = pathServer + "/" + fileName;
                    var pathAndFile = _netWorkProvider.MapPath(path);

                    file.Save(pathAndFile);

                    return new FileAttachDto()
                    {
                        FileName = fileName,
                        Path = path,
                        Size = size
                    };
                }
            }
            return null;
        }
                
        private Image Base64ToImage(string base64str, out long size)
        {
            try
            {
                var clearBase64 = base64str.Replace("data:image/png;base64,", "");
                byte[] imageBytes = Convert.FromBase64String(clearBase64);
                MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);

                ms.Write(imageBytes, 0, imageBytes.Length);
                Image img = Image.FromStream(ms, true);
                size = ms.Length;
                return img;
            }
            catch (Exception)
            {
                size = 0;
                return null;
            }
        }

        [DisableAuditing]
        public FileAttach Upload(string name, byte[] fileBytes, bool save = false)
        {
            long size = 0;
            var path = "";
            var token = Guid.NewGuid().ToString();
            var fileName = token + name.GetExtension();

            try
            {
                var pathAndFile = Path.Combine(_pathFolderMedia, fileName);
                File.WriteAllBytes(pathAndFile, fileBytes);

                path = _pathWebFolderMedia + "/" + fileName;
                size = fileBytes.Length;

                var file = new FileAttach() { FileName = name, Size = size, Path = path};

                if (save)
                {
                    file = _fileAttach.Insert(file);
                }

                return file;
            }
            catch (Exception ex)
            {
                size = 0;
                path = "";
                name = ex.Message;
            }

            return null;
        }

        [DisableAuditing]
        [AbpAuthorize]
        [UnitOfWork]
        public FileAttach Upload(string name, byte[] fileBytes, bool save = false, string typePhoto = PersoneriaConsts.PersonKey)
        {
            long size = 0;
            var path = "";
            var token = Guid.NewGuid().ToString();
            var fileName = token + name.GetExtension();

            try
            {
                var pathAndFile = Path.Combine(_pathFolderMedia, fileName);
                File.WriteAllBytes(pathAndFile, fileBytes);

                path = _pathWebFolderMedia + "/" + fileName;
                size = fileBytes.Length;

                var file = new FileAttach() { FileName = name, Size = size, Path = path, Store = typePhoto };

                if (save)
                {
                    file = _fileAttach.Insert(file);                    
                }

                return file;
            }
            catch (Exception ex)
            {
                size = 0;
                path = "";
                name = ex.Message;
            }

            return null;
        }
    }
}
