﻿using Personeria.Domain;

namespace Personeria.FileAttachs
{
    public interface IFileAttachService
    {
        FileAttach Upload(string name, byte[] fileBytes, bool save = false);

        FileAttach Upload(string name, byte[] fileBytes, bool save = false, string typePhoto = PersoneriaConsts.PersonKey);
    }
}
