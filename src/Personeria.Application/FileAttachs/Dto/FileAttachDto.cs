﻿using Abp.AutoMapper;
using Personeria.Base;
using Personeria.Domain;
using Personeria.Extensions;

namespace Personeria.FileAttachs.Dto
{
    [AutoMap(typeof(FileAttach))]
    public class FileAttachDto : EntityDto
    {
        public string Title { get; set; }
        public string Link { get; set; }

        public string Path { get; set; }
        public long Size { get; set; }
        public string FileName { get; set; }
        public string Store { get; set; }


        public string Extension => Path.GetExtension();

        public string MimeType => Path.GetMimeType();

        public bool IsFileOfDomain => !Path.StartsWith("http");

        public bool IsImage => FileName.IsImage();

        public bool IsVideo => FileName.IsVideo();

        public string PathToLower => Path.ToLower();


        public string SizeTranslated
        {
            get
            {
                string[] sizes = { "B", "KB", "MB", "GB" };
                double len = Size;
                int order = 0;
                while (len >= 1024 && order + 1 < sizes.Length)
                {
                    order++;
                    len = len / 1024;
                }
                return string.Format("{0:0.##} {1}", len, sizes[order]);
            }
        }


    }
}
