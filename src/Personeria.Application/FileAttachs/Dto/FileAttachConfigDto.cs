﻿using Abp.AutoMapper;
using Personeria.Domain;

namespace Personeria.FileAttachs.Dto
{
    [AutoMapTo(typeof(FileAttach))]
    public class FileAttachConfigDto
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
    }
}
