﻿using Abp.Application.Services;
using System.Collections.Generic;
using Personeria.Base;
using Personeria.Persons.Dto;

namespace Personeria.Persons
{
    public interface IPersonService : IApplicationService
    {
        PersonDto GetById(long id);

        PersonDto GetByDocIdentity(string numIdentity);

        ContextDto<PersonDto> GetAll(SearchDto filters);

        long AddOrUpdate(PersonDto dto);

        void Delete(long id);

        List<IdDescription> PersonList();
    }
}
