﻿using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using NinjaNye.SearchExtensions;
using PagedList;
using System.Collections.Generic;
using System.Linq;
using Personeria.Base;
using Personeria.Domain;
using Personeria.Extensions;
using Personeria.Persons.Dto;

namespace Personeria.Persons
{
   // [AbpAuthorize]
    public class PersonService: PersoneriaAppServiceBase, IPersonService
    {
        private readonly IRepository<Person, long> _PersonRepository;

        public PersonService(IRepository<Person, long> PersonRepository)
        {
            _PersonRepository = PersonRepository;
        }

        [UnitOfWork(isTransactional: true)]
        public long AddOrUpdate(PersonDto dto)
        {
            var isnew = _PersonRepository.FirstOrDefault(x => x.Id == dto.Id);
            // -- Verifico si el nombre ya se encuentra registrado
            if (isnew != null && isnew.NumberIdentity.ToLower() != dto.NumberIdentity.ToLower())
            {
                var existother = _PersonRepository.FirstOrDefault(x => x.Id != isnew.Id && x.NumberIdentity.ToLower() == dto.NumberIdentity.ToLower());
                if (existother != null)
                {
                    throw new UserFriendlyException("Ya se encuentra registrada una Person con el mismo documento.");
                }
            }
            else if (isnew == null)
            {
                var existother = _PersonRepository.FirstOrDefault(x => x.NumberIdentity.ToLower() == dto.NumberIdentity.ToLower());
                if (existother != null)
                {
                    throw new UserFriendlyException("Ya se encuentra registrada una Person con el mismo documento.");
                }
            }
            var person = dto.MapTo(isnew);
            _PersonRepository.InsertOrUpdate(person);
            CurrentUnitOfWork.SaveChanges();

            return person.Id;
        }

        [UnitOfWork(isTransactional: true)]
        public void Delete(long id)
        {
            var Person = _PersonRepository.FirstOrDefault(x => x.Id == id);
            if (Person != null)
            {
                Person.IsActive = false;
                _PersonRepository.Update(Person);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public ContextDto<PersonDto> GetAll(SearchDto filters)
        {
            filters.Search = string.IsNullOrEmpty(filters.Search) ? "" : filters.Search.ToLower();
            var resultList = _PersonRepository
                    .GetAll()
                    .Where(x => x.IsActive)
                    .Search(x => x.Name)
                    .Search(x => x.Surname)
                    .Search(x => x.NumberIdentity)
                    .Containing(filters.Search)
                    .OrderByTo(filters.OrderColumn, filters.Order.ToLower() == "desc")
                    .ToPagedList(filters.Page, filters.PageSize);

            return new ContextDto<PersonDto>
            {
                Data = resultList.Select(x => x.MapTo<PersonDto>()),
                MetaData = resultList.GetMetaData()
            };
        }

        public PersonDto GetById(long id)
        {
            var person = _PersonRepository.FirstOrDefault(x => x.Id == id);
            return person?.MapTo<PersonDto>();
        }

        public PersonDto GetByDocIdentity(string numIdentity)
        {
            var person = _PersonRepository.FirstOrDefault(x => x.NumberIdentity == numIdentity);
            return person?.MapTo<PersonDto>();
        }

        public List<IdDescription> PersonList()
        {
            return _PersonRepository.GetAll()
              .ToList()
              .Where(x => x.IsActive)
              .Select(x => new IdDescription
              {
                  Id = x.Id,
                  Description = string.Format("({0}) {1} {2}",x.NumberIdentity,x.Surname,x.Name)
              }).OrderBy(x => x.Description).ToList();
        }
    }
}
