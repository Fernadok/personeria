﻿using Abp.AutoMapper;
using Abp.Domain.Entities;
using Personeria.Domain;

namespace Personeria.Persons.Dto
{
    [AutoMap(typeof(Person))]
    public class PersonDto : Entity<long>
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string NumberIdentity { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Adreess { get; set; }

        public long LocationId { get; set; }
    }
}
