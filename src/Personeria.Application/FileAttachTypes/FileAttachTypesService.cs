﻿using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using NinjaNye.SearchExtensions;
using PagedList;
using System.Collections.Generic;
using System.Linq;
using Personeria.Base;
using Personeria.Domain;
using Personeria.Extensions;
using Personeria.FileAttachTypes.Dto;

namespace Personeria.FileAttachTypes
{
   // [AbpAuthorize]
    public class FileAttachTypeService: PersoneriaAppServiceBase, IFileAttachTypeService
    {
        private readonly IRepository<FileAttachType, long> _FileAttachTypeRepository;

        public FileAttachTypeService(IRepository<FileAttachType, long> FileAttachTypeRepository)
        {
            _FileAttachTypeRepository = FileAttachTypeRepository;
        }

        [UnitOfWork(isTransactional: true)]
        public long AddOrUpdate(FileAttachTypeDto dto)
        {
            var isnew = _FileAttachTypeRepository.FirstOrDefault(x => x.Id == dto.Id);
            // -- Verifico si el nombre ya se encuentra registrado
            if (isnew != null && isnew.Name.ToLower() != dto.Name.ToLower())
            {
                var existother = _FileAttachTypeRepository.FirstOrDefault(x => x.Id != isnew.Id && x.Name.ToLower() == dto.Name.ToLower());
                if (existother != null)
                {
                    throw new UserFriendlyException("Ya se encuentra registrada un tipo de archivo con el mismo nombre, intente ponerle otro nombre.");
                }
            }
            else if (isnew == null)
            {
                var existother = _FileAttachTypeRepository.FirstOrDefault(x => x.Name.ToLower() == dto.Name.ToLower());
                if (existother != null)
                {
                    throw new UserFriendlyException("Ya se encuentra registrada un tipo de archivo con el mismo nombre, intente ponerle otro nombre.");
                }
            }
            var fileType = dto.MapTo(isnew);
            _FileAttachTypeRepository.InsertOrUpdate(fileType);
            CurrentUnitOfWork.SaveChanges();

            return fileType.Id;
        }

        [UnitOfWork(isTransactional: true)]
        public void Delete(long id)
        {
            var FileAttachType = _FileAttachTypeRepository.FirstOrDefault(x => x.Id == id);
            if (FileAttachType != null)
            {
                FileAttachType.IsActive = false;
                _FileAttachTypeRepository.Update(FileAttachType);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public ContextDto<FileAttachTypeDto> GetAll(SearchDto filters)
        {
            filters.Search = string.IsNullOrEmpty(filters.Search) ? "" : filters.Search.ToLower();
            var resultList = _FileAttachTypeRepository
                    .GetAll()
                    .Where(x => x.IsActive)
                    .Search(x => x.Name)
                    .Containing(filters.Search)
                    .OrderByTo(filters.OrderColumn, filters.Order.ToLower() == "desc")
                    .ToPagedList(filters.Page, filters.PageSize);

            return new ContextDto<FileAttachTypeDto>
            {
                Data = resultList.Select(x => x.MapTo<FileAttachTypeDto>()),
                MetaData = resultList.GetMetaData()
            };
        }

        public FileAttachTypeDto GetById(long id)
        {
            var fileType = _FileAttachTypeRepository.FirstOrDefault(x => x.Id == id);
            return fileType?.MapTo<FileAttachTypeDto>();
        }

        public List<IdDescription> FileAttachTypeList()
        {
            return _FileAttachTypeRepository.GetAll()
              .ToList()
              .Where(x => x.IsActive)
              .Select(x => new IdDescription
              {
                  Id = x.Id,
                  Description = x.Name
              }).OrderBy(x => x.Description).ToList();
        }
    }
}
