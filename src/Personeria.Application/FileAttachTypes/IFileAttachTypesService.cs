﻿using Abp.Application.Services;
using System.Collections.Generic;
using Personeria.Base;
using Personeria.FileAttachTypes.Dto;

namespace Personeria.FileAttachTypes
{
    public interface IFileAttachTypeService : IApplicationService
    {
        FileAttachTypeDto GetById(long id);

        ContextDto<FileAttachTypeDto> GetAll(SearchDto filters);

        long AddOrUpdate(FileAttachTypeDto dto);

        void Delete(long id);

        List<IdDescription> FileAttachTypeList();
    }
}
