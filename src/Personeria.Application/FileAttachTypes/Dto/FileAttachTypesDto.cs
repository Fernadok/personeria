﻿using Abp.AutoMapper;
using Abp.Domain.Entities;
using Personeria.Domain;

namespace Personeria.FileAttachTypes.Dto
{
    [AutoMap(typeof(FileAttachType))]
    public class FileAttachTypeDto : Entity<long>
    {
        public string Name { get; set; }
    }
}
