﻿using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Personeria.Base;
using Personeria.Domain;
using Personeria.Email;
using Personeria.Enums;
using Personeria.Histories.Dto;
using Personeria.ProcessFlow.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Personeria.ProcessFlow
{
    // [AbpAuthorize]
    public class ProcessFlowService : PersoneriaAppServiceBase, IProcessFlowService
    {
        private readonly IRepository<Transact, long> _transactRepository;
        private readonly IRepository<TransactType, long> _transactTypeRepository;
        private readonly IEmailService _emailService;
        private readonly IRepository<Area, long> _areaRepository;

        public ProcessFlowService(IRepository<Transact, long> transact,
                                  IRepository<TransactType, long> transactTypeRepository,
                                  IEmailService emailService,
                                  IRepository<Area, long> areaRepository)
        {
            _transactRepository = transact;
            _transactTypeRepository = transactTypeRepository;
            _emailService = emailService;
            _areaRepository = areaRepository;
        }

        public async Task<long> ChangeState(ChangeProccessFlow dto)
        {
            var entity = _transactRepository.GetAllIncluding(x => x.Area, x => x.TransactType)
                          .FirstOrDefault(x => x.Id == dto.TransactId && x.IsActive);

            entity.State = dto.State;
            if (dto.State == TransactState.Finished)
            {
                var TransactType = _transactTypeRepository
                                    .GetAllIncluding(x => x.TransactTypeAreaRelations)
                                    .FirstOrDefault(x => x.Id == entity.TransactTypeId)
                                    .TransactTypeAreaRelations;

                var OrderNow = TransactType.FirstOrDefault(o => o.AreaId == entity.AreaId);

                var changeArea = TransactType.FirstOrDefault(o => o.Order > OrderNow.Order);
                if (changeArea == null)
                {
                    entity.State = TransactState.Finished;
                }
                else
                {
                    entity.State = TransactState.InProcess;
                    entity.AreaId = changeArea.AreaId;
                }

                
            }
            else if (dto.State == TransactState.Locked && dto.RevisionRequest)
            {
                entity.Revision = Guid.NewGuid();
                //Send Meil to User
            }

            _transactRepository.Update(entity);
            CurrentUnitOfWork.SaveChanges();

            #region Historial
            var _historyTransact = new HistoryDto
            {
                CreateDate = DateTime.Now,
                Ip = "",
                UserId = AbpSession.UserId,
                TransactId = dto.TransactId,
                State = entity.State,
                Observation = entity.State == TransactState.Finished ?
                string.Format(HistoryMessages.FinishTransact
                                , dto.TransactId
                                , entity.Area.Name.ToUpper()
                                , L(entity.State.ToString()).ToUpper()) + " | " + dto.Observation
               : entity.State == TransactState.Locked && dto.RevisionRequest ?
               string.Format(HistoryMessages.RevisionTransact
                                , dto.TransactId
                                , entity.Area.Name.ToUpper()
                                , L(entity.State.ToString()).ToUpper()) + " | " + dto.Observation
               :string.Format(HistoryMessages.ChangeTransact
                                , dto.TransactId
                                , entity.Area.Name.ToUpper()
                                , L(entity.State.ToString()).ToUpper()) + " | " + dto.Observation
            };
            HistoryService.AddOrUpdate(_historyTransact);
            CurrentUnitOfWork.SaveChanges();
            #endregion

            if (dto.Notify)
            {
                var subject = $"PERSONERIA | Aviso de Trámite N° #{entity.Id}";
                var email = entity.Person.Email;
                var name = entity.Person.Name;
                var surname = entity.Person.Surname;
                var message = _historyTransact.Observation;
                var url = $"{WebDomainProvider.CurrentDomain}/tramites/{entity.Id}";
                _emailService.SendMessage(subject, email, surname, name, message, url);
            }

            if (dto.RevisionRequest)
            {
                var subject = $"PERSONERIA | Aviso de Trámite N° #{entity.Id}";
                var email = entity.Person.Email;
                var name = entity.Person.Name;
                var surname = entity.Person.Surname;
                var message = _historyTransact.Observation;
                var url = $"{WebDomainProvider.CurrentDomain}/revision/{entity.Revision}";
                _emailService.SendMessage(subject, email, surname, name, message, url);
            }

            await NotificationService.Publish_SentNotForChangeTransact(entity.Id, true);
            return entity.Id;
        }

        public async Task<long> OpenFlow(ChangeProccessFlow dto)
        {
            var entity = _transactRepository.GetAllIncluding(x => x.Area, x => x.TransactType)
                          .FirstOrDefault(x => x.Id == dto.TransactId && x.IsActive);

            var TransactType = _transactTypeRepository
                                .GetAllIncluding(x => x.TransactTypeAreaRelations)
                                .FirstOrDefault(x => x.Id == entity.TransactTypeId)
                                .TransactTypeAreaRelations;


            var changeArea = TransactType.FirstOrDefault(o => o.Order == 1);

            entity.State = TransactState.InProcess;
            entity.AreaId = changeArea.AreaId;

            _transactRepository.Update(entity);
            CurrentUnitOfWork.SaveChanges();

            #region Historial
            var _historyTransact = new HistoryDto
            {
                CreateDate = DateTime.Now,
                Ip = "",
                UserId = AbpSession.UserId,
                TransactId = dto.TransactId,
                State = entity.State,
                Observation = string.Format(HistoryMessages.ReOpenTransact
                                                        , dto.TransactId
                                                        , entity.Area.Name.ToUpper()
                                                        , L(Enums.TransactState.InProcess.ToString()).ToUpper())
            };
            HistoryService.AddOrUpdate(_historyTransact);
            CurrentUnitOfWork.SaveChanges();
            #endregion

            if (dto.Notify)
            {
                var subject = $"PERSONERIA | Aviso de Trámite N° #{entity.Id}";
                var email = entity.Person.Email;
                var name = entity.Person.Name;
                var surname = entity.Person.Surname;
                var message = string.Format(HistoryMessages.ReOpenTransact
                                                        , dto.TransactId
                                                        , entity.Area.Name.ToUpper()
                                                        , L(Enums.TransactState.InProcess.ToString()).ToUpper());
                var url = $"{WebDomainProvider.CurrentDomain}/tramites/{entity.Id}";
                _emailService.SendMessage(subject, email, surname, name, message, url);
            }

            await NotificationService.Publish_SentNotForChangeTransact(entity.Id, true);
            return entity.Id;
        }

        public async Task<long> BackArea(ChangeProccessFlow dto)
        {
            var entity = _transactRepository
                          .FirstOrDefault(x => x.Id == dto.TransactId && x.IsActive);

            if ((long)dto.State == -3)
            {
                var TransactType = _transactTypeRepository
                          .GetAllIncluding(x => x.TransactTypeAreaRelations)
                          .FirstOrDefault(x => x.Id == entity.TransactTypeId)
                          .TransactTypeAreaRelations;



                var OrderNow = TransactType.FirstOrDefault(o => o.AreaId == entity.AreaId);
                var OrderBack = OrderNow.Order;

                var changeArea = TransactType.FirstOrDefault(o => o.Order == OrderBack - 1);

                entity.State = TransactState.InProcess;
                entity.AreaId = changeArea.AreaId;
            }
            else
            {
                entity.State = TransactState.InProcess;
                entity.AreaId = entity.LockedAreaId.Value;
                entity.LockedAreaId = null;
            }


            _transactRepository.Update(entity);
            CurrentUnitOfWork.SaveChanges();

            var area = _areaRepository.FirstOrDefault(x => x.Id == entity.AreaId && x.IsActive);

            #region Historial
            var _historyTransact = new HistoryDto
            {
                CreateDate = DateTime.Now,
                Ip = "",
                UserId = AbpSession.UserId,
                TransactId = dto.TransactId,
                State = entity.State,
                Observation = string.Format(HistoryMessages.BackArea
                                                        , dto.TransactId
                                                        , area.Name.ToUpper()
                                                        , L(Enums.TransactState.InProcess.ToString()).ToUpper())
            };
            HistoryService.AddOrUpdate(_historyTransact);
            CurrentUnitOfWork.SaveChanges();
            #endregion

            if (dto.Notify)
            {
                var subject = $"PERSONERIA | Aviso de Trámite N° #{entity.Id}";
                var email = entity.Person.Email;
                var name = entity.Person.Name;
                var surname = entity.Person.Surname;
                var message = string.Format(HistoryMessages.BackArea
                                                        , dto.TransactId
                                                        , area.Name.ToUpper()
                                                        , L(Enums.TransactState.InProcess.ToString()).ToUpper());
                var url = $"{WebDomainProvider.CurrentDomain}/tramites/{entity.Id}";
                _emailService.SendMessage(subject, email, surname, name, message, url);
            }

            await NotificationService.Publish_SentNotForChangeTransact(entity.Id, true);
            return entity.Id;
        }

        public async Task<long> SendOtherArea(ChangeProccessFlow dto)
        {
            var entity = _transactRepository
                          .FirstOrDefault(x => x.Id == dto.TransactId && x.IsActive);

            entity.LockedAreaId = entity.AreaId;
            entity.AreaId = dto.SendAreaId.Value;

            _transactRepository.Update(entity);
            CurrentUnitOfWork.SaveChanges();

            var area = _areaRepository.FirstOrDefault(x => x.Id == entity.AreaId && x.IsActive);

            #region Historial
            var _historyTransact = new HistoryDto
            {
                CreateDate = DateTime.Now,
                Ip = "",
                UserId = AbpSession.UserId,
                TransactId = dto.TransactId,
                State = entity.State,
                Observation = string.Format(HistoryMessages.SendOtherArea
                                                        , dto.TransactId
                                                        , area.Name.ToUpper()
                                                        , L(TransactState.InProcess.ToString()).ToUpper())
            };
            HistoryService.AddOrUpdate(_historyTransact);
            CurrentUnitOfWork.SaveChanges();
            #endregion

            if (dto.Notify)
            {
                var subject = $"PERSONERIA | Aviso de Trámite N° #{entity.Id}";
                var email = entity.Person.Email;
                var name = entity.Person.Name;
                var surname = entity.Person.Surname;
                var message = string.Format(HistoryMessages.SendOtherArea
                                                        , dto.TransactId
                                                        , area.Name.ToUpper()
                                                        , L(Enums.TransactState.InProcess.ToString()).ToUpper());
                var url = $"{WebDomainProvider.CurrentDomain}/tramites/{entity.Id}";
                _emailService.SendMessage(subject, email, surname, name, message, url);
            }

            await NotificationService.Publish_SentNotForChangeTransact(entity.Id, true);
            return entity.Id;
        }


    }
}
