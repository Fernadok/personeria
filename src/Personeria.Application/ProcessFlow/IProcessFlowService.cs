﻿using Abp.Application.Services;
using Personeria.ProcessFlow.Dto;
using System.Threading.Tasks;

namespace Personeria.ProcessFlow
{
    public interface IProcessFlowService : IApplicationService
    {
        Task<long> ChangeState(ChangeProccessFlow dto);

        Task<long> OpenFlow(ChangeProccessFlow dto);

        Task<long> BackArea(ChangeProccessFlow dto);

        Task<long> SendOtherArea(ChangeProccessFlow dto);
    }
}
