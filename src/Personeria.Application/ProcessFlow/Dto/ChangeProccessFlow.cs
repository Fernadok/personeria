﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Personeria.ProcessFlow.Dto
{
    public class ChangeProccessFlow
    {
        public ChangeProccessFlow()
        {
            Notify = false;
            RevisionRequest = false;
        }

        public long TransactId { get; set; }
        public Enums.TransactState State { get; set; }
        public string Observation { get; set; }
        public bool Notify { get; set; }
        public long? SendAreaId { get; set; }
        public bool RevisionRequest { get; set; }
    }
}
