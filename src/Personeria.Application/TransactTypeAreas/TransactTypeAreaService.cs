﻿using System;
using System.Collections.Generic;
using System.Linq;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using NinjaNye.SearchExtensions;
using PagedList;
using Personeria.Base;
using Personeria.Domain.Relation;
using Personeria.Extensions;
using Personeria.TransactTypeAreas.Dto;

namespace Personeria.TransactTypeAreas
{
    public class TransactTypeAreaService : PersoneriaAppServiceBase, ITransactTypeAreaService
    {
        private readonly IRepository<TransactTypeArea, long> _TransactTypeAreaRepository;

        public TransactTypeAreaService(IRepository<TransactTypeArea, long> TransactTypeAreaRepository)
        {
            _TransactTypeAreaRepository = TransactTypeAreaRepository;
        }

        [UnitOfWork(isTransactional: true)]
        public long AddOrUpdate(TransactTypeAreaDto dto)
        {
            var isnew = _TransactTypeAreaRepository.FirstOrDefault(x => x.Id == dto.Id);

            var ar = dto.MapTo(isnew);
            _TransactTypeAreaRepository.InsertOrUpdate(ar);
            CurrentUnitOfWork.SaveChanges();

            return ar.Id;
        }

        [UnitOfWork(isTransactional: true)]
        public void Delete(long id)
        {
            var trans = _TransactTypeAreaRepository.FirstOrDefault(x => x.Id == id);
            if (trans != null)
            {
                trans.IsActive = false;
                _TransactTypeAreaRepository.Update(trans);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public ContextDto<TransactTypeAreaGetDto> GetAll(SearchDto filters)
        {
            filters.Search = string.IsNullOrEmpty(filters.Search) ? "" : filters.Search.ToLower();
            var resultList = _TransactTypeAreaRepository
                    .GetAllIncluding(c => c.Area, a => a.TransactType)
                    .Where(x => x.IsActive)
                    .Search(x => x.Area.Name)
                    .Search(x => x.TransactType.Name)
                    .Containing(filters.Search)
                    .OrderByTo(filters.OrderColumn, filters.Order.ToLower() == "desc")
                    .ToPagedList(filters.Page, filters.PageSize);

            return new ContextDto<TransactTypeAreaGetDto>
            {
                Data = resultList.Select(x => x.MapTo<TransactTypeAreaGetDto>()),
                MetaData = resultList.GetMetaData()
            };
        }

        public TransactTypeAreaDto GetById(long id)
        {
            var trans = _TransactTypeAreaRepository.FirstOrDefault(x => x.Id == id);
            return trans?.MapTo<TransactTypeAreaDto>();
        }

        public List<IdDescription> TransactTypeAreaList()
        {
            return _TransactTypeAreaRepository.GetAll()
             .ToList()
             .Where(x => x.IsActive)
             .Select(x => new IdDescription
             {
                 Id = x.Id,
                 Description = x.Area.Name
             }).OrderBy(x => x.Description).ToList();
        }

        public TransactTypeArea GetFirstAreaByTransactType(long IdTransactType)
        {
            var trans = _TransactTypeAreaRepository.FirstOrDefault(x => x.TransactTypeId == IdTransactType && x.Order == 1);
            return trans;
        }
    }
}
