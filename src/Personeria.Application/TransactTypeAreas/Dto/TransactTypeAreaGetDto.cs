﻿using Abp.AutoMapper;
using Abp.Domain.Entities;
using Personeria.Areas.Dto;
using Personeria.Domain.Relation;
using Personeria.TransactTypes.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Personeria.TransactTypeAreas.Dto
{
    [AutoMapFrom(typeof(TransactTypeArea))]
    public class TransactTypeAreaGetDto : Entity<long>
    {
        public long TransactTypeId { get; set; }
        public virtual TransactTypeDto TransactType { get; set; }

        public long AreaId { get; set; }
        public virtual AreaDto Area { get; set; }

        public int Order { get; set; }
    }
}
