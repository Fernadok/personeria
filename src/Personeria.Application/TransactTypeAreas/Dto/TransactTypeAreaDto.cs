﻿using Abp.AutoMapper;
using Personeria.Domain.Relation;

namespace Personeria.TransactTypeAreas.Dto
{
    [AutoMap(typeof(TransactTypeArea))]
    public class TransactTypeAreaDto
    {
        public long Id { get; set; }

        public long TransactTypeId { get; set; }

        public long AreaId { get; set; }

        public string AreaName { get; set; }

        public int Order { get; set; }
    }
}
