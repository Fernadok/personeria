﻿using Abp.Application.Services;
using Personeria.Base;
using Personeria.Domain.Relation;
using Personeria.TransactTypeAreas.Dto;
using System.Collections.Generic;

namespace Personeria.TransactTypeAreas
{
    public interface ITransactTypeAreaService:IApplicationService
    {
        TransactTypeAreaDto GetById(long id);

        ContextDto<TransactTypeAreaGetDto> GetAll(SearchDto filters);

        long AddOrUpdate(TransactTypeAreaDto dto);

        void Delete(long id);

        List<IdDescription> TransactTypeAreaList();

        TransactTypeArea GetFirstAreaByTransactType(long IdTransactType);
    }
}
