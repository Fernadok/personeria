using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Personeria.Authorization;
using Personeria.Authorization.Roles;
using Personeria.Authorization.Users;
using Personeria.Roles.Dto;
using Personeria.Users.Dto;
using Microsoft.AspNet.Identity;
using System.Linq.Expressions;
using System;
using PagedList;
using Personeria.Base;
using Abp.AutoMapper;
using Personeria.UserArea;
using Personeria.Configuration;
using Personeria.Notifications;

namespace Personeria.Users
{
    [AbpAuthorize(PermissionNames.Pages_Users)]
    public class UserAppService : AsyncCrudAppService<User, UserDto, long, PagedResultRequestDto, CreateUserDto, UpdateUserDto>, IUserAppService
    {
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<Domain.Relation.UserArea, long> _userAreaRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IUserAreaService _userAreaService;
        private readonly INotificationAppService _notAppService;

        public UserAppService(
            IRepository<User, long> repository,
            UserManager userManager,
            IRepository<Role> roleRepository,
            RoleManager roleManager,
            IUserAreaService userAreaService,
            INotificationAppService notAppService,
            IRepository<Domain.Relation.UserArea, long> userAreaRepository,
            IRepository<User, long> userRepository)
            : base(repository)
        {
            _userManager = userManager;
            _roleRepository = roleRepository;
            _roleManager = roleManager;
            _userAreaService = userAreaService;
            _notAppService = notAppService;
            _userAreaRepository = userAreaRepository;
            _userRepository = userRepository;
        }

        public override async Task<UserDto> Get(Abp.Application.Services.Dto.EntityDto<long> input)
        {
            var user = (await _userManager.GetUserByIdAsync(input.Id)).MapTo<UserDto>();
            var userRoles = await _userManager.GetRolesAsync(user.Id);
            user.Roles = userRoles.Select(ur => ur).ToArray();
            return user;
        }

        public override async Task<UserDto> Create(CreateUserDto input)
        {
            CheckCreatePermission();

            var user = ObjectMapper.Map<User>(input);

            user.TenantId = AbpSession.TenantId;
            user.Password = new PasswordHasher().HashPassword(input.Password);
            user.IsEmailConfirmed = true;

            //Assign roles
            user.Roles = new Collection<UserRole>();
            foreach (var roleName in input.RoleNames)
            {
                var role = await _roleManager.GetRoleByNameAsync(roleName);
                user.Roles.Add(new UserRole(AbpSession.TenantId, user.Id, role.Id));
            }


            CheckErrors(await _userManager.CreateAsync(user));

            await CurrentUnitOfWork.SaveChangesAsync();

            foreach (var item in input.AreaIds)
            {
                _userAreaService.AddOrUpdate(new UserArea.Dto.UserAreaDto
                {
                    UserId = user.Id,
                    AreaId = item
                });
            }
            CurrentUnitOfWork.SaveChanges();

            // -- Set notification for user
            await _notAppService.Subscribe_SentNotForNewTransact(user.Id);
            await _notAppService.Subscribe_SentNotForChangeTransact(user.Id);
            
            return MapToEntityDto(user);
        }

        public async Task<long> UpdateUser(UpdateUserDto input)
        {
            CheckUpdatePermission();

            var user = _userRepository
                                .GetAllIncluding(x => x.UserAreaRelations)
                                .FirstOrDefault(x => x.Id == input.Id);

            MapToEntity(input, user);

            CheckErrors(await _userManager.UpdateAsync(user));

            if (input.RoleNames != null)
            {
                CheckErrors(await _userManager.SetRoles(user, input.RoleNames));
            }

            #region Areas
            
            // -- Remove Areas
            var areasRemove = user.AreaIds.Where(x => !input.AreaIds.Contains(x));
            if (areasRemove.Any())
            {
                await _userAreaRepository.DeleteAsync(x => areasRemove.Contains(x.AreaId) && x.UserId == user.Id);
                await CurrentUnitOfWork.SaveChangesAsync();
            }

            // -- Add Areas
            var areasAdd = input.AreaIds.Where(x => !user.AreaIds.Contains(x)).ToList();
            if (areasAdd.Any())
            {
                foreach (var areaId in areasAdd)
                {
                    await _userAreaRepository.InsertAsync(new Domain.Relation.UserArea
                    {
                        AreaId = areaId,
                        UserId = user.Id
                    });
                }
                await CurrentUnitOfWork.SaveChangesAsync();
            }

            #endregion

            return input.Id;
        }

        public override async Task Delete(Abp.Application.Services.Dto.EntityDto<long> input)
        {
            var user = await _userManager.GetUserByIdAsync(input.Id);
            await _userManager.DeleteAsync(user);
        }

        public async Task<ListResultDto<RoleDto>> GetRoles()
        {
            var roles = await _roleRepository.GetAllListAsync();
            return new ListResultDto<RoleDto>(ObjectMapper.Map<List<RoleDto>>(roles));
        }

        protected override User MapToEntity(CreateUserDto createInput)
        {
            var user = ObjectMapper.Map<User>(createInput);
            return user;
        }

        protected override void MapToEntity(UpdateUserDto input, User user)
        {
            ObjectMapper.Map(input, user);
        }

        protected override IQueryable<User> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAllIncluding(x => x.Roles);
        }

        protected override async Task<User> GetEntityByIdAsync(long id)
        {
            var user = Repository.GetAllIncluding(x => x.Roles).FirstOrDefault(x => x.Id == id);
            //var areas = _userAreaService.GetAllByUserId(id);
            //foreach (var item in areas)
            //{
            //    user.AreaIds.Add(item.AreaId);
            //}
            return await Task.FromResult(user);
        }

        protected override IQueryable<User> ApplySorting(IQueryable<User> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.UserName);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public ContextDto<UserListDto> GetAllUsers(SearchDto filters)
        {
            var notEmpty = !string.IsNullOrEmpty(filters.Search);

            Expression<Func<User, bool>> expression = x =>
                    (notEmpty && ((filters.Search.StartsWith("#") && x.Id.ToString() == filters.Search.Replace("#", "").ToString())
                                 || (x.Name.ToLower().Contains(filters.Search.ToLower()))
                                 || (x.Surname.ToLower().Contains(filters.Search.ToLower()))
                                 || (x.EmailAddress.ToLower().Contains(filters.Search.ToLower())))
                    ) || !notEmpty;

            var resultList = Repository
                            .GetAll()
                            .Where(expression)
                            .OrderByDescending(x => x.CreationTime)
                            .ToPagedList(filters.Page, filters.PageSize);

            return new ContextDto<UserListDto>
            {
                Data = resultList.Select(x => x.MapTo<UserListDto>()),
                MetaData = resultList.GetMetaData()
            };
        }
    }
}