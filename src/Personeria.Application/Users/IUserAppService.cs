using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Personeria.Base;
using Personeria.Roles.Dto;
using Personeria.Users.Dto;

namespace Personeria.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedResultRequestDto, CreateUserDto, UpdateUserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

        ContextDto<UserListDto> GetAllUsers(SearchDto filters);
        Task<long> UpdateUser(UpdateUserDto input);
    }
}