﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Personeria.Authorization.Accounts.Dto;

namespace Personeria.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        //Task<RegisterOutput> Register(RegisterInput input);
    }
}
