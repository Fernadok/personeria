﻿using Personeria.Enums;
using System;

namespace Personeria.Base
{
    public class SearchDto
    {
        public SearchDto()
        {
            Page = 1;
            PageSize = 10;
            Type = "all";
            OrderColumn = "";
            Order = "desc";
        }

        public long Id { get; set; }
        //FILTER
        public string Search { get; set; }
        public int Page { get; set; }
        public string Order { get; set; }
        public int PageSize { get; set; }
        public string OrderColumn { get; set; }

        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }

        public string Type { get; set; }
        public TransactState State { get; set; }
        public long? TypeTransact { get; set; }

    }
}
