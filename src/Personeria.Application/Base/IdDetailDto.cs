﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Personeria.Base
{
    public class IdDetailDto<TPrimaryKey>
    {
        public TPrimaryKey Id { get; set; }
        public string Detail { get; set; }
    }
}
