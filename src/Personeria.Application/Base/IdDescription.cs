﻿namespace Personeria.Base
{
    public class IdDescription : IdDescription<long>
    {
    }

    public class IdDescription<TPrimaryKey>
    {
        public TPrimaryKey Id { get; set; }
        public string Description { get; set; }
        public string DescriptionFull { get; set; }
        public string Created { get; set; }
    }
}
