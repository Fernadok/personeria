﻿using System;

namespace Personeria.Base
{
    public abstract class AuditableEntityDto<T>: EntityBase<T>
    {
        public AuditableEntityDto()
        {
            CreatedDate = DateTime.Now;
        }

        DateTime CreatedDate { get; set; }
        string CreatedBy { get; set; }
        DateTime UpdatedDate { get; set; }
        string UpdatedBy { get; set; }
    }

    public abstract class AuditableEntityDto : AuditableEntityDto<long>
    {

    }

}
