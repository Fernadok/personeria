﻿using Abp.Localization;
using Newtonsoft.Json;
using System;

namespace Personeria.Base
{
    [JsonObject(IsReference = false)]
    public class EntityDto<TPrimaryKey>: Abp.Application.Services.Dto.EntityDto<TPrimaryKey>
    {
        public EntityDto()
        {            
            IsActive = true;
        }

        public bool IsActive { get; set; }

        public string L(string key)
        {
            return LocalizationHelper.GetString(PersoneriaConsts.LocalizationSourceName, key);
        }
    }
    
    public class EntityDto : EntityDto<long>
    {

    }
}
