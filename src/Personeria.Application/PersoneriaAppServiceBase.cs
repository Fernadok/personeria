﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.Runtime.Session;
using Microsoft.AspNet.Identity;
using Personeria.Authorization.Users;
using Personeria.Domain;
using Personeria.Histories;
using Personeria.MultiTenancy;
using Personeria.Notifications;
using System;
using System.Threading.Tasks;
using Personeria.ProcessFlow;
using Personeria.Email;
using Personeria.Providers;

namespace Personeria
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class PersoneriaAppServiceBase : ApplicationService
    {
        public TenantManager TenantManager { get; set; }
        public UserManager UserManager { get; set; }

        public IRepository<FileAttach, long> FileAttachRepository { get; set; }
        public IRepository<User, long> UserRepository { get; set; }
        public IRepository<Transact, long> TransactRepository { get; set; }
        public IRepository<Print, long> PrintRepository { get; set; }

        public IHistoryService HistoryService { get; set; }
        public INotificationAppService NotificationService { get; set; }

        public INetWorkProvider NetWorkProvider { get; set; }
        public IWebDomainProvider WebDomainProvider { get; set; }

        protected PersoneriaAppServiceBase()
        {
            LocalizationSourceName = PersoneriaConsts.LocalizationSourceName;
        }

        protected virtual Task<User> GetCurrentUserAsync()
        {
            var user = UserManager.FindByIdAsync(AbpSession.GetUserId());
            if (user == null)
            {
                throw new ApplicationException("¡No hay usuario actual!");
            }

            return user;
        }

        protected virtual User GetCurrentUser()
        {
            if (!AbpSession.UserId.HasValue) { return null; }
            var user = UserRepository.FirstOrDefault(x => x.Id == AbpSession.UserId.Value);
            if (user == null)
            {
                return null;
            }

            return user;
        }

        protected virtual Task<Tenant> GetCurrentTenantAsync()
        {
            return TenantManager.GetByIdAsync(AbpSession.GetTenantId());
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}