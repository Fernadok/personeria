﻿using Abp.AutoMapper;
using Abp.Domain.Entities;
using Newtonsoft.Json;
using Personeria.Domain;
using Personeria.Enums;
using Personeria.FileAttachs.Dto;
using Personeria.Locations.Dto;
using Personeria.Persons.Dto;
using System;
using System.Collections.Generic;

namespace Personeria.Transacts.Dto
{
    [AutoMapFrom(typeof(Transact))]
    public class TransactGetModalDto : Entity<long>
    {
        public TransactGetModalDto()
        {
            FileAttachs = new HashSet<FileAttachDto>();
        }

        public string Subject { get; set; }
        public TransactState State { get; set; }
        public string Observation { get; set; }

        public long PersonId { get; set; }
        public PersonDto Person { get; set; }
        public string FullName => string.Format("({0}) {1} {2}", Person.NumberIdentity, Person.Surname, Person.Name);

        public long TransactTypeId { get; set; }
        public string TransactTypeName { get; set; }

        public long LocationId { get; set; }
        [JsonIgnore]
        public LocationDto Location { get; set; }
        public string LocationName => Location.Name;

        public Guid? Revision { get; set; }

        public virtual HashSet<FileAttachDto> FileAttachs { get; set; }

        public string CodeQr { get; set; }
    }
}
