﻿using Abp.AutoMapper;
using Personeria.Base;
using Personeria.Domain;
using Personeria.Enums;
using System;

namespace Personeria.Transacts.Dto
{
    [AutoMapFrom(typeof(Transact))]
    public class TransactGetDto : EntityDto
    {
        public TransactGetDto()
        {
            Percent = 0;
        }

        public string Subject { get; set; }

        public TransactState State { get; set; }

        public string StateFormat => L(State.ToString());

        public string Observation { get; set; }

        public long PersonId { get; set; }

        public string PersonNumberIdentity { get; set; }

        public string PersonName { get; set; }

        public string PersonSurname { get; set; }

        public string FullName => $"{PersonNumberIdentity} / {PersonName} {PersonSurname}";

        public long TransactTypeId { get; set; }

        public string TransactTypeName { get; set; }

        public long LocationId { get; set; }

        public string LocationName { get; set; }

        public long AreaId { get; set; }

        public long? LockedAreaId { get; set; }

        public string AreaName { get; set; }

        public Guid? Revision { get; set; }

        public int Percent { get; set; }

        public string StateColor
        {
            get
            {
                switch (State)
                {
                    case TransactState.All:
                        return "bg-blue";
                    case TransactState.InProcess:
                        return "bg-blue";
                    case TransactState.InPause:
                        return "bg-orange";
                    case TransactState.Locked:
                        return "bg-red";
                    case TransactState.Finished:
                        return "bg-green";
                    default:
                        return "";
                }
            }
        }
    }
}
