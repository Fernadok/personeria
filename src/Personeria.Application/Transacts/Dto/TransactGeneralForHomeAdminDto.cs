﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Personeria.Transacts.Dto
{
    public class TransactGeneralForHomeAdminDto
    {
        public int Today { get; set; }
        public int Yesterday { get; set; }
        public int LastWeek { get; set; }
        public int LastMonth { get; set; }
        public int LastYear { get; set; }
        public int Total { get; set; }
    }
}
