﻿using Abp.Domain.Entities;
using Personeria.Enums;
using System;
using System.Collections.Generic;

namespace Personeria.Transacts.Dto
{
    public class TransactInputDto : Entity<long>
    {
        public TransactInputDto()
        {
            TransactType = new List<TransactTypeInputDto>();
        }

        public long PersonId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string NumberIdentity { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Adreess { get; set; }


        public long LocationId { get; set; }
        public long TransactLocationId { get; set; }
        public string Subject { get; set; }
        public string Observation { get; set; }

        public TransactState State { get; set; }

        public Guid? Revision { get; set; }

        public List<TransactTypeInputDto> TransactType { get; set; }
    }
}
