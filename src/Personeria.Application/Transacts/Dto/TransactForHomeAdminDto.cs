﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Personeria.Transacts.Dto
{
    public class TransactForHomeAdminDto
    {
        public int TransactInProcess { get; set; }
        public int TransactInPause { get; set; }
        public int TransactLocked { get; set; }
        public int TransactFinished { get; set; }
    }
}
