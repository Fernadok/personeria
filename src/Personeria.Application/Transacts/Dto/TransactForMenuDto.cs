﻿using Abp.AutoMapper;
using Newtonsoft.Json;
using Personeria.Base;
using Personeria.Domain;
using Personeria.Enums;
using Personeria.TransactTypes.Dto;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Personeria.Transacts.Dto
{
    [AutoMapFrom(typeof(Transact))]
    public class TransactForMenuDto : EntityDto
    {
        [JsonIgnore]
        public TransactState State { get; set; }

        [JsonIgnore]
        public TransactTypeLightDto TransactType { get; set; }        
        public long FirstSections => TransactType.TransactTypeAreaRelations.OrderBy(x => x.Order).FirstOrDefault().AreaId;
        public long LastSections => TransactType.TransactTypeAreaRelations.OrderByDescending(x => x.Order).FirstOrDefault().AreaId;

        [JsonIgnore]
        public long AreaId { get; set; }
        [JsonIgnore]
        public long? LockedAreaId { get; set; }

        public List<IdDescription> StatesList {
            get {
                var res = State;
                List<IdDescription> _states = new List<IdDescription>();
                if (LockedAreaId.HasValue)
                {
                    _states.Add(new IdDescription { Id = -2, Description = "Devolver" });
                    return _states;
                }
                if (res == TransactState.InProcess)
                {
                    if (FirstSections != AreaId)
                    {
                        _states.Add(new IdDescription { Id = -3, Description = "Devolver" });
                    }
                    _states.Add(new IdDescription { Id = (long)TransactState.Locked, Description = "Bloquear" });
                    _states.Add(new IdDescription { Id = (long)TransactState.InPause, Description = "Pausar" });
                    _states.Add(new IdDescription { Id = (long)TransactState.Finished, Description = (LastSections != AreaId ? "Continuar" : "Terminar") });
                }
                if (res == TransactState.InPause)
                {
                    if (FirstSections != AreaId)
                    {
                        _states.Add(new IdDescription { Id = -3, Description = "Devolver" });
                    }
                    _states.Add(new IdDescription { Id = (long)TransactState.Locked, Description = "Bloquear" });
                    _states.Add(new IdDescription { Id = (long)TransactState.InProcess, Description = "En Curso" });
                    _states.Add(new IdDescription { Id = (long)TransactState.Finished, Description = (LastSections != AreaId ? "Continuar" : "Terminar") });
                }
                if (res == TransactState.Locked)
                {
                    if (FirstSections != AreaId)
                    {
                        _states.Add(new IdDescription { Id = -3, Description = "Devolver" });
                    }
                    _states.Add(new IdDescription { Id = (long)TransactState.InProcess, Description = "En Curso" });
                    //Pedir Archivo
                }
                if (res == TransactState.Finished)
                {
                    //Reabrir -> siguiente area
                    _states.Add(new IdDescription { Id = -1, Description = "Re Abrir" });
                }
                return _states;
            }

        }
    }
       
}
