﻿using Personeria.FileAttachs.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Personeria.Transacts.Dto
{
    public class AttachFileAdminDto
    {
        public AttachFileAdminDto()
        {
            Files = new List<FileAttachDto>();
        }

        public long TransactId { get; set; }
        public List<FileAttachDto> Files { get; set; }
    }
}
