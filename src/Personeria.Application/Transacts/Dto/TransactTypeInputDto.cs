﻿using Personeria.FileAttachs.Dto;

namespace Personeria.Transacts.Dto
{
    public class TransactTypeInputDto
    {
        public long TransactTypeId { get; set; }
        public long FileAttachTypeId { get; set; }
       
        public FileAttachDto File  { get; set; }
    }
}
