﻿using Abp.AutoMapper;
using Personeria.Base;
using Personeria.Domain;

namespace Personeria.Transacts.Dto
{
    [AutoMapFrom(typeof(Transact))]
    public class TransactLightDto : EntityDto
    {
        public string Subject { get; set; }
    }
}
