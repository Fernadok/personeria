﻿using Abp.Application.Services;
using Personeria.Transacts.Dto;
using System;
using System.Threading.Tasks;

namespace Personeria.Transacts
{
    public interface ITransactFromHomeAppService : IApplicationService
    {
        TransactGetModalDto GetByIdentityNumber(string identityNumber);

        long GetByRegister(long register);

        Task<long> Create(TransactInputDto dto);


        TransactGetModalDto GetRevision(Guid? id);
        Task SaveRevision(TransactInputDto dto);
    }
}
