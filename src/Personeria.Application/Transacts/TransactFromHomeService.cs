﻿using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Personeria.Domain;
using Personeria.Enums;
using Personeria.Histories.Dto;
using Personeria.Persons;
using Personeria.Persons.Dto;
using Personeria.Transacts.Dto;
using Personeria.TransactTypeAreas;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Personeria.Transacts
{

    public class TransactFromHomeAppService : PersoneriaAppServiceBase, ITransactFromHomeAppService
    {
        private readonly IRepository<Transact, long> _transactRepository;
        private readonly IRepository<FileAttach, long> _fileAttachRepository;
        private readonly ITransactTypeAreaService _transactTypeAreaService;
        private readonly IPersonService _personService;

        public TransactFromHomeAppService(IRepository<Transact, long> transactRepository
                             , IRepository<FileAttach, long> fileAttachRepository
                             , TransactTypeAreaService transactTypeAreaServiceRepository
                             , IPersonService personService)
        {
            _transactRepository = transactRepository;
            _fileAttachRepository = fileAttachRepository;
            _transactTypeAreaService = transactTypeAreaServiceRepository;
            _personService = personService;
        }

        public async Task<long> Create(TransactInputDto dto)
        {
            var _historyTransact = new HistoryDto();

            if (dto.PersonId == 0)
            {
                dto.PersonId = _personService.AddOrUpdate(new PersonDto
                {
                    Name = dto.Name,
                    Surname = dto.Surname,
                    NumberIdentity = dto.NumberIdentity,
                    Email = dto.Email,
                    Adreess = dto.Adreess,
                    Phone = dto.Phone,
                    LocationId = dto.LocationId
                });
                CurrentUnitOfWork.SaveChanges();
            }
            var _area = _transactTypeAreaService.GetFirstAreaByTransactType(dto.TransactType[0].TransactTypeId);
            var transact = new Transact
            {
                Id = dto.Id,
                PersonId = dto.PersonId,
                LocationId = dto.TransactLocationId,
                TransactTypeId = dto.TransactType[0].TransactTypeId,
                State = Enums.TransactState.InProcess,
                AreaId = _area.AreaId,
                Subject = dto.Subject,
                Observation = dto.Observation,
            };
            _transactRepository.InsertOrUpdate(transact);
            CurrentUnitOfWork.SaveChanges();

            #region FileAttach
            foreach (var TransactType in dto.TransactType)
            {
                if (TransactType.File == null)
                {
                    continue;
                }

                var fileAttach = TransactType.File.MapTo<FileAttach>();
                fileAttach.TransactId = transact.Id;
                _fileAttachRepository.InsertOrUpdate(fileAttach);
            }
            CurrentUnitOfWork.SaveChanges();
            #endregion

            #region Historial
            _historyTransact.CreateDate = DateTime.Now;
            _historyTransact.Ip = "";
            _historyTransact.UserId = AbpSession.UserId;
            _historyTransact.TransactId = transact.Id;
            _historyTransact.State = transact.State;
            _historyTransact.Observation = string.Format(HistoryMessages.NewTransact
                                                        , (dto.Surname + " " + dto.Name).ToUpper()
                                                        , transact.Id
                                                        , _area.Area.Name.ToUpper()
                                                        , L(Enums.TransactState.InProcess.ToString()));
            HistoryService.AddOrUpdate(_historyTransact);
            CurrentUnitOfWork.SaveChanges();
            #endregion

            // -- Notification when is New
            await NotificationService.Publish_SentNotForNewTransact(transact.Id);

            return transact.Id;
        }

        public TransactGetModalDto GetByIdentityNumber(string identityNumber)
        {
            var trans = _transactRepository
               .GetAllIncluding(s => s.TransactType,
                                s => s.Person,
                                s => s.Location,
                                s => s.FileAttachs,
                                s => s.Histories)
               .Where(x => x.IsActive && x.Person.NumberIdentity.Equals(identityNumber.Trim()))
               .FirstOrDefault();

            return trans?.MapTo<TransactGetModalDto>();
        }

        public long GetByRegister(long register)
        {
            var trans = _transactRepository
             .GetAllIncluding(s => s.TransactType,
                              s => s.Person,
                              s => s.Location,
                              s => s.FileAttachs,
                              s => s.Histories)
             .Where(x => x.IsActive && x.Id == register)
             .FirstOrDefault();

            return trans != null ? trans.Id : 0;
        }

        public TransactGetModalDto GetRevision(Guid? id)
        {
            if (!id.HasValue) return null;

            var trans = _transactRepository
                .GetAllIncluding(s => s.TransactType,
                                 s => s.Person,
                                 s => s.Location,
                                 s => s.FileAttachs)
                .Where(x => x.IsActive && x.Revision == id)
                .FirstOrDefault();

            var tra = trans?.MapTo<TransactGetModalDto>();
            if (tra != null)
            {
                tra.CodeQr = NetWorkProvider.GetCodeQr($"{WebDomainProvider.CurrentDomain}/tramites/{tra.Id}");
            }
            return tra;
        }

        public async Task SaveRevision(TransactInputDto dto)
        {
            if (!dto.Revision.HasValue) return;

            var _historyTransact = new HistoryDto();
            var transact = _transactRepository
                        .GetAllIncluding(s => s.TransactType,
                                         s => s.Person,
                                         s => s.Location,
                                         s => s.FileAttachs,
                                         s => s.Histories)
                        .Where(x => x.IsActive && x.Revision == dto.Revision)
                        .FirstOrDefault();

            if (transact != null)
            {
                transact.Revision = null;

                await _transactRepository.UpdateAsync(transact);
                await CurrentUnitOfWork.SaveChangesAsync();

                #region FileAttach
                if (dto.TransactType.Count > 0)
                {
                    var fileImages = transact.FileAttachs.Select(s => s.Id);
                    await FileAttachRepository.DeleteAsync(x => fileImages.Contains(x.Id));
                    await CurrentUnitOfWork.SaveChangesAsync();

                    var filesForNew = dto.TransactType;
                    foreach (var TransactType in filesForNew)
                    {
                        var fileAtt = TransactType.File.MapTo<FileAttach>();
                        fileAtt.TransactId = transact.Id;
                        await FileAttachRepository.InsertAsync(fileAtt);
                    }
                    await CurrentUnitOfWork.SaveChangesAsync();
                }
                else if (transact.FileAttachs.Count > 0)
                {
                    var file = transact.FileAttachs.Select(s => s.Id);
                    await FileAttachRepository.DeleteAsync(x => file.Contains(x.Id));
                    await CurrentUnitOfWork.SaveChangesAsync();
                }

                #region Historial
                _historyTransact.CreateDate = DateTime.Now;
                _historyTransact.Ip = "";
                _historyTransact.TransactId = transact.Id;
                _historyTransact.State = transact.State;
                _historyTransact.Observation = string.Format(HistoryMessages.RevisionTransactBack
                                                            , transact.Id
                                                            , (transact.Person.Name + " " + transact.Person.Surname).ToUpper()
                                                            , transact.Area.Name.ToUpper()
                                                            , L(transact.State.ToString()));
                HistoryService.AddOrUpdate(_historyTransact);

                #endregion

                await NotificationService.Publish_SentNotForChangeTransact(transact.Id, false, true);
                #endregion
            }
        }
    }
}
