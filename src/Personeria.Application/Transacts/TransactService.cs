﻿using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using NinjaNye.SearchExtensions;
using PagedList;
using Personeria.Base;
using Personeria.Domain;
using Personeria.Enums;
using Personeria.Extensions;
using Personeria.Histories.Dto;
using Personeria.Persons;
using Personeria.ProcessFlow;
using Personeria.Sessions;
using Personeria.Transacts.Dto;
using Personeria.TransactTypeAreas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Personeria.Transacts
{
    //[AbpAuthorize]
    public class TransactService : PersoneriaAppServiceBase, ITransactService
    {
        private readonly IRepository<Transact, long> _transactRepository;
        private readonly IRepository<TransactType, long> _transactTypeRepository;
        private readonly IRepository<FileAttach, long> _fileAttachRepository;
        private readonly IRepository<Personeria.Domain.Relation.UserArea, long> _userAreaRepository;
        private readonly ITransactTypeAreaService _transactTypeAreaService;
        private readonly IPersonService _personService;
        private readonly IProcessFlowService _processFlowService;
        private readonly ISessionAppService _sessionAppService;

        public TransactService(IRepository<Transact, long> transactRepository
                             , IRepository<TransactType, long> transactTypeRepository
                             , IRepository<FileAttach, long> fileAttachRepository
                             , TransactTypeAreaService transactTypeAreaServiceRepository
                             , IPersonService personService
                             , IProcessFlowService processFlowService
                             , IRepository<Personeria.Domain.Relation.UserArea, long> userAreaRepository
                             , ISessionAppService sessionAppService)
        {
            _transactRepository = transactRepository;
            _transactTypeRepository = transactTypeRepository;
            _fileAttachRepository = fileAttachRepository;
            _transactTypeAreaService = transactTypeAreaServiceRepository;
            _personService = personService;
            _processFlowService = processFlowService;
            _userAreaRepository = userAreaRepository;
            _sessionAppService = sessionAppService;
        }

        [UnitOfWork(isTransactional: true)]
        public async Task<long> AddOrUpdate(TransactInputDto dto)
        {
            var _historyTransact = new HistoryDto();
            var isnew = _transactRepository
                        .GetAllIncluding(s => s.TransactType,
                                         s => s.Person,
                                         s => s.Location,
                                         s => s.FileAttachs,
                                         s => s.Histories)
                        .Where(x => x.IsActive && x.Id == dto.Id)
                        .FirstOrDefault();

            if (isnew != null)
            {
                var transact = isnew;
                transact.Observation = dto.Observation;
                transact.TransactTypeId = dto.TransactType[0].TransactTypeId;
                transact.LocationId = dto.TransactLocationId;

                if (dto.State == TransactState.Locked && dto.Revision.HasValue)
                {
                    transact.Revision = null;
                    //Send Notification Area
                }

                _transactRepository.InsertOrUpdate(transact);
                CurrentUnitOfWork.SaveChanges();

                #region FileAttach
                if (dto.TransactType.Count > 0)
                {
                    var fileImages = transact.FileAttachs.Select(s => s.Id).ToArray();
                    Expression<Func<FileAttach, bool>> expressionfileImages = x => fileImages.Contains(x.Id);
                    FileAttachRepository.Delete(expressionfileImages);
                    base.CurrentUnitOfWork.SaveChanges();

                    var filesForNew = dto.TransactType;
                    foreach (var TransactType in filesForNew)
                    {
                        var fileAtt = TransactType.File.MapTo<FileAttach>();
                        transact.FileAttachs.Add(fileAtt);
                    }
                    _transactRepository.Update(transact);
                    base.CurrentUnitOfWork.SaveChanges();
                }
                else
                {
                    if (transact.FileAttachs.Count > 0)
                    {
                        var file = transact.FileAttachs.Select(s => s.Id).ToArray();
                        Expression<Func<FileAttach, bool>> expressionFile = x => file.Contains(x.Id);

                        FileAttachRepository.Delete(expressionFile);
                        base.CurrentUnitOfWork.SaveChanges();
                    }
                }
                #endregion
            }
            else
            {

                if (dto.PersonId == 0)
                {
                    dto.PersonId = _personService.AddOrUpdate(new Persons.Dto.PersonDto
                    {
                        Name = dto.Name,
                        Surname = dto.Surname,
                        NumberIdentity = dto.NumberIdentity,
                        Email = dto.Email,
                        Adreess = dto.Adreess,
                        Phone = dto.Phone,
                        LocationId = dto.LocationId
                    });
                    CurrentUnitOfWork.SaveChanges();
                }
                var _area = _transactTypeAreaService.GetFirstAreaByTransactType(dto.TransactType[0].TransactTypeId);
                var transact = new Transact
                {
                    Id = dto.Id,
                    PersonId = dto.PersonId,
                    LocationId = dto.TransactLocationId,
                    TransactTypeId = dto.TransactType[0].TransactTypeId,
                    State = Enums.TransactState.InProcess,
                    AreaId = _area.AreaId,
                    Subject = dto.Subject,
                    Observation = dto.Observation,
                };
                _transactRepository.InsertOrUpdate(transact);
                CurrentUnitOfWork.SaveChanges();

                #region FileAttach
                foreach (var TransactType in dto.TransactType)
                {
                    if (TransactType.File == null)
                    {
                        continue;
                    }

                    var fileAttach = TransactType.File.MapTo<FileAttach>();
                    fileAttach.TransactId = transact.Id;
                    _fileAttachRepository.InsertOrUpdate(fileAttach);
                }
                CurrentUnitOfWork.SaveChanges();
                #endregion

                #region Historial
                _historyTransact.CreateDate = DateTime.Now;
                _historyTransact.Ip = "";
                _historyTransact.UserId = AbpSession.UserId;
                _historyTransact.TransactId = transact.Id;
                _historyTransact.State = transact.State;
                _historyTransact.Observation = string.Format(HistoryMessages.NewTransact
                                                            , (dto.Surname + " " + dto.Name).ToUpper()
                                                            , transact.Id
                                                            , _area.Area.Name.ToUpper()
                                                            , L(Enums.TransactState.InProcess.ToString()));
                HistoryService.AddOrUpdate(_historyTransact);
                CurrentUnitOfWork.SaveChanges();
                #endregion

                if (isnew == null)
                {
                    await NotificationService.Publish_SentNotForNewTransact(transact.Id);
                }

                return transact.Id;
            }

            return isnew.Id;
        }

        [UnitOfWork(isTransactional: true)]
        public void Delete(long id)
        {
            var Transact = _transactRepository.FirstOrDefault(x => x.IsActive && x.Id == id);
            if (Transact != null)
            {
                Transact.IsActive = false;
                _transactRepository.Update(Transact);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public async Task<ContextDto<TransactGetDto>> GetAll(SearchDto filters)
        {
            var user = (await _sessionAppService.GetCurrentLogin()).User;            
            var areasForUser = user.AreaIds;
            
            filters.Search = string.IsNullOrEmpty(filters.Search) ? "" : filters.Search.ToLower();
            var resultList = _transactRepository
                    .GetAllIncluding(s => s.Area, 
                                     s => s.TransactType,                                      
                                     s => s.Location,                                      
                                     s => s.Person)
                    .Where(x => x.IsActive)
                    .Where(x => user.IsAdmin || (!user.IsAdmin && areasForUser.Contains(x.AreaId)))
                    .Where(x => filters.State == TransactState.All || (filters.State != TransactState.All && x.State == filters.State))
                    .Where(x => !filters.TypeTransact.HasValue || (filters.TypeTransact.HasValue && x.TransactType.Id == filters.TypeTransact))
                    .Where(x => !filters.DateStart.HasValue || (filters.DateStart.HasValue && x.CreatedDate >= filters.DateStart.Value))
                    .Where(x => !filters.DateEnd.HasValue || (filters.DateEnd.HasValue && x.CreatedDate <= filters.DateEnd.Value))
                    .Search(x => x.Subject,
                            x => x.Person.Name,
                            x => x.Person.Email,
                            x => x.Person.Surname)
                    .Containing(filters.Search)
                    .OrderByTo(filters.OrderColumn, filters.Order.ToLower() == "desc")
                    .ToPagedList(filters.Page, filters.PageSize);

            var context = new ContextDto<TransactGetDto>
            {
                Data = resultList.Select(x => x.MapTo<TransactGetDto>()),
                MetaData = resultList.GetMetaData()
            };

            return context;
        }

        public async Task<List<IdDescription>> GetStateList(long id)
        {
            var transact = await _transactRepository.FirstOrDefaultAsync(x => x.Id == id);
            if (transact == null) return new List<IdDescription>();

            var res = transact.State;
            var lockedAreaId = transact.LockedAreaId;
            var areaId = transact.AreaId;
            var firstRelation = transact.TransactType
                                .TransactTypeAreaRelations
                                .OrderBy(x => x.Order)
                                .Take(1)
                                .FirstOrDefault();
            var lastRelation = transact.TransactType
                                .TransactTypeAreaRelations
                                .OrderByDescending(x => x.Order)
                                .Take(1)
                                .FirstOrDefault();

            if (firstRelation == null || lastRelation == null) return new List<IdDescription>();


            var _states = new List<IdDescription>();

            if (lockedAreaId.HasValue)
            {
                _states.Add(new IdDescription { Id = -2, Description = "Devolver" });
                return _states;
            }
            if (res == TransactState.InProcess)
            {
                if (firstRelation.AreaId != areaId)
                {
                    _states.Add(new IdDescription { Id = -3, Description = "Devolver" });
                }
                _states.Add(new IdDescription { Id = (long)TransactState.Locked, Description = "Bloquear" });
                _states.Add(new IdDescription { Id = (long)TransactState.InPause, Description = "Pausar" });
                _states.Add(new IdDescription { Id = (long)TransactState.Finished, Description = (lastRelation.AreaId != areaId ? "Continuar" : "Terminar") });
            }
            if (res == TransactState.InPause)
            {
                if (firstRelation.AreaId != areaId)
                {
                    _states.Add(new IdDescription { Id = -3, Description = "Devolver" });
                }
                _states.Add(new IdDescription { Id = (long)TransactState.Locked, Description = "Bloquear" });
                _states.Add(new IdDescription { Id = (long)TransactState.InProcess, Description = "En Curso" });
                _states.Add(new IdDescription { Id = (long)TransactState.Finished, Description = (lastRelation.AreaId != areaId ? "Continuar" : "Terminar") });
            }
            if (res == TransactState.Locked)
            {
                if (firstRelation.AreaId != areaId)
                {
                    _states.Add(new IdDescription { Id = -3, Description = "Devolver" });
                }
                _states.Add(new IdDescription { Id = (long)TransactState.InProcess, Description = "En Curso" });
                //Pedir Archivo
            }
            if (res == TransactState.Finished)
            {
                //Reabrir -> siguiente area
                _states.Add(new IdDescription { Id = -1, Description = "Re Abrir" });
            }
            return _states;
        }

        public async Task<int> GetProgress(long transactTypeId, long areaId, bool finish = false)
        {
            var tt = await _transactTypeRepository.FirstOrDefaultAsync(x => x.Id == transactTypeId && x.IsActive);
            var max = tt.TransactTypeAreaRelations.Count();
            var area = tt.TransactTypeAreaRelations.FirstOrDefault(x => x.AreaId == areaId);
            var percentUnit = 100 / max;
            var percent = percentUnit * area.Order;
            if (percent == 100 && !finish)
            {
                percent = percent - 10;
            }
            return percent;
        }

        
        public TransactGetModalDto GetById(long id)
        {
            var trans = _transactRepository
                .GetAllIncluding(s => s.TransactType,
                                 s => s.Person,
                                 s => s.Location,
                                 s => s.FileAttachs)
                .Where(x => x.IsActive && x.Id == id)
                .FirstOrDefault();

            var tra = trans?.MapTo<TransactGetModalDto>();
            if (tra != null)
            {
                tra.CodeQr = NetWorkProvider.GetCodeQr($"{WebDomainProvider.CurrentDomain}/tramites/{tra.Id}");
            }
            return tra;
        }

        public List<IdDescription> TransactList()
        {
            return _transactRepository.GetAll()
              .ToList()
              .Where(x => x.IsActive)
              .Select(x => new IdDescription
              {
                  Id = x.Id,
                  Description = x.Subject
              }).OrderBy(x => x.Description).ToList();
        }

        public async Task<TransactForHomeAdminDto> GetToTals()
        {
            var transacts = new TransactForHomeAdminDto
            {
                TransactInProcess = await _transactRepository.CountAsync(x => x.IsActive && x.State == Enums.TransactState.InProcess),
                TransactInPause = await _transactRepository.CountAsync(x => x.IsActive && x.State == Enums.TransactState.InPause),
                TransactLocked = await _transactRepository.CountAsync(x => x.IsActive && x.State == Enums.TransactState.Locked),
                TransactFinished = await _transactRepository.CountAsync(x => x.IsActive && x.State == Enums.TransactState.Finished)
            };

            return transacts;
        }

        public async Task<TransactGeneralForHomeAdminDto> GetGeneralTotals()
        {
            var date = DateTime.Now;

            var dateToday = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
            var dateYerterday = dateToday.AddDays(-1);
            var dateLastWeek = dateToday.AddDays(-7);
            var dateLastMonth = dateToday.AddDays(-30);
            var dateLastYear = dateToday.AddDays(-365);

            var transacts = new TransactGeneralForHomeAdminDto
            {
                Today = await _transactRepository.CountAsync(x => x.IsActive && x.CreatedDate >= dateToday),
                Yesterday = await _transactRepository.CountAsync(x => x.IsActive && x.CreatedDate < dateToday && x.CreatedDate >= dateYerterday),
                LastWeek = await _transactRepository.CountAsync(x => x.IsActive && x.CreatedDate < dateToday && x.CreatedDate >= dateLastWeek),
                LastMonth = await _transactRepository.CountAsync(x => x.IsActive && x.CreatedDate < dateToday && x.CreatedDate >= dateLastMonth),
                LastYear = await _transactRepository.CountAsync(x => x.IsActive && x.CreatedDate < dateToday && x.CreatedDate >= dateLastYear),
                Total = await _transactRepository.CountAsync(x => x.IsActive)
            };

            return transacts;
        }

        [UnitOfWork(isTransactional: true)]
        public void FileAttachAdmin(AttachFileAdminDto dto)
        {
            var transaction = _transactRepository
                               .GetAllIncluding(s => s.TransactType,
                                                s => s.Person,
                                                s => s.Location,
                                                s => s.FileAttachs,
                                                s => s.Histories)
                               .Where(x => x.IsActive && x.Id == dto.TransactId)
                               .FirstOrDefault();

            #region FileAttach
            if (dto.Files.Count > 0)
            {
                // -- Delete files 
                var idsForDelete = dto.Files.Where(x => x.Store == PersoneriaConsts.UserKey).Select(x => x.Id);
                var filesForDelete = transaction.FileAttachs.Where(x => !idsForDelete.Contains(x.Id) && x.Store == PersoneriaConsts.UserKey);
                foreach (var file in filesForDelete)
                {
                    file.IsActive = false;
                    _fileAttachRepository.Update(file);
                }
                CurrentUnitOfWork.SaveChanges();

                // -- Create new file
                var idsForNews = transaction.FileAttachs.Where(x => x.IsActive && x.Store == PersoneriaConsts.UserKey).Select(x => x.Id);
                var filesForNew = dto.Files.Where(x => !idsForNews.Contains(x.Id) && x.Store == PersoneriaConsts.UserKey).Select(x => x.MapTo<FileAttach>());
                foreach (var file in filesForNew)
                {
                    transaction.FileAttachs.Add(file);
                }
                _transactRepository.Update(transaction);
                CurrentUnitOfWork.SaveChanges();
            }
            else
            {
                if (transaction.FileAttachs.Count > 0)
                {
                    foreach (var fileAttach in transaction.FileAttachs.Where(x => x.Store == PersoneriaConsts.UserKey))
                    {
                        fileAttach.IsActive = false;
                        _fileAttachRepository.Update(fileAttach);
                    }
                    CurrentUnitOfWork.SaveChanges();

                    // -- Delete files of Store
                    var idsForDeleteStore = dto.Files.Where(x => x.Store == PersoneriaConsts.UserKey).Select(x => x.Id);
                    var filesForDeleteStore = transaction.FileAttachs.Where(x => !idsForDeleteStore.Contains(x.Id) && x.Store == PersoneriaConsts.UserKey);
                    foreach (var file in new HashSet<FileAttach>(filesForDeleteStore))
                    {
                        file.TransactId = null;
                        _fileAttachRepository.Update(file);
                    }
                    CurrentUnitOfWork.SaveChanges();
                }
            }
            #endregion
        }

        public AttachFileAdminDto GetFilesAttach(long transactId, string store)
        {
            AttachFileAdminDto dto = new AttachFileAdminDto();

            var listFiles = _fileAttachRepository.GetAll()
                            .Where(x => x.TransactId == transactId && x.Store.Equals(store) && x.IsActive).ToList();
            dto.Files = listFiles.MapTo(dto.Files);
            dto.TransactId = transactId;
            return dto;
        }

    }
}
