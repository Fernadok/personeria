﻿using Abp.Application.Services;
using Personeria.Base;
using Personeria.Transacts.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Personeria.Transacts
{
    public interface ITransactService : IApplicationService
    {
        TransactGetModalDto GetById(long id);

        Task<ContextDto<TransactGetDto>> GetAll(SearchDto filters);

        Task<long> AddOrUpdate(TransactInputDto dto);

        void Delete(long id);

        List<IdDescription> TransactList();

        Task<TransactForHomeAdminDto> GetToTals();

        Task<TransactGeneralForHomeAdminDto> GetGeneralTotals();

        void FileAttachAdmin(AttachFileAdminDto dto);

        AttachFileAdminDto GetFilesAttach(long transactId, string store);

        Task<int> GetProgress(long transactTypeId, long areaId, bool finish = false);

        Task<List<IdDescription>> GetStateList(long id);
    }
}
