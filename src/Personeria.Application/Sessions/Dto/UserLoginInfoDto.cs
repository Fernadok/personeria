﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Newtonsoft.Json;
using Personeria.Authorization.Users;
using Personeria.Roles.Dto;
using System.Collections.Generic;
using System.Linq;

namespace Personeria.Sessions.Dto
{
    [AutoMapFrom(typeof(User))]
    public class UserLoginInfoDto : EntityDto
    {
        public UserLoginInfoDto()
        {
            AreaIds = new HashSet<long>();
        }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string UserName { get; set; }

        public string EmailAddress { get; set; }
                
        public HashSet<long> AreaIds { get; set; }

        [JsonIgnore]
        public List<UserRoleGetDto> Roles { get; set; } = new List<UserRoleGetDto>();

        [JsonIgnore]
        public List<int> Rols
        {
            get { return Roles.Select(x => x.RoleId).ToList(); }
        }

        public bool IsAdmin { get; set; }

        public long? PrintTicketDefaultId { get; set; }
        public string PrintTicketDefaultIp { get; set; }
    }
}
