﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Abp.Auditing;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using MoreLinq;
using Personeria.Authorization.Roles;
using Personeria.Sessions.Dto;

namespace Personeria.Sessions
{
    public class SessionAppService : PersoneriaAppServiceBase, ISessionAppService
    {
        private readonly RoleManager _roleManager;
        private readonly IRepository<Domain.Relation.UserArea, long> _userAreaRepository;

        public SessionAppService(RoleManager roleManager, IRepository<Domain.Relation.UserArea, long> userAreaRepository)
        {
            _roleManager = roleManager;
            _userAreaRepository = userAreaRepository;
        }

        [DisableAuditing]
        public async Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations()
        {
            var output = new GetCurrentLoginInformationsOutput();
            try
            {
                if (AbpSession.UserId.HasValue)
                {
                    output.User = (await GetCurrentUserAsync()).MapTo<UserLoginInfoDto>();
                }
            }
            catch (Exception){}            

            return output;
        }

        [DisableAuditing]
        public async Task<GetCurrentLoginInformationsOutput> GetCurrentLogin()
        {
            var output = new GetCurrentLoginInformationsOutput
            {
                User = GetCurrentUser().MapTo<UserLoginInfoDto>()
            };

            if (output.User != null)
            {
                output.User.IsAdmin = await UserManager.IsInRoleAsync(output.User.Id, StaticRoleNames.Host.Admin);
                output.User.AreaIds = _userAreaRepository.GetAll()
                                                       .Where(x => x.UserId == output.User.Id)
                                                       .Select(s=>s.AreaId)
                                                       .ToHashSet();
            }
            return output;
        }
    }
}