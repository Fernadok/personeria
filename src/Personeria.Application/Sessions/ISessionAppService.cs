﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Personeria.Sessions.Dto;

namespace Personeria.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();

        Task<GetCurrentLoginInformationsOutput> GetCurrentLogin();
    }
}
