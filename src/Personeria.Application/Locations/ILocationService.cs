﻿using Abp.Application.Services;
using System.Collections.Generic;
using Personeria.Base;
using Personeria.Locations.Dto;

namespace Personeria.Locations
{
    public interface ILocationService : IApplicationService
    {
        LocationDto GetById(long id);

        ContextDto<LocationDto> GetAll(SearchDto filters);

        long AddOrUpdate(LocationDto dto);

        void Delete(long id);

        List<IdDescription> LocationList();
    }
}
