﻿using Abp.AutoMapper;
using Abp.Domain.Entities;
using Personeria.Domain;

namespace Personeria.Locations.Dto
{
    [AutoMap(typeof(Location))]
    public class LocationDto : Entity<long>
    {
        public string Name { get; set; }
        public string PostalCode { get; set; }

        public long DepartmentId { get; set; }
    }
}
