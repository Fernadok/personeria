﻿using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using NinjaNye.SearchExtensions;
using PagedList;
using System.Collections.Generic;
using System.Linq;
using Personeria.Base;
using Personeria.Domain;
using Personeria.Extensions;
using Personeria.Locations.Dto;

namespace Personeria.Locations
{
    //[AbpAuthorize]
    public class LocationService: PersoneriaAppServiceBase, ILocationService
    {
        private readonly IRepository<Location, long> _LocationRepository;

        public LocationService(IRepository<Location, long> LocationRepository)
        {
            _LocationRepository = LocationRepository;
        }

        [UnitOfWork(isTransactional: true)]
        public long AddOrUpdate(LocationDto dto)
        {
            var isnew = _LocationRepository.FirstOrDefault(x => x.Id == dto.Id);
            // -- Verifico si el nombre ya se encuentra registrado
            if (isnew != null && isnew.Name.ToLower() != dto.Name.ToLower())
            {
                var existother = _LocationRepository.FirstOrDefault(x => x.Id != isnew.Id && x.Name.ToLower() == dto.Name.ToLower());
                if (existother != null)
                {
                    throw new UserFriendlyException("Ya se encuentra registrada una Location con el mismo nombre, intente ponerle otro nombre.");
                }
            }
            else if (isnew == null)
            {
                var existother = _LocationRepository.FirstOrDefault(x => x.Name.ToLower() == dto.Name.ToLower());
                if (existother != null)
                {
                    throw new UserFriendlyException("Ya se encuentra registrada una Location con el mismo nombre, intente ponerle otro nombre.");
                }
            }
            var ar = dto.MapTo(isnew);
            _LocationRepository.InsertOrUpdate(ar);
            CurrentUnitOfWork.SaveChanges();

            return ar.Id;
        }

        [UnitOfWork(isTransactional: true)]
        public void Delete(long id)
        {
            var Location = _LocationRepository.FirstOrDefault(x => x.Id == id);
            if (Location != null)
            {
                Location.IsActive = false;
                _LocationRepository.Update(Location);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public ContextDto<LocationDto> GetAll(SearchDto filters)
        {
            filters.Search = string.IsNullOrEmpty(filters.Search) ? "" : filters.Search.ToLower();
            var resultList = _LocationRepository
                    .GetAll()
                    .Where(x => x.IsActive)
                    .Search(x => x.Name)
                    .Containing(filters.Search)
                    .OrderByTo(filters.OrderColumn, filters.Order.ToLower() == "desc")
                    .ToPagedList(filters.Page, filters.PageSize);

            return new ContextDto<LocationDto>
            {
                Data = resultList.Select(x => x.MapTo<LocationDto>()),
                MetaData = resultList.GetMetaData()
            };
        }

        public LocationDto GetById(long id)
        {
            var location = _LocationRepository.FirstOrDefault(x => x.Id == id);
            return location?.MapTo<LocationDto>();
        }

        public List<IdDescription> LocationList()
        {
            return _LocationRepository.GetAll()
              .Where(s=>s.Department.StateId == 18) //SAN LUIS
              .ToList()
              .Where(x => x.IsActive)
              .Select(x => new IdDescription
              {
                  Id = x.Id,
                  Description =string.Format("({0}) {1}", x.Department.Name,x.Name) 
              }).OrderBy(x => x.Description).ToList();
        }
    }
}
