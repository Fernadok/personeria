﻿using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using NinjaNye.SearchExtensions;
using PagedList;
using System.Collections.Generic;
using System.Linq;
using Personeria.Base;
using Personeria.Domain;
using Personeria.Extensions;
using Personeria.Areas.Dto;

namespace Personeria.Areas
{
    //[AbpAuthorize]
    public class AreaService: PersoneriaAppServiceBase, IAreaService
    {
        private readonly IRepository<Area, long> _AreaRepository;

        public AreaService(IRepository<Area, long> AreaRepository)
        {
            _AreaRepository = AreaRepository;
        }

        [UnitOfWork(isTransactional: true)]
        public long AddOrUpdate(AreaDto dto)
        {
            var isnew = _AreaRepository.FirstOrDefault(x => x.Id == dto.Id);
            // -- Verifico si el nombre ya se encuentra registrado
            if (isnew != null && isnew.Name.ToLower() != dto.Name.ToLower())
            {
                var existother = _AreaRepository.FirstOrDefault(x => x.Id != isnew.Id && x.Name.ToLower() == dto.Name.ToLower());
                if (existother != null)
                {
                    throw new UserFriendlyException("Ya se encuentra registrada una Area con el mismo nombre, intente ponerle otro nombre.");
                }
            }
            else if (isnew == null)
            {
                var existother = _AreaRepository.FirstOrDefault(x => x.Name.ToLower() == dto.Name.ToLower());
                if (existother != null)
                {
                    throw new UserFriendlyException("Ya se encuentra registrada una Area con el mismo nombre, intente ponerle otro nombre.");
                }
            }
            var ar = dto.MapTo(isnew);
            _AreaRepository.InsertOrUpdate(ar);
            CurrentUnitOfWork.SaveChanges();

            return ar.Id;
        }

        [UnitOfWork(isTransactional: true)]
        public void Delete(long id)
        {
            var area = _AreaRepository.FirstOrDefault(x => x.Id == id);
            if (area != null)
            {
                area.IsActive = false;
                _AreaRepository.Update(area);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public ContextDto<AreaDto> GetAll(SearchDto filters)
        {
            filters.Search = string.IsNullOrEmpty(filters.Search) ? "" : filters.Search.ToLower();
            var resultList = _AreaRepository
                    .GetAll()
                    .Where(x => x.IsActive)
                    .Search(x => x.Name)
                    .Search(x => x.Description)
                    .Containing(filters.Search)
                    .OrderByTo(filters.OrderColumn, filters.Order.ToLower() == "desc")
                    .ToPagedList(filters.Page, filters.PageSize);

            return new ContextDto<AreaDto>
            {
                Data = resultList.Select(x => x.MapTo<AreaDto>()),
                MetaData = resultList.GetMetaData()
            };
        }

        public AreaDto GetById(long id)
        {
            var pre = _AreaRepository.FirstOrDefault(x => x.Id == id);
            return pre?.MapTo<AreaDto>();
        }

        public List<IdDescription> AreaList()
        {
            return _AreaRepository.GetAll()
              .ToList()
              .Where(x => x.IsActive)
              .Select(x => new IdDescription
              {
                  Id = x.Id,
                  Description = x.Name
              }).OrderBy(x => x.Description).ToList();
        }

        public List<IdDescription> MoveAreaList(List<long> except)
        {
            return _AreaRepository.GetAll()
                   .ToList()
                   .Where(x => x.IsActive && !except.Contains(x.Id))
                   .Select(x => new IdDescription
                   {
                       Id = x.Id,
                       Description = x.Name
                   }).OrderBy(x => x.Description).ToList();
        }
    }
}
