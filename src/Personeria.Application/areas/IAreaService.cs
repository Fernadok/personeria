﻿using Abp.Application.Services;
using System.Collections.Generic;
using Personeria.Base;
using Personeria.Areas.Dto;

namespace Personeria.Areas
{
    public interface IAreaService : IApplicationService
    {
        AreaDto GetById(long id);

        ContextDto<AreaDto> GetAll(SearchDto filters);

        long AddOrUpdate(AreaDto dto);

        void Delete(long id);

        List<IdDescription> AreaList();
        List<IdDescription> MoveAreaList(List<long> except);
    }
}
