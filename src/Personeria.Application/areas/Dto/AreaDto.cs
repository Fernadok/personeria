﻿using Abp.AutoMapper;
using Abp.Domain.Entities;
using Personeria.Domain;

namespace Personeria.Areas.Dto
{
    [AutoMap(typeof(Area))]
    public class AreaDto : Entity<long>
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
