﻿using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.UI;
using MoreLinq;
using NinjaNye.SearchExtensions;
using PagedList;
using Personeria.Base;
using Personeria.Domain;
using Personeria.Domain.Relation;
using Personeria.Extensions;
using Personeria.TransactTypes.Dto;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Personeria.TransactTypes
{    
    public class TransactTypeService : PersoneriaAppServiceBase, ITransactTypeService
    {
        private readonly IRepository<TransactType, long> _transactTypeRepository;
        private readonly IRepository<TransactTypeArea, long> _transactTypeAreasRepository;
        private readonly IRepository<TransactTypeFileAttachType, long> _transactTypeFileAttachTypeRepository;

        public TransactTypeService(IRepository<TransactType, long> TransactTypeRepository
                                 , IRepository<TransactTypeArea, long> TransactTypeAreasRepository
                                 , IRepository<TransactTypeFileAttachType, long> transactTypeFileAttachTypeRepository)
        {
            _transactTypeRepository = TransactTypeRepository;
            _transactTypeAreasRepository = TransactTypeAreasRepository;
            _transactTypeFileAttachTypeRepository = transactTypeFileAttachTypeRepository;
        }

        [AbpAuthorize]
        public async Task<long> AddOrUpdate(TransactTypeInputDto dto)
        {
            var typeTransact = _transactTypeRepository
                                    .GetAllIncluding(x => x.TransactTypeAreaRelations,
                                                     x => x.TransactTypeFileAttachTypeRelations)
                                   .FirstOrDefault(x => x.IsActive && x.Id == dto.Id);
            var isnew = typeTransact == null;

            // -- Verifico si el nombre ya se encuentra registrado
            var existother = _transactTypeRepository.GetAll().Any(x => (isnew || (!isnew && x.Id != dto.Id)) && x.Name.ToLower() == dto.Name.ToLower());
            if (existother)
            {
                throw new UserFriendlyException("Ya se encuentra registrada un tipo de transacción con el mismo nombre, intente ponerle otro nombre.");
            }


            if (!isnew)
            {
                if (typeTransact.Transacts.Any(x => x.IsActive && x.State != Enums.TransactState.Finished))
                {
                    throw new UserFriendlyException("Este TIPO DE TRÁMITE no se puede modificar porque todavía tiene trámites pendientes que no estan cerrados, cierre todos los trámites primero y despues intente nuevamente.");
                }
            }

            // -- Save Type Transact
            var tranType = dto.MapTo(typeTransact);
            await _transactTypeRepository.InsertOrUpdateAsync(tranType);
            await CurrentUnitOfWork.SaveChangesAsync();

            #region -- Guardo las areas
            // -- Elimino las relaciones que no tienen que estar            
            var areasIdsDelete = tranType.TransactTypeAreaRelations.Where(x => !dto.Areas.Any(a => x.Id == a.Id)).Select(x => x.Id).ToList();
            if (areasIdsDelete.Any())
            {
                await _transactTypeAreasRepository.DeleteAsync(x => areasIdsDelete.Contains(x.Id));
                await CurrentUnitOfWork.SaveChangesAsync();
            }

            // -- Agrego lo que no esten            
            if (dto.Areas.Any())
            {
                foreach (var area in dto.Areas)
                {
                    var ar = await _transactTypeAreasRepository.FirstOrDefaultAsync(x => x.Id == area.Id);
                    var arNew = area.MapTo(ar);
                    arNew.TransactTypeId = tranType.Id;
                    await _transactTypeAreasRepository.InsertOrUpdateAsync(arNew);
                    await CurrentUnitOfWork.SaveChangesAsync();
                };
            }
            #endregion

            #region -- Guardo relación con archivos
            // -- Borro las relaciones que se sacaron
            var fileIdsDelete = tranType.TransactTypeFileAttachTypeRelations.Where(x => !dto.Files.Any(a => x.Id == a.Id)).Select(x => x.Id).ToList();
            if (fileIdsDelete.Any())
            {
                await _transactTypeFileAttachTypeRepository.DeleteAsync(x => fileIdsDelete.Contains(x.Id));
                await CurrentUnitOfWork.SaveChangesAsync();
            }

            // -- Agrego lo que no esten            
            if (dto.Files.Any())
            {
                foreach (var file in dto.Files)
                {
                    var fl = await _transactTypeFileAttachTypeRepository.FirstOrDefaultAsync(x => x.Id == file.Id);
                    var flNew = file.MapTo(fl);
                    flNew.TransactTypeId = tranType.Id;
                    await _transactTypeFileAttachTypeRepository.InsertOrUpdateAsync(flNew);
                    await CurrentUnitOfWork.SaveChangesAsync();
                };
            }

            #endregion

            return tranType.Id;
        }

        [AbpAuthorize]
        public async Task Delete(long id)
        {
            var transactType = await _transactTypeRepository.FirstOrDefaultAsync(x => x.Id == id);

            if (transactType.Transacts.Any(x => x.IsActive && x.State != Enums.TransactState.Finished))
            {
                throw new UserFriendlyException("Este TIPO DE TRÁMITE no se puede eliminar porque todavía tiene trámites pendientes que no estan cerrados, cierre todos los trámites primero y despues intente nuevamente.");
            }

            if (transactType != null)
            {
                transactType.IsActive = false;
                await _transactTypeRepository.UpdateAsync(transactType);
                await CurrentUnitOfWork.SaveChangesAsync();
            }
        }

        [AbpAuthorize]
        public ContextDto<TransactTypeDto> GetAll(SearchDto filters)
        {
            filters.Search = string.IsNullOrEmpty(filters.Search) ? "" : filters.Search.ToLower();
            var resultList = _transactTypeRepository
                    .GetAllIncluding(x => x.Transacts)
                    .Where(x => x.IsActive)
                    .Search(x => x.Name)
                    .Containing(filters.Search)
                    .OrderByTo(filters.OrderColumn, filters.Order.ToLower() == "desc")
                    .ToPagedList(filters.Page, filters.PageSize);

            return new ContextDto<TransactTypeDto>
            {
                Data = resultList.Select(x =>
                {
                    var tt = x.MapTo<TransactTypeDto>();
                    tt.Count = x.Transacts.Count(t => t.IsActive);
                    return tt;
                }),
                MetaData = resultList.GetMetaData()
            };
        }

        [AbpAuthorize]
        public async Task<TransactTypeGetDto> GetById(long id)
        {
            var transact = await _transactTypeRepository.FirstOrDefaultAsync(x => x.Id == id);
            return transact?.MapTo<TransactTypeGetDto>();
        }

        public List<IdDescription> TransactTypeList()
        {
            return _transactTypeRepository.GetAll()
              .Where(x => x.IsActive)
              .OrderBy(x => x.Name)
              .Select(x => new IdDescription
              {
                  Id = x.Id,
                  Description = x.Name
              }).ToList();
        }
    }
}
