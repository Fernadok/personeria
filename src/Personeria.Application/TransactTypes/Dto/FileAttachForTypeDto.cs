﻿using Abp.AutoMapper;
using Personeria.Domain.Relation;

namespace Personeria.TransactTypes.Dto
{
    [AutoMap(typeof(TransactTypeFileAttachType))]
    public class FileAttachForTypeDto
    {
        public FileAttachForTypeDto()
        {
            IsRequired = false;
        }

        public long Id { get; set; }

        public long TransactTypeId { get; set; }
        public long FileAttachTypeId { get; set; }

        public string FileAttachTypeName { get; set; }

        public bool IsRequired { get; set; }
    }
}
