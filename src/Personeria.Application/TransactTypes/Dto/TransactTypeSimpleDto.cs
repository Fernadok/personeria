﻿using Abp.AutoMapper;
using Abp.Domain.Entities;
using Personeria.Domain;
using Personeria.Domain.Relation;
using Personeria.TransactTypeAreas.Dto;
using System.Collections.Generic;

namespace Personeria.TransactTypes.Dto
{
    [AutoMap(typeof(TransactType))]
    public class TransactTypeSimpleDto : Entity<long>
    {
        public TransactTypeSimpleDto()
        {
            TransactTypeAreaRelations = new HashSet<TransactTypeAreaDto>();
            TransactTypeFileAttachTypeRelations = new HashSet<TransactTypeFileAttachType>();
        }

        public string Name { get; set; }

        public virtual HashSet<TransactTypeAreaDto> TransactTypeAreaRelations { get; set; }
        public virtual HashSet<TransactTypeFileAttachType> TransactTypeFileAttachTypeRelations { get; set; }

    }
}
