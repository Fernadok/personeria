﻿using Abp.AutoMapper;
using Abp.Domain.Entities;
using Personeria.TransactTypeAreas.Dto;
using Personeria.TransactTypeFileAttachTypes.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Personeria.TransactTypes.Dto
{
    [AutoMap(typeof(Domain.TransactType))]
    public class TransactTypeGetDto : Entity<long>
    {
        public TransactTypeGetDto()
        {
            TransactTypeAreaRelations = new HashSet<TransactTypeAreaDto>();
            TransactTypeFileAttachTypeRelations = new HashSet<TransactTypeFileAttachTypeDto>();
        }
        public string Name { get; set; }
        public virtual HashSet<TransactTypeAreaDto> TransactTypeAreaRelations { get; set; }
        public virtual HashSet<TransactTypeFileAttachTypeDto> TransactTypeFileAttachTypeRelations { get; set; }
    }
}
