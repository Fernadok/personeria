﻿using Abp.AutoMapper;
using Abp.Domain.Entities;
using Personeria.TransactTypeAreas.Dto;
using System.Collections.Generic;

namespace Personeria.TransactTypes.Dto
{
    [AutoMapTo(typeof(Domain.TransactType))]
    public class TransactTypeInputDto : Entity<long>
    {
        public TransactTypeInputDto()
        {
            Areas = new HashSet<TransactTypeAreaDto>();
            Files = new HashSet<FileAttachForTypeDto>();
        }

        public string Name { get; set; }

        public HashSet<TransactTypeAreaDto> Areas { get; set; }
        public HashSet<FileAttachForTypeDto> Files { get; set; }
    }
}
