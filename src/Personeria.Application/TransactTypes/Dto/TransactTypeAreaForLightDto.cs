﻿using Abp.AutoMapper;
using Personeria.Domain.Relation;

namespace Personeria.TransactTypes.Dto
{
    [AutoMap(typeof(TransactTypeArea))]
    public class TransactTypeAreaForLightDto
    {
        public TransactTypeAreaForLightDto()
        {
            Order = 0;
        }

        public long Id { get; set; }
        public long TransactTypeId { get; set; }
        public long AreaId { get; set; }
        public string AreaName { get; set; }
        public int Order { get; set; }
    }
}
