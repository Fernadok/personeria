﻿using Abp.AutoMapper;
using Personeria.Domain;
using Personeria.TransactTypeAreas.Dto;
using Personeria.TransactTypeFileAttachTypes.Dto;
using System.Collections.Generic;

namespace Personeria.TransactTypes.Dto
{
    [AutoMap(typeof(TransactType))]
    public class TransactTypeLightDto
    {
        public TransactTypeLightDto()
        {
            TransactTypeFileAttachTypeRelations = new HashSet<TransactTypeFileAttachTypeDto>();
            TransactTypeAreaRelations = new HashSet<TransactTypeAreaDto>();
        }

        public string Name { get; set; }

        public virtual HashSet<TransactTypeFileAttachTypeDto> TransactTypeFileAttachTypeRelations { get; set; }
        public virtual HashSet<TransactTypeAreaDto> TransactTypeAreaRelations { get; set; }
    }
}
