﻿using Abp.AutoMapper;
using Abp.Domain.Entities;
using Newtonsoft.Json;
using Personeria.Domain;
using Personeria.Transacts.Dto;
using System.Collections.Generic;

namespace Personeria.TransactTypes.Dto
{
    [AutoMap(typeof(TransactType))]
    public class TransactTypeDto
    {        
        public long Id { get; set; }
        public string Name { get; set; }

        public int Count { get; set; }
    }
}
