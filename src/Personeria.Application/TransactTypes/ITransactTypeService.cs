﻿using Abp.Application.Services;
using System.Collections.Generic;
using Personeria.Base;
using Personeria.TransactTypes.Dto;
using System.Threading.Tasks;

namespace Personeria.TransactTypes
{
    public interface ITransactTypeService : IApplicationService
    {
        Task<TransactTypeGetDto> GetById(long id);

        ContextDto<TransactTypeDto> GetAll(SearchDto filters);

        Task<long> AddOrUpdate(TransactTypeInputDto dto);

        Task Delete(long id);

        List<IdDescription> TransactTypeList();
    }
}
