﻿using Personeria.Enums;

namespace Personeria.Notifications.Dto
{
    public class NotificationInputDto
    {
        public NotificationInputDto()
        {
            State = NotificationState.All;
        }
        public NotificationState State { get; set; }
    }
}
