﻿using Abp.Notifications;
using Personeria.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Personeria.Notifications.Dto
{
    public class NotificationDto: EntityDto<Guid>
    {
        public UserNotificationState State { get; set; }
        public dynamic Content { get; set; }
        public string StateName
        {
            get
            {
                return L(State.ToString());
            }
        }
    }
}
