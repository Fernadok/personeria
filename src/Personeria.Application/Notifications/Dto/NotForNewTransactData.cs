﻿using Abp.Notifications;
using System;

namespace Personeria.Notifications.Dto
{
    [Serializable]
    public class NotForNewTransactData: NotificationData
    {
        public NotForNewTransactData(string title,string message, string url,long? transactId)
        {
            Title = title;
            Message = message;
            Url = url;
            TransactId = transactId;
        }

        public string Title { get; set; }
        public string Message { get; set; }
        public string Url { get; set; }
        public long? TransactId { get; set; }
    }
}
