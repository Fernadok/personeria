﻿using Abp;
using Abp.Auditing;
using Abp.Dependency;
using Abp.Notifications;
using Personeria.Authorization.Users;
using Personeria.Enums;
using Personeria.Notifications.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Personeria.Notifications
{
    public class NotificationAppService : PersoneriaAppServiceBase, INotificationAppService, ITransientDependency
    {
        private readonly INotificationSubscriptionManager _notificationSubscriptionManager;
        private readonly INotificationPublisher _notiticationPublisher;
        private readonly IUserNotificationManager _userNotificationManager;

        public NotificationAppService(INotificationSubscriptionManager notificationSubscriptionManager,
                                   INotificationPublisher notiticationPublisher,
                                   IUserNotificationManager userNotificationManager)
        {
            this._notificationSubscriptionManager = notificationSubscriptionManager;
            this._notiticationPublisher = notiticationPublisher;
            this._userNotificationManager = userNotificationManager;
        }

        #region Methods for Notifications General

        // -- Get All Notifications
        [DisableAuditing]
        public async Task<List<NotificationDto>> GetAllNotifications(NotificationInputDto dto)
        {
            var user = new UserIdentifier(AbpSession.TenantId, AbpSession.UserId.Value);
            var notifications = dto.State == NotificationState.Unread ?
                                await _userNotificationManager.GetUserNotificationsAsync(user, state: UserNotificationState.Unread) :
                                dto.State == NotificationState.Read ?
                                await _userNotificationManager.GetUserNotificationsAsync(user, state: UserNotificationState.Read) :
                                await _userNotificationManager.GetUserNotificationsAsync(user);
            var notificationResults = notifications.Select(x => new NotificationDto
            {
                Id = x.Id,
                State = x.State,
                Content = x.Notification,
            }).ToList();

            return notificationResults;
        }

        // -- Get Count Notifications Unread
        [DisableAuditing]
        public async Task<int> GetCountNotifications()
        {
            return await _userNotificationManager.GetUserNotificationCountAsync(new UserIdentifier(AbpSession.TenantId, AbpSession.UserId.Value), UserNotificationState.Unread);
        }

        // -- Mark All Notifications as Read
        [DisableAuditing]
        public async Task MarkAllAsRead()
        {
            var user = new UserIdentifier(AbpSession.TenantId, AbpSession.UserId.Value);
            await _userNotificationManager.UpdateAllUserNotificationStatesAsync(user, UserNotificationState.Read);
        }

        // -- Mark Notification as Read
        [DisableAuditing]
        public async Task MarkAsRead(Guid id)
        {
            var user = new UserIdentifier(AbpSession.TenantId, AbpSession.UserId.Value);
            await _userNotificationManager.UpdateUserNotificationStateAsync(AbpSession.TenantId, id, UserNotificationState.Read);
        }

        // -- Mark Notification as Read
        [DisableAuditing]
        public async Task DeleteAll()
        {
            var user = new UserIdentifier(AbpSession.TenantId, AbpSession.UserId.Value);
            await _userNotificationManager.DeleteAllUserNotificationsAsync(user);
        }

        #endregion

        #region Notification For New Transact
        // -- Subscribe  
        [DisableAuditing]
        public async Task Subscribe_SentNotForNewTransact(long userId)
        {
            var existSubscribe = await IsSuscribe_SentNotForNewTransact(userId);
            if (!existSubscribe)
            {
                await _notificationSubscriptionManager.SubscribeAsync(new UserIdentifier(AbpSession.TenantId, userId), NotificationType.NotForNewTransact.ToString());
            }
        }

        // -- Send Notification
        [DisableAuditing]
        public async Task Publish_SentNotForNewTransact(long? transactId)
        {
            if (transactId.HasValue)
            {
                // -- Get Consultation
                var trans = TransactRepository
                            .GetAllIncluding(x => x.Person)
                            .FirstOrDefault(x => x.Id == transactId.Value);
                if (trans != null)
                {
                    // -- Return users Subscribe                                        
                    var subscriptions = await _notificationSubscriptionManager.GetSubscriptionsAsync(AbpSession.TenantId, NotificationType.NotForNewTransact.ToString());

                    IQueryable<long> usersInAreas;
                    if (AbpSession.UserId.HasValue && AbpSession.UserId.Value != 0)
                    {
                        // -- Return users for Area New Transact with ignore user logued
                        usersInAreas = UserRepository
                                            .GetAll()
                                            .Where(x => x.IsActive && x.Id != AbpSession.UserId.Value && x.UserAreaRelations.Any(a => a.AreaId == trans.AreaId))
                                            .Select(x => x.Id);
                    }
                    else
                    {
                        // -- Return users for Area New Transact
                        usersInAreas = UserRepository
                                            .GetAll()
                                            .Where(x => x.IsActive && x.UserAreaRelations.Any(a => a.AreaId == trans.AreaId))
                                            .Select(x => x.Id);
                    }
                    // -- Filters for user in area
                    var userIdIndetifiers = subscriptions
                                                .Where(x => usersInAreas.Any(u => u == x.UserId))
                                                .Select(x => new UserIdentifier(x.TenantId, x.UserId))
                                                .ToArray();

                    if (userIdIndetifiers.Any())
                    {
                        await _notiticationPublisher.PublishAsync(NotificationType.NotForNewTransact.ToString(),
                                    new NotForNewTransactData($"NUEVO TRAMITE | REG. #{trans.Id}",
                                                              $"TRAMITE DE {trans.Person.Name.ToUpper()} {trans.Person.Surname.ToUpper()}",
                                                              $"transacts.detail",
                                                              trans.Id),
                                    userIds: userIdIndetifiers);
                    }
                }
            }
        }


        // -- Unsuscribe
        [DisableAuditing]
        public async Task Desubscribe_SentNotForNewTransact(long userId)
        {
            var existSubscribe = await IsSuscribe_SentNotForNewTransact(userId);
            if (existSubscribe)
            {
                await _notificationSubscriptionManager.UnsubscribeAsync(new UserIdentifier(AbpSession.TenantId, userId), NotificationType.NotForNewTransact.ToString());
            }
        }

        // -- IsSuscribe
        [DisableAuditing]
        public async Task<bool> IsSuscribe_SentNotForNewTransact(long userId)
        {
            return await _notificationSubscriptionManager.IsSubscribedAsync(new UserIdentifier(AbpSession.TenantId, userId), NotificationType.NotForNewTransact.ToString());
        }
        #endregion

        #region Notification For Change Transact
        // -- Subscribe  
        [DisableAuditing]
        public async Task Subscribe_SentNotForChangeTransact(long userId)
        {
            var existSubscribe = await IsSuscribe_SentNotForChangeTransact(userId);
            if (!existSubscribe)
            {
                await _notificationSubscriptionManager.SubscribeAsync(new UserIdentifier(AbpSession.TenantId, userId), NotificationType.NotForChangeTransact.ToString());
            }
        }

        // -- Send Notification
        [DisableAuditing]
        public async Task Publish_SentNotForChangeTransact(long? transactId, bool isUser = false, bool isRevision = false)
        {
            if (transactId.HasValue)
            {
                // -- Get Consultation
                var trans = TransactRepository
                            .GetAllIncluding(x => x.Person)
                            .FirstOrDefault(x => x.Id == transactId.Value);
                if (trans != null)
                {
                    // -- Return users Subscribe                                        
                    var subscriptions = await _notificationSubscriptionManager.GetSubscriptionsAsync(AbpSession.TenantId, NotificationType.NotForChangeTransact.ToString());

                    IQueryable<long> usersInAreas;
                    if (AbpSession.UserId.HasValue && AbpSession.UserId.Value != 0)
                    {
                        // -- Return users for Area New Transact with ignore user logued
                        usersInAreas = UserRepository
                                            .GetAll()
                                            .Where(x => x.IsActive && x.Id != AbpSession.UserId.Value && x.UserAreaRelations.Any(a => a.AreaId == trans.AreaId))
                                            .Select(x => x.Id);
                    }
                    else
                    {
                        // -- Return users for Area New Transact
                        usersInAreas = UserRepository
                                            .GetAll()
                                            .Where(x => x.IsActive && x.UserAreaRelations.Any(a => a.AreaId == trans.AreaId))
                                            .Select(x => x.Id);
                    }
                    // -- Filters for user in area
                    var userIdIndetifiers = subscriptions
                                                .Where(x => usersInAreas.Any(u => u == x.UserId))
                                                .Select(x => new UserIdentifier(x.TenantId, x.UserId))
                                                .ToArray();

                    if (userIdIndetifiers.Any())
                    {
                        User user;
                        var desUser = "";
                        if (isUser && AbpSession.UserId.HasValue && AbpSession.UserId.Value != 0)
                        {
                            user = await UserRepository.FirstOrDefaultAsync(x => x.Id == AbpSession.UserId.Value);
                            desUser = $"REALIZADOS POR {user.FullName.ToUpper()}";
                        }

                        var title = isRevision ? $"REVISIÓN EN TRAMITE | REG. #{trans.Id}" : $"CAMBIOS EN TRAMITE | REG. #{trans.Id}";
                        var description = isRevision ? $"REALIZADOS POR {trans.Person.Name.ToUpper()} {trans.Person.Surname.ToUpper()}" : desUser;
                        var state = isRevision ? NotificationType.NotForRevisionTransact.ToString() : NotificationType.NotForChangeTransact.ToString();

                        await _notiticationPublisher.PublishAsync(state,
                                   new NotForNewTransactData(title,
                                                             description,
                                                             $"transacts.detail",
                                                             trans.Id),
                                   userIds: userIdIndetifiers);
                    }
                }
            }
        }


        // -- Unsuscribe
        [DisableAuditing]
        public async Task Desubscribe_SentNotForChangeTransact(long userId)
        {
            var existSubscribe = await IsSuscribe_SentNotForChangeTransact(userId);
            if (existSubscribe)
            {
                await _notificationSubscriptionManager.UnsubscribeAsync(new UserIdentifier(AbpSession.TenantId, userId), NotificationType.NotForChangeTransact.ToString());
            }
        }

        // -- IsSuscribe
        [DisableAuditing]
        public async Task<bool> IsSuscribe_SentNotForChangeTransact(long userId)
        {
            return await _notificationSubscriptionManager.IsSubscribedAsync(new UserIdentifier(AbpSession.TenantId, userId), NotificationType.NotForChangeTransact.ToString());
        }
        #endregion

    }
}
