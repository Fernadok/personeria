﻿using Abp.Application.Services;
using Personeria.Notifications.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Personeria.Notifications
{
    public interface INotificationAppService : IApplicationService
    {
        Task<List<NotificationDto>> GetAllNotifications(NotificationInputDto dto);
        Task<int> GetCountNotifications();
        Task MarkAllAsRead();
        Task MarkAsRead(Guid id);
        Task DeleteAll();


        [RemoteService(false)]
        Task Subscribe_SentNotForNewTransact(long userId);
        [RemoteService(false)]
        Task Desubscribe_SentNotForNewTransact(long userId);
        [RemoteService(false)]
        Task Publish_SentNotForNewTransact(long? transactId);
        [RemoteService(false)]
        Task<bool> IsSuscribe_SentNotForNewTransact(long userId);

        [RemoteService(false)]
        Task Subscribe_SentNotForChangeTransact(long userId);
        [RemoteService(false)]
        Task Desubscribe_SentNotForChangeTransact(long userId);
        [RemoteService(false)]
        Task Publish_SentNotForChangeTransact(long? transactId, bool isUser = false, bool isRevision = false);
        [RemoteService(false)]
        Task<bool> IsSuscribe_SentNotForChangeTransact(long userId);
    }
}