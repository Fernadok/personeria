﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Personeria.MultiTenancy.Dto;

namespace Personeria.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}
