﻿using Personeria.Email.Templ;
using Personeria.Providers;
using System.Net.Mail;

namespace Personeria.Email
{
    public class EmailService : PersoneriaAppServiceBase, IEmailService
    {
        private readonly INetWorkProvider _netWorkProvider;
        private string EmailFrom = string.Empty;

        public EmailService(INetWorkProvider netWorkProvider)
        {
            _netWorkProvider = netWorkProvider;
            EmailFrom = _netWorkProvider.GetAppSettings("EmailFrom");
        }

        public void SendMessage(string subject, string correoTo, string surname, string name, string message, string url)
        {
            using (var client = new SmtpClient())
            {
                var mail = new MailMessage
                {
                    From = new MailAddress(EmailFrom, "Personeria"),
                    Subject = subject,
                    IsBodyHtml = true,
                    Body = string.Format(TemplateMail.MessageNotifyChangeState, name, surname, url, message)
                };

                mail.Priority = MailPriority.Normal;
                mail.To.Add(correoTo);

                client.Send(mail);
            }
        }
    }
}
