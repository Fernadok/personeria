﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Personeria.Email
{
    public interface IEmailService:IApplicationService
    {
        void SendMessage(string subject, string correoTo , string surname, string name, string message,string url);
    }
}
