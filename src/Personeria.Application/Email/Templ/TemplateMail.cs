﻿namespace Personeria.Email.Templ
{
    public static class TemplateMail
    {
        public static string MessageNotifyChangeState = @"<center class='x_wrapper' style='display:table; table-layout:fixed; width:100%; min-width:620px;'>
                                                    <div class='x_spacer' style='font-size:1px; line-height:20px; width:100%'>&nbsp;</div>
                                                    <table class='x_header x_centered' style='border-collapse:collapse; border-spacing:0; margin-left:auto; margin-right:auto; width:100%'>
                                                    <tbody>
                                                    <tr>
                                                    <td style = 'padding:0; vertical-align:top' > &nbsp;</td>
                                                    <td class='x_logo x_emb-logo-padding-box' style='padding:0; vertical-align:top; width:600px; padding-top:8px; padding-bottom:18px'>
                                                    <div class='x_logo-center' align='center' id='x_emb-email-header' style='font-family: Bitter, Georgia, serif, serif, EmojiFont; color: rgb(119, 142, 163); text-align: center; font-size: 0px !important; line-height: 0 !important;'>
                                                    <img data-imagetype='External' src='http://personeria.sanluis.gov.ar/images/logo-personeria-negro.png?width=300' alt='' width='216' height='59' style='border:0; display:block; margin-left:auto; margin-right:auto; max-width:216px'></div>
                                                    </td>
                                                    <td style = 'padding:0; vertical-align:top' > &nbsp;</td>
                                                    </tr>
                                                    </tbody>
                                                    </table>
                                                    <table class='x_one-col-bg' style='border-collapse:collapse; border-spacing:0; width:100%'>
                                                    <tbody>
                                                    <tr>
                                                    <td align = 'center' style='padding:0; vertical-align:top'>
                                                    <table class='x_one-col x_centered x_column-bg' style='border-collapse:collapse; border-spacing:0; margin-left:auto; margin-right:auto; background-color:#ffffff; border-radius:6px; -moz-border-radius:6px; width:600px; table-layout:fixed'>
                                                    <tbody>
                                                    <tr>
                                                    <td class='x_column' style='padding:0; vertical-align:top; text-align:left'>
                                                    <div>
                                                    <div class='x_column-top' style='font-size:32px; line-height:32px'>&nbsp;</div>
                                                    </div>
                                                    <table class='x_contents' style='border-collapse:collapse; border-spacing:0; table-layout:fixed; width:100%'>
                                                    <tbody>
                                                    <tr>
                                                    <td class='x_padded' style='padding:0; vertical-align:top; padding-left:32px; padding-right:32px; word-break:break-word; word-wrap:break-word; text-align:left'>
                                                    <h1 class='x_font-roboto x_size-32' style='font-style:normal; font-weight:400; font-family:roboto,tahoma,sans-serif; margin-bottom:16px; margin-top:0; font-size:32px; line-height:40px; color:#99a389; text-align:center'>
                                                    <span style = 'color:#222222' > Hola {0} {1},</span></h1>
                                                    </td>
                                                    </tr>
                                                    </tbody>
                                                    </table>
                                                    <table class='x_contents' style='border-collapse:collapse; border-spacing:0; table-layout:fixed; width:100%'>
                                                    <tbody>
                                                    <tr>
	                                                    <td class='x_padded' style='padding:0; vertical-align:top; padding-left:32px; padding-right:32px; word-break:break-word; word-wrap:break-word; text-align:left'>
	                                                    <p class='x_font-roboto x_size-16' style='font-style:normal; font-weight:400; font-family:roboto,tahoma,sans-serif; margin-bottom:20px; margin-top:0; font-size:16px; line-height:24px; color:#808285; text-align:center'>
	                                                    <span style = 'color:#222222'> {3}</span></p>
	                                                    <p class='x_font-roboto x_size-16' style='font-style:normal; font-weight:400; font-family:roboto,tahoma,sans-serif; margin-bottom:20px; margin-top:0; font-size:16px; line-height:24px; color:#808285; text-align:center'>
	                                                    <a href='{2}' style='color:#222222'>Ver tr&aacute;mite</a></p>
	                                                    <hr/>
                                                        <p class='x_font-roboto x_size-16' style='font-style:normal; font-weight:400; font-family:roboto,tahoma,sans-serif; margin-bottom:20px; margin-top:0; font-size:16px; line-height:24px; color:#808285; text-align:center'>
	                                                    <span style = 'color:#222222'><strong style= 'font-weight:bold'>POR CUALQUIER DUDA O CONSULTA, CONTACTENOS. </strong></span></p>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                    </table>
                                                    <div class='x_column-bottom' style='font-size:32px; line-height:32px'>&nbsp;</div>
                                                    </td>
                                                    </tr>
                                                    </tbody>
                                                    </table>
                                                    </td>
                                                    </tr>
                                                    </tbody>
                                                    </table>
                                                    <div class='x_separator' style='font-size:20px; line-height:20px'>&nbsp;</div>
                                                    <table class='x_one-col-bg' style='border-collapse:collapse; border-spacing:0; width:100%'>
                                                    <tbody>
                                                    <tr>
                                                    <td align = 'center' style='padding:0; vertical-align:top'>
                                                    </td>
                                                    </tr>
                                                    </tbody>
                                                    </table>
                                                    <div class='x_separator' style='font-size:20px; line-height:20px'>&nbsp;</div>
                                                    </center>";
    }
}
