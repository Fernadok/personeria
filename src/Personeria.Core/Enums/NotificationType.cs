﻿namespace Personeria.Enums
{
    public enum NotificationType
    {
        NotForNewTransact = 0,
        NotForChangeTransact = 1,
        NotForRevisionTransact = 2
    }
}
