﻿namespace Personeria.Enums
{
    public enum TransactState
    {
        All = -1,
        InProcess = 0,
        InPause = 1,
        Locked = 2,
        Finished = 3
    }
}
