﻿namespace Personeria.Enums
{
    public enum NotificationState
    {
        All = 0,
        Unread = 1,
        Read = 2
    }
}
