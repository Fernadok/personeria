﻿namespace Personeria.Enums
{
    public static class HistoryMessages
    {
        public const string NewTransact = "NUEVO TRÁMITE: Iniciado por {0} | N° {1} |  Se encuentra en área {2} en el estado {3}.";
        public const string ChangeTransact = "CAMBIOS EN TRÁMITE: N° {0} | El tramite se encuentra en área {1} en el estado {2}.";
        public const string FinishTransact = "TRÁMITE FINALIZADO: N° {0} | Se encuentra en área {1} en el estado {2}.";
        public const string ReOpenTransact = "RE-APERTURA DE TRÁMITE: N° {0} | Se encuentra en área {1} en el estado {2}.";
        public const string BackArea = "DEVOLUCION DE TRÁMITE: N° {0} | Se encuentra en área {1} en el estado {2}.";
        public const string SendOtherArea = "AREA ASIGNADA TRÁMITE: N° {0} | Se encuentra en área {1} en el estado {2}.";
        public const string RevisionTransact = "REVISIÓN DE TRÁMITE: N° {0} | Se require que revise el tramite que se encuentra en área {1} con estado {2}.";
        public const string RevisionTransactBack = "REVISIÓN DE TRÁMITE: N° {0} | Se genero una revisión del tramite correctamente de {1}, ahora se encuentra en área {2} con estado {3}.";

    }
}
