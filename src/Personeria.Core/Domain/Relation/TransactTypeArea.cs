﻿using Personeria.Base;

namespace Personeria.Domain.Relation
{
    public class TransactTypeArea: AuditableEntity
    {
        public TransactTypeArea()
        {
            Order = 0;
        }

        public long TransactTypeId { get; set; }
        public virtual TransactType TransactType { get; set; }


        public long AreaId { get; set; }
        public virtual Area Area { get; set; }


        public int Order { get; set; }
    }
}
