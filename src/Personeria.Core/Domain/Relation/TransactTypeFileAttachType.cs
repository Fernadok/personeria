﻿using System.Collections.Generic;
using Personeria.Base;

namespace Personeria.Domain.Relation
{
    public class TransactTypeFileAttachType: EntityBase
    {
        public TransactTypeFileAttachType()
        {
            IsRequired = false;
            FileAttachs = new HashSet<FileAttach>();
        }

        public long FileAttachTypeId { get; set; }
        public virtual FileAttachType FileAttachType { get; set; }

        public long TransactTypeId { get; set; }
        public virtual TransactType TransactType { get; set; }

        public bool IsRequired { get; set; }
        public virtual HashSet<FileAttach> FileAttachs { get; set; }
    }
}
