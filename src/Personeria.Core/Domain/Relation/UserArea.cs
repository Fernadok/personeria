﻿using Personeria.Authorization.Users;
using Personeria.Base;

namespace Personeria.Domain.Relation
{
    public class UserArea: AuditableEntity
    {
        public long UserId { get; set; }
        public virtual User User { get; set; }


        public long AreaId { get; set; }
        public virtual Area Area { get; set; }
    }
}
