﻿using Personeria.Base;
using Personeria.Domain.Relation;
using System.Collections.Generic;

namespace Personeria.Domain
{
    public class FileAttachType: AuditableEntity
    {
        public FileAttachType()
        {
            TransactTypeFileAttachTypeRelations = new HashSet<TransactTypeFileAttachType>();
        }

        public string Name { get; set; }
        public virtual HashSet<TransactTypeFileAttachType> TransactTypeFileAttachTypeRelations { get; set; }
    }
}