﻿using Personeria.Base;
using Personeria.Domain.Relation;

namespace Personeria.Domain
{
    public class FileAttach : AuditableEntity
    {
        public FileAttach()
        {
            Store = PersoneriaConsts.PersonKey;
        }

        // -- Propiedades para guardar archivos de configuración
        public string Title { get; set; }
        public string Link { get; set; }


        public string Path { get; set; }
        public long Size { get; set; }
        public string FileName { get; set; }
        public string Store { get; set; }


        public long? TransactId { get; set; }
        public virtual Transact Transact { get; set; }


        public long? TransactTypeFileAttachTypeId { get; set; }
        public virtual TransactTypeFileAttachType TransactTypeFileAttachType { get; set; }
    }
}
