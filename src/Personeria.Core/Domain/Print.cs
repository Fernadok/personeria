﻿using Personeria.Authorization.Users;
using Personeria.Base;
using System.Collections.Generic;

namespace Personeria.Domain
{
    public class Print : EntityBase
    {
        public string Name { get; set; }
        public string Ip { get; set; }

        public virtual HashSet<User> Users { get; set; }
    }
}
