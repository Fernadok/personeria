﻿using Personeria.Base;
using System.Collections.Generic;

namespace Personeria.Domain
{
    public class Location : EntityBase
    {
        public Location()
        {
            Persons = new HashSet<Person>();
            Transacts = new HashSet<Transact>();
        }

        public string Name { get; set; }
        public string PostalCode { get; set; }

        public long DepartmentId { get; set; }
        public virtual Department Department { get; set; }

        public virtual HashSet<Person> Persons { get; set; }
        public virtual HashSet<Transact> Transacts { get; set; }
    }
}
