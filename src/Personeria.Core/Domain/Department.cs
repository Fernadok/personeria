﻿using Personeria.Base;
using System.Collections.Generic;

namespace Personeria.Domain
{
    public class Department: EntityBase
    {
        public Department()
        {
            Locations = new HashSet<Location>();
        }
        #region Properties

        public string Name { get; set; }

        public long StateId { get; set; }
        public virtual State State { get; set; }

        public virtual HashSet<Location> Locations { get; set; }

        #endregion        
    }
}
