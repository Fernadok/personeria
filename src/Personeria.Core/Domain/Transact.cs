﻿using Personeria.Base;
using Personeria.Enums;
using System;
using System.Collections.Generic;

namespace Personeria.Domain
{
    public class Transact : AuditableEntity
    {
        public Transact()
        {
            State = TransactState.InProcess;
            Histories = new HashSet<History>();
            FileAttachs = new HashSet<FileAttach>();
        }

        public string Subject { get; set; }
        public TransactState State { get; set; }
        public string Observation { get; set; }


        public long AreaId { get; set; }
        public virtual Area Area { get; set; }

        public long? LockedAreaId { get; set; }

        public long PersonId { get; set; }
        public virtual Person Person { get; set; }


        public long TransactTypeId { get; set; }
        public virtual TransactType TransactType { get; set; }


        public long LocationId { get; set; }
        public virtual Location Location { get; set; }

        public Guid? Revision { get; set; }

        public virtual HashSet<History> Histories { get; set; }
        public virtual HashSet<FileAttach> FileAttachs { get; set; }        
    }
}