﻿using Personeria.Base;
using Personeria.Domain.Relation;
using System.Collections.Generic;

namespace Personeria.Domain
{
    public class TransactType: AuditableEntity
    {
        public TransactType()
        {
            Transacts = new HashSet<Transact>();
            TransactTypeFileAttachTypeRelations = new HashSet<TransactTypeFileAttachType>();
            TransactTypeAreaRelations = new HashSet<TransactTypeArea>();
        }

        public string Name { get; set; }

        public virtual HashSet<Transact> Transacts { get; set; }
        public virtual HashSet<TransactTypeFileAttachType> TransactTypeFileAttachTypeRelations { get; set; }
        public virtual HashSet<TransactTypeArea> TransactTypeAreaRelations { get; set; }
    }
}