﻿using Personeria.Base;
using System.Collections.Generic;

namespace Personeria.Domain
{
    public class Person: AuditableEntity
    {
        public Person()
        {
            Transacts = new HashSet<Transact>();
        }

        public string Name { get; set; }
        public string Surname { get; set; }
        public string NumberIdentity { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Adreess { get; set; }


        public long LocationId { get; set; }
        public virtual Location Location { get; set; }

        public virtual HashSet<Transact> Transacts { get; set; }
    }
}