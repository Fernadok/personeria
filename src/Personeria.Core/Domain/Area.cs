﻿using Personeria.Base;
using Personeria.Domain.Relation;
using System.Collections.Generic;

namespace Personeria.Domain
{
    public class Area : AuditableEntity
    {
        public Area()
        {
            UserAreaRelations = new HashSet<UserArea>();
            TransactTypeAreaRelations = new HashSet<TransactTypeArea>();
            Transacts = new HashSet<Transact>();
        }

        public string Name { get; set; }
        public string Description { get; set; }

        public virtual HashSet<UserArea> UserAreaRelations { get; set; }
        public virtual HashSet<TransactTypeArea> TransactTypeAreaRelations { get; set; }

        public virtual HashSet<Transact> Transacts { get; set; }
    }
}