﻿using Personeria.Authorization.Users;
using Personeria.Base;
using Personeria.Enums;
using System;

namespace Personeria.Domain
{
    public class History: EntityBase
    {
        public History()
        {
            CreateDate = DateTime.Now;
            State = TransactState.InProcess;
        }

        public DateTime CreateDate { get; set; }

        public long? UserId { get; set; }
        public virtual User User { get; set; }

        public long TransactId { get; set; }
        public virtual Transact Transact { get; set; }

        public TransactState State { get; set; }
        public string Observation { get; set; }
        public string Ip { get; set; }
    }
}