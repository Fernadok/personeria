﻿using Personeria.Base;
using System.Collections.Generic;

namespace Personeria.Domain
{
    public class State: EntityBase
    {
        public State()
        {
            Departments = new HashSet<Department>();
        }

        #region Properties

        public string Name { get; set; }

        public virtual HashSet<Department> Departments { get; set; }

        #endregion

        
    }
}
