﻿namespace Personeria
{
    public class PersoneriaConsts
    {
        public const string LocalizationSourceName = "Personeria";

        public const bool MultiTenancyEnabled = false;

        public const string ConfigKey = "CONFIG";
        public const string PersonKey = "PERSON";
        public const string UserKey = "USER";
    }
}