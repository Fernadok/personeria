﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace Personeria.Authorization
{
    public class PersoneriaAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            context.CreatePermission(PermissionNames.Pages_Users, L("Users"));
            context.CreatePermission(PermissionNames.Pages_Roles, L("Roles"));
            context.CreatePermission(PermissionNames.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);

            context.CreatePermission(PermissionNames.Pages_Areas, L("Areas"));
            context.CreatePermission(PermissionNames.Pages_Peoples, L("Peoples"));
            context.CreatePermission(PermissionNames.Pages_Transacts, L("Transacts"));
            context.CreatePermission(PermissionNames.Pages_TransactTypes, L("TransactTypes"));                                    
            context.CreatePermission(PermissionNames.Pages_Config, L("Configurations"));
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, PersoneriaConsts.LocalizationSourceName);
        }
    }
}
