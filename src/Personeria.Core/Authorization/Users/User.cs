﻿using System;
using System.Collections.Generic;
using System.Linq;
using Abp.Authorization.Users;
using Abp.Extensions;
using Microsoft.AspNet.Identity;
using MoreLinq;
using Personeria.Domain;
using Personeria.Domain.Relation;

namespace Personeria.Authorization.Users
{
    public class User : AbpUser<User>
    {
        public User()
        {
            Histories = new HashSet<History>();
            UserAreaRelations = new HashSet<UserArea>();            
        }

        public const string DefaultPassword = "P@ssword01qweasd";

        public static string CreateRandomPassword()
        {
            return Guid.NewGuid().ToString("N").Truncate(16);
        }

        public static User CreateTenantAdminUser(int tenantId, string emailAddress, string password)
        {
            var user = new User
            {
                TenantId = tenantId,
                UserName = AdminUserName,
                Name = AdminUserName,
                Surname = AdminUserName,
                EmailAddress = emailAddress,
                Password = new PasswordHasher().HashPassword(password)
            };

            return user;
        }

        public virtual HashSet<History> Histories { get; set; }
        public virtual HashSet<UserArea> UserAreaRelations { get; set; }

        public HashSet<long> AreaIds => UserAreaRelations
                                        .Where(x => x.Area.IsActive)
                                        .Select(x => x.AreaId)
                                        .ToHashSet();

        public long? PrintTicketDefaultId { get; set; }
        public virtual Print PrintTicketDefault { get; set; }
    }
}