﻿using Abp.Authorization;
using Personeria.Authorization.Roles;
using Personeria.Authorization.Users;

namespace Personeria.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
