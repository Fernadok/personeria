﻿namespace Personeria.Authorization
{
    public static class PermissionNames
    {
        public const string Pages_Tenants = "Pages.Tenants";
        public const string Pages_Users = "Pages.Users";
        public const string Pages_Roles = "Pages.Roles";

        public const string Pages_Areas = "Pages.Areas";
        public const string Pages_Peoples = "Pages.Peoples";
        public const string Pages_Transacts = "Pages.Transacts";
        public const string Pages_TransactTypes = "Pages.TransactTypes";        
        public const string Pages_Config = "Pages.Config";

    }
}