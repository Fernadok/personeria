﻿namespace Personeria.Providers
{
    public interface IWebDomainProvider
    {
        string CurrentDomain { get; }

        bool IsDebug { get; }
    }
}
