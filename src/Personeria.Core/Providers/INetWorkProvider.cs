﻿namespace Personeria.Providers
{
    public interface INetWorkProvider
    {
        string MapPath(string path);
        string GetAppSettings(string key);
        string GetConnetionString();
        string GetCodeQr(string text, int height = 250, int width = 250, int margin = 0);    
    }
}
