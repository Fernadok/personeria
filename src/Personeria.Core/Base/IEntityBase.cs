﻿using Abp.Domain.Entities;

namespace Personeria.Base
{
    public interface IEntityBase<TPrimaryKey>: IEntity<TPrimaryKey>
    {
        bool IsActive { get; set; }
    }
}
