﻿using Abp.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations;

namespace Personeria.Base
{
    public abstract class AuditableEntity<T> : EntityBase<T>, IAuditableEntity
    {
        [ScaffoldColumn(false)]
        public DateTime CreatedDate { get; set; }


        [MaxLength(256)]
        [ScaffoldColumn(false)]
        public string CreatedBy { get; set; }

        [ScaffoldColumn(false)]
        public DateTime UpdatedDate { get; set; }

        [MaxLength(256)]
        [ScaffoldColumn(false)]
        public string UpdatedBy { get; set; }
    }

    public abstract class AuditableEntity : AuditableEntity<long>
    {

    }
}
