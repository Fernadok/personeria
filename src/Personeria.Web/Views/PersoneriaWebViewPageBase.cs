﻿using Abp.Web.Mvc.Views;

namespace Personeria.Web.Views
{
    public abstract class PersoneriaWebViewPageBase : PersoneriaWebViewPageBase<dynamic>
    {

    }

    public abstract class PersoneriaWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected PersoneriaWebViewPageBase()
        {
            LocalizationSourceName = PersoneriaConsts.LocalizationSourceName;
        }
    }
}