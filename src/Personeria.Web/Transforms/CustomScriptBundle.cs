﻿using System;
using System.Web.Optimization;

namespace Personeria.Web.Transforms
{
    public class CustomScriptBundle : Bundle
    {
        public CustomScriptBundle(string virtualPath)
            : this(virtualPath, null)
        {
        }

        public CustomScriptBundle(string virtualPath, string cdnPath)
            : base(virtualPath, cdnPath, null)
        {
            this.ConcatenationToken = ";" + Environment.NewLine;
            this.Builder = new CustomBundleBuilder();
        }
    }
}
