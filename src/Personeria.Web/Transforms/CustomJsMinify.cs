﻿using Microsoft.Ajax.Utilities;
using System;
using System.Web.Optimization;

namespace Personeria.Web.Transforms
{
    public class CustomJsMinify : IItemTransform
    {
        public string Process(string includedVirtualPath, string input)
        {
            if (includedVirtualPath.EndsWith("min.js", StringComparison.OrdinalIgnoreCase))
            {
                return input;
            }

            var codeSettings = new CodeSettings
            {
                EvalTreatment = EvalTreatment.MakeImmediateSafe,
                PreserveImportantComments = false,
                MinifyCode = true
            };
            codeSettings.PreserveImportantComments = false;

            var minifier = new Minifier();
            string str = minifier.MinifyJavaScript(input, codeSettings);

            if (minifier.ErrorList.Count > 0)
                return "/* " + string.Concat(minifier.Errors) + " */";

            return str;
        }
    }
}
