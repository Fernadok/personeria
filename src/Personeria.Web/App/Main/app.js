﻿(function () {
    'use strict';

    var app = angular.module('app', [
        'ngAnimate',
        'ngSanitize',
        'ngTouch',

        'ui.router',
        'ui.bootstrap',
        'ui.jq',

        'cfp.loadingBar',
        'ngFileUpload',
        
        'abp'
    ]);
    //-- Generando constante Config
    // http://personeria.sanluis.gov.ar/
    var config = {
        webDomain: 'http://localhost:6234/'
    };
    app.constant('config', config);

    //Configuration for Angular UI routing.
    app.config([
        '$stateProvider',
        '$urlRouterProvider',
        '$locationProvider',
        '$qProvider',
        'cfpLoadingBarProvider',
        function ($stateProvider,
            $urlRouterProvider,
            $locationProvider,
            $qProvider,
            cfpLoadingBarProvider) {

            $locationProvider.hashPrefix('');
            $urlRouterProvider.otherwise('/');
            $qProvider.errorOnUnhandledRejections(false);
            $locationProvider.html5Mode(true);

            // -- Config Loading Bar
            cfpLoadingBarProvider.includeSpinner = true;
            cfpLoadingBarProvider.latencyThreshold = 500;

            $stateProvider
                .state('home', {
                    url: '/',
                    templateUrl: '/App/Main/views/home/home.cshtml',
                    menu: 'GOBIERNO DE SAN LUIS' //Matches to name of 'Home' menu in PersoneriaNavigationProvider
                })
                .state('transacts', {
                    url: '/tramites/:paramId',
                    menu: 'TRAMITES',
                    views: {
                        'modal': {
                            templateUrl: '/App/main/views/transacts/detailModal.cshtml'
                        }
                    }
                })
                .state('revision', {
                    url: '/revision/:id',
                    menu: 'REVISION',
                    views: {
                        'modal': {
                            templateUrl: '/App/main/views/transacts/revisionModal.cshtml'
                        }
                    }
                });

        }
    ]).run(['$rootScope', '$timeout', 'cfpLoadingBar', '$location', '$window', function ($rootScope, $timeout, cfpLoadingBar, $location, $window) {

        var loadTop = function () {
            $("html,body").animate({ scrollTop: 0 }, 1000);
        };

        var closedLeft = function () {
            $timeout(function () {
                var isOpen = $("body.ls-closed").hasClass("overlay-open");
                if (isOpen) {
                    $(".bars").click();
                }
            }, 200);
        };

        var closedLoading = function () {
            $timeout(function () {
                $('.page-loader').slideUp();
            }, 500);
        };

        if (!navigator.onLine) {
            location.href = "/offline.html";
        }
        $window.addEventListener("offline", function () {
            location.href = "/offline.html?returnUrl=" + $location.absUrl();
        }, false);

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            cfpLoadingBar.start();
        });
        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            cfpLoadingBar.complete();
            document.title = "Personeria | " + toState.menu.toUpperCase();
            closedLeft();
            closedLoading();
        });
        $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams) { });
        $rootScope.$on('$stateNotFound', function (event, toState, toParams, fromState, fromParams) { });
        $rootScope.$on('$viewContentLoading', function (event, viewConfig) { });
        $rootScope.$on('$viewContentLoaded', function (event, viewConfig) {
            //if (!$window.isDebug) {
            //    $window.ga('send', 'pageview', { page: $location.url() });
            //}
        });

    }]);

})();