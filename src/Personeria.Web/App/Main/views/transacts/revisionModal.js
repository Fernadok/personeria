﻿
(function () {
    angular
        .module('app')
        .controller('app.views.transact.revision', func);

    func.$inject = [
        '$window',
        '$location',
        '$uploadService',
        'abp.services.app.transactFromHome',
        'abp.services.app.historyService',
        'abp.services.app.locationService',
        '$stateParams'];

    function func(
        $window,
        $location,
        $uploadService,
        $transactService,
        $historyService,
        $locationService,
        $stateParams) {

        var vm = this;
        var id = $stateParams.id;
        vm.reg = id;
        vm.stateBlocked = false;

        //-- VARIABLES --//
        vm.url = '';
        vm.hasPersonData = false;
        vm.showMessage = false;
        vm.hasTransactTypeData = false;
        vm.transactionName = '';
        vm.locationName = '';
        vm.transactLocationName = '';
        vm.nextStep = false;
        vm.readOnlyPerson = true;
        vm.isNew = (id === null);
        //-- FIN VARIABLES --//

        //-- OBJECT --//
        vm.transact = {
            id: 0,
            search: '',
            person: {
                id: 0,
                fullName: '',
                name: '',
                surname: '',
                numberIdentity: '',
                adreess: '',
                phone: '',
                email: '',
                locationId: 0

            },
            transactLocationId: 0,
            transactType: {}
        };
        vm.documentList = [];
        //-- FIN OBJECT --//
        vm.finishGeneral = true;
        vm.getDataToForm = function () {
            abp.ui.setBusy(".modal-content");
            $transactService.getRevision(id).then(function (result) {

                if (result.data === null) {
                    abp.message.error("No se entro la revisión de este tramite, puede ser que ya fue revisada o que se cancelo el pedido de revisión", "Aviso");
                    $location.url("/");
                    return;
                }

                vm.resultData = result.data;
                vm.transact = vm.resultData;

                document.title = "Personeria | REVISIÓN DEL TRÁMITE #" + vm.transact.id;
                                
                vm.transactionName = vm.resultData.transactTypeName;                                
                vm.transactTypeId = vm.resultData.transactTypeId;
                
                $.each(vm.resultData.fileAttachs, function (index, item) {
                    var file = {};
                    file.progress = 0;
                    file.error = '';
                    file.finish = true;
                    file.data = item || {};
                    file.fileInfo = item || null;
                    file.fileInfo.isRequired = file.fileInfo.title.split('@')[0] === 'true';
                    file.document = file.fileInfo.title.split('@')[1].toUpperCase();
                    file.upload = function (fileIn) {
                        if (fileIn && !fileIn.$error && file.finish) {
                            file.finish = false;
                            vm.finishGeneral = file.finish;
                            $uploadService.uploadFile(fileIn, function (resdata) {
                                file.fileInfo = resdata;
                                file.fileInfo.title = file.data.isRequired + "@" + file.document;
                                file.finish = true;
                                vm.finishGeneral = file.finish;
                                vm.filesReqiered();
                            }, function (progress) {
                                file.progress = progress;
                            });
                        }
                    };
                    vm.documentList.push(file);
                });

                vm.hasPersonData = true;
                vm.hasTransactTypeData = true;
                abp.ui.clearBusy(".modal-content");
            });
        };
        
        vm.openFile = function (file) {
            var url = file.path;
            $window.open(url, '_blank');
        };
        vm.deleteFile = function (file) {
            var inx = vm.documentList.indexOf(file);
            vm.documentList[inx].fileInfo = null;
            vm.filesReqiered();
        };

        vm.saveVisible = false;
        vm.filesReqiered = function () {
            var visible = true;
            $.each(vm.documentList, function (inx, it) {
                if (it.data.isRequired && it.fileInfo == null) {
                    visible = false;
                }
            });

            vm.saveVisible = visible;
        };

        //-- ACTION FORM --//
        vm.cancel = function () {
            $location.url('/');
        };

        // -- Save
        vm.save = function () {
            abp.ui.setBusy(".modal-content");
            vm.transactInput = {
                id: vm.transact.id,
                personId: vm.transact.person.id,
                name: vm.transact.person.name,
                surname: vm.transact.person.surname,
                numberIdentity: vm.transact.person.numberIdentity,
                adreess: vm.transact.person.adreess,
                phone: vm.transact.person.phone,
                email: vm.transact.person.email,
                locationId: vm.transact.person.locationId,
                transactLocationId: vm.transact.transactLocationId,
                transactType: [],
                observation: vm.transact.observation,
                enableRevision: vm.stateBlocked && vm.transact.revision !== null,
                revision: vm.transact.revision
            };
            $.each(vm.documentList, function (ind, it) {
                vm.transactInput.transactType.push({
                    transactTypeId: it.data.transactTypeId || vm.transact.transactType.id,
                    fileAttachTypeId: it.data.fileAttachTypeId,
                    file: it.fileInfo
                });
            });
            $transactService.saveRevision(vm.transactInput).then(function () {
                abp.message.success("Su revisión fue enviada correctamente, ahora el área responsable va a continuar su tramite, no olvide su numero de registro así puede entrar al sistema y ver el progreso que va teniendo su tramite", "Aviso");
                $location.url("/");
            }, function (error) {
                abp.ui.clearBusy(".modal-content");
            });
        };

        vm.getDataToForm();                
    }
})();