﻿(function () {
    angular
        .module('app')
        .controller('app.views.transact.consult', ['$scope',
            '$uibModalInstance',
            '$uibModal',
            'abp.services.app.transactFromHome',
            function func($scope, $uibModalInstance, $uibModal, $transactFromHomeService) {
                var vm = this;

                vm.cancel = function () {
                    $uibModalInstance.dismiss({});
                };

                vm.check = function () {
                    abp.ui.setBusy(".modal-content");
                    $transactFromHomeService.getByRegister(vm.register).then(function (result) {
                        abp.ui.clearBusy(".modal-content");
                        if (result.data === 0) {
                            abp.message.info("No se encontró el trámite, por favor revise el registro", "REGISTRO: #" + vm.register);
                            return;
                        }
                        $uibModalInstance.close(result.data);
                    }, function (error) {
                        abp.ui.clearBusy(".modal-content");
                    });
                };

            }]);
})();