﻿(function () {
    angular
        .module('app')
        .controller('app.views.transact.detail', func);

    func.$inject = [
        '$window',
        'config',
        '$scope',
        '$timeout',
        'abp.services.app.transactService',
        'abp.services.app.historyService',
        'abp.services.app.locationService',
        '$stateParams'];

    function func(
        $window,
        $config,
        $scope,
        $timeout,
        $transactService,
        $historyService,
        $locationService,
        $stateParams) {

        var vm = this;
        var id = $stateParams.paramId;
        vm.reg = id;
        vm.stateBlocked = false;

        //-- VARIABLES --//
        vm.url = '';
        vm.hasPersonData = false;
        vm.showMessage = false;
        vm.hasTransactTypeData = false;
        vm.transactionName = '';
        vm.locationName = '';
        vm.transactLocationName = '';
        vm.nextStep = false;
        vm.readOnlyPerson = true;
        vm.isNew = (id === null);
        //-- FIN VARIABLES --//

        //-- OBJECT --//
        vm.transact = {
            id: 0,
            search: '',
            person: {
                id: 0,
                fullName: '',
                name: '',
                surname: '',
                numberIdentity: '',
                adreess: '',
                phone: '',
                email: '',
                locationId: 0

            },
            transactLocationId: 0,
            transactType: {}
        };
        vm.documentList = [];
        //-- FIN OBJECT --//

        vm.getDataToForm = function () {
            $transactService.getById(id).then(function (result) {
                vm.resultData = result.data;
                vm.stateBlocked = vm.resultData.state === 2;
                vm.transact.id = vm.resultData.id;
                vm.transact.observation = vm.resultData.observation;
                vm.transact.person = vm.resultData.person;
                vm.transact.person.fullName = vm.resultData.fullName;
                vm.transact.codeQr = vm.resultData.codeQr;
                vm.transact.transactType = vm.resultData.transactType;
                vm.transactionName = vm.resultData.transactTypeName;
                vm.transact.transactLocationId = vm.resultData.locationId;

                vm.transactTypeId = vm.resultData.transactTypeId;
                //¿vm.transactTypeSelectedChange();

                $.each(vm.resultData.fileAttachs, function (index, item) {
                    var file = {};
                    file.progress = 0;
                    file.error = '';
                    file.finish = true;
                    file.data = item || {};
                    file.fileInfo = item || null;                    
                    file.fileInfo.isRequired = file.fileInfo.title.split('@')[0] === 'true';
                    file.document = file.fileInfo.title.split('@')[1].toUpperCase();
                    file.upload = function (fileIn) {
                        if (fileIn && !fileIn.$error && file.finish) {
                            file.finish = false;
                            vm.finishGeneral = file.finish;
                            $uploadService.uploadFile(fileIn, function (resdata) {
                                file.fileInfo = resdata;
                                file.fileInfo.title = file.data.isRequired + "@" + file.document;
                                file.finish = true;
                                vm.finishGeneral = file.finish;
                                vm.filesReqiered();
                            }, function (progress) {
                                file.progress = progress;
                            });
                        }
                    }
                    vm.documentList.push(file);
                });

                vm.hasPersonData = true;
                vm.hasTransactTypeData = true;
                
                $locationService.getById(vm.resultData.locationId).then(function (locRes) {
                    vm.transactLocationName = locRes.data.name;
                });
            });
        };

        vm.getHistories = function () {
            $historyService.historyList(id).then(function (res) {
                vm.historyList = res.data;
            });
        };

        vm.openFile = function (file) {
            var url = file.path;
            $window.open(url, '_blank');
        };
        vm.deleteFile = function (file) {
            var inx = vm.documentList.indexOf(file);
            vm.documentList[inx].fileInfo = null;
            vm.filesReqiered();
        }

        vm.saveVisible = false;
        vm.filesReqiered = function () {
            var visible = true;
            $.each(vm.documentList, function (inx, it) {
                if (it.data.isRequired && it.fileInfo == null) {
                    visible = false;
                }
            });

            vm.saveVisible = visible;
        };
        //-- ACTION FORM --//
        vm.cancel = function () {
            $location.url('/tramites');
        };
        
        function printElement(elem) {
            var domClone = elem.cloneNode(true);

            var $printSection = document.getElementById("printSection");

            if (!$printSection) {
                $printSection = document.createElement("div");
                $printSection.id = "printSection";
                document.body.appendChild($printSection);
            }

            $printSection.innerHTML = "";
            $printSection.appendChild(domClone);
            window.print();
        }

        vm.print = function () {
            printElement(document.getElementById("printThis"));
        };
        //-- FIN ACTION FORM --//

        vm.getDataToForm();
        vm.getHistories();
        document.title = "Personeria | TRAMITE #" + id;
    }
})();