﻿(function () {
    var controllerId = 'app.views.home';
    angular
        .module('app')
        .controller(controllerId, [
            '$scope',
            '$state',
            '$timeout',
            'appSession',
            '$uibModal',
            '$stateParams',
            '$location',
            'abp.services.app.configuration',
            function ($scope,
                $state,
                $timeout,
                $appSession,
                $uibModal,
                $stateParams,
                $location,
                $config) {
                var vm = this;

                vm.isLogin = $appSession.user !== null;

                vm.go = function (url) {
                    window.location.href = url;
                };

                vm.myInterval = 3000;
                vm.noWrapSlides = false;
                vm.active = 0;
                vm.slides = [];

                // -- Traigo la imagenes para la galeria configuradas
                $config.getHomePhotos().then(function (result) {
                    result.data.forEach(function (item) {
                        vm.slides.push(item);
                    });
                });

                // -- Consultar tramite
                vm.loadOpen = false;
                vm.consult = function () {
                    if (!vm.loadOpen) {
                        abp.ui.setBusy(".consult-transact");
                        vm.loadOpen = true;
                        var modalInstance = $uibModal.open({
                            templateUrl: '/App/Main/views/transacts/consult.cshtml',
                            controller: 'app.views.transact.consult as vm',
                            backdrop: 'static'
                        });
                        modalInstance.rendered.then(function () {
                            abp.ui.clearBusy(".consult-transact");
                            vm.loadOpen = false;
                            $timeout(function () {
                                $.AdminBSB.input.activate();
                            }, 500);
                        });

                        modalInstance.result.then(function (resId) {                            
                            $state.go('transacts', { paramId: resId }, { reload: true });
                        });
                    }
                };

                vm.createTransact = function (obj) {
                    if (!vm.loadOpen) {
                        abp.ui.setBusy(".new-transact");
                        vm.loadOpen = true;
                        var modalInstance = $uibModal.open({
                            templateUrl: '/App/Main/views/transacts/addOrUpdateModal.cshtml',
                            controller: 'app.views.transact.addOrUpdate as vm',
                            backdrop: 'static',
                            size: 'lg',
                            resolve: {
                                id: function () {
                                    return obj ? obj.id : null;
                                }
                            }
                        });

                        modalInstance.rendered.then(function () {
                            abp.ui.clearBusy(".new-transact");
                            vm.loadOpen = false;
                            $timeout(function () {
                                $.AdminBSB.input.activate();
                            }, 500);
                        });

                        modalInstance.result.then(function (resId) {                            
                            $location.url('/tramites/' + resId);
                        });
                    }
                };

            }
        ]);
})();