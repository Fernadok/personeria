﻿(function () {
    'use strict';
    angular
        .module('admin')
        .directive('printClick', [        
        function () {
            return function (scope, element, attrs) {
                element.bind("click", function (event) {
                    console.log(attrs.printClick);
                    var printSectionId = attrs.printClick;

                    var myPrintContent = document.getElementById(printSectionId);
                    var myPrintWindow = window.open('', '_blank', 'fullscreen=yes,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
                    myPrintWindow.document.write(myPrintContent.innerHTML);

                    var icons = myPrintWindow.document.getElementsByName('hidden_icon');
                    for (var a = 0; a < icons.length; a++) {
                        icons[a].style.display = "none";
                    }

                    var inputs = myPrintWindow.document.getElementsByTagName('input');
                    for (var a = 0; a < inputs.length; a++) {
                        inputs[a].style.display = "none";
                    }

                    var textareas = myPrintWindow.document.getElementsByTagName('textarea');
                    for (var a = 0; a < textareas.length; a++) {
                        textareas[a].style.display = "none";
                    }

                    var spans = myPrintWindow.document.getElementsByClassName('print-hidden');
                    for (var a = 0; a < spans.length; a++) {
                        spans[a].style.display = "block";
                    }
                    
                    myPrintWindow.document.close();
                    myPrintWindow.focus();
                    myPrintWindow.print();
                    myPrintWindow.close();

                    scope.$apply(function () {
                        scope.$eval(attrs.printClick);
                    });

                    event.preventDefault();
                });
            };
        }
    ]);
})();