﻿(function () {
    angular
        .module('admin')
        .factory('appSession', [
            function () {
                var _session = {
                    user: null,
                    refresh: function () {
                        var self = this;
                        abp.services.app.session.getCurrentLogin({ async: false })
                            .done(function (result) {
                                self.user = result.user;
                                if (self.user) {
                                    self.user.fullName = (function () {
                                        return self.user.name + ' ' + self.user.surname;
                                    })();
                                }
                            })
                            .fail(function (ex) {
                                abp.message.error(ex.message, "Error de sesión").then(function () {
                                    var cookies = document.cookie.split(";");
                                    for (var i = 0; i < cookies.length; i++) {
                                        var cookie = cookies[i];
                                        var eqPos = cookie.indexOf("=");
                                        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
                                        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
                                    }
                                    location.href = '/logout';
                                });
                            });
                    }
                };

                _session.refresh();
                return _session;
            }
        ]);
})();