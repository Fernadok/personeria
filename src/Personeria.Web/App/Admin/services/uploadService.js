﻿(function () {
    angular
        .module('admin')
        .factory('$uploadService', [
        'Upload',
        function (Upload) {

            var _uploadFile = function (file, funcSuccess, funcProgress, funcError) {

                var url = '/upload';

                if (file.save) {
                    url += '?save=true';
                }

                if (file.uploadUnc) {
                    url += url.indexOf('?') !== -1 ? '&uploadUnc=true' : '?uploadUnc=true';
                }

                if (file.typePhoto) {
                    url += url.indexOf('?') !== -1 ? '&typePhoto=CONFIG' : '?typePhoto=CONFIG';
                }

                Upload.upload({
                    url: url,
                    file: file
                }).progress(function (evt) {
                    if (typeof funcProgress == "function") {
                        funcProgress(parseInt(100.0 * evt.loaded / evt.total));
                    }
                }).success(function (data, status, headers, config) {
                    if (typeof funcSuccess == "function") {
                        funcSuccess(data)
                    }
                }).error(function (data, status, headers, config) {
                    if (typeof funcError == "function") {
                        funcError("Error: " + status);
                    }
                });
            }
            return {
                uploadFile: _uploadFile
            };
        }
    ]);
})();