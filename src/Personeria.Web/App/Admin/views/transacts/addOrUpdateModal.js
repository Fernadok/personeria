﻿(function () {
    angular
        .module('admin')
        .controller('admin.views.transact.addOrUpdate', func);

    func.$inject = [
        '$window',
        '$uploadService',
        '$scope',
        '$timeout',
        '$uibModalInstance',
        'abp.services.app.transactService',
        'abp.services.app.personService',
        'abp.services.app.locationService',
        'abp.services.app.transactTypeService',
        'abp.services.app.transactTypeFileAttachTypeService',
        '$uibModal',
        'id'];

    function func(
        $window,
        $uploadService,
        $scope,
        $timeout,
        $uibModalInstance,
        $transactService,
        $personService,
        $locationService,
        $transactTypeService,
        $transactTypeFileAttachTypeService,
        $uibModal,
        id) {

        var vm = this;

        vm.permission = {
            hasEdit: abp.auth.hasPermission("Pages.transacts")
        };

        //-- VARIABLES --//
        vm.hasPersonData = false;
        vm.showMessage = false;
        vm.hasTransactTypeData = false;
        vm.transactionName = '';
        vm.locationName = '';
        vm.transactLocationName = '';
        vm.nextStep = false;
        vm.readOnlyPerson = true;
        vm.isNew = (id === null);
        //-- FIN VARIABLES --//

        //-- OBJECT --//
        vm.transact = {
            id: 0,
            search: '',
            person: {
                id: 0,
                fullName: '',
                name: '',
                surname: '',
                numberIdentity: '',
                adreess: '',
                phone: '',
                email: '',
                locationId: 0

            },
            transactLocationId: null,
            transactType: {}
        };
        vm.documentList = [];
        //-- FIN OBJECT --//

        vm.getDataToForm = function () {
            $transactService.getById(id).then(function (result) {
                vm.resultData = result.data;
                vm.transact.id = vm.resultData.id;
                vm.transact.person = vm.resultData.person;
                vm.transact.person.fullName = vm.resultData.fullName;

                vm.transact.transactType = vm.resultData.transactType;
                vm.transactionName = vm.resultData.transactTypeName;

                $.each(vm.resultData.fileAttachs, function (index, item) {
                    var file = {};
                    file.progress = 0;
                    file.error = '';
                    file.finish = true;
                    file.data = item || {};
                    file.fileInfo = item || null;

                    file.upload = function (fileIn) {
                        if (fileIn && !fileIn.$error && file.finish) {
                            file.finish = false;
                            vm.finishGeneral = file.finish;
                            $uploadService.uploadFile(fileIn, function (resdata) {
                                file.fileInfo = resdata;
                                file.fileInfo.title = file.data.isRequired + "@" + file.data.fileAttachTypeName;
                                file.finish = true;
                                vm.finishGeneral = file.finish;
                                vm.filesReqiered();
                            }, function (progress) {
                                file.progress = progress;
                            });
                        }
                    };

                    vm.documentList.push(file);
                });

                vm.hasPersonData = true;
                vm.hasTransactTypeData = true;

                vm.locationSelectedChange();
            });
        };

        //-- LOCATIONLIST --//
        vm.getLocations = function () {
            $locationService.locationList()
                .then(function (result) {
                    vm.locationList = result.data;
                    if (!vm.isNew) {
                        vm.getDataToForm();
                    }
                });
        };
        vm.getLocations();
        vm.locationSelectedChange = function () {
            $.each(vm.locationList, function (inx, it) {
                if (it.id === vm.transact.transactLocationId) {
                    vm.transactLocationName = it.description;
                }
            });
        };
        //-- FIN LOCATIONLIST --//

        //-- TRANSACTYPELIST --//
        vm.getTransactionType = function () {
            $transactTypeService.transactTypeList()
                .then(function (result) {
                    vm.transactTypeList = result.data;
                });
        };
        vm.getTransactionType();
        vm.transactTypeSelectedChange = function () {
            abp.ui.setBusy(".modal-content");
            $transactTypeFileAttachTypeService.getByTransactTypeId(vm.transactTypeId)
                .then(function (result) {
                    vm.FileAttachTypesList = result.data;
                    vm.hasTransactTypeData = true;
                    vm.documentList = [];

                    $.each(vm.transactTypeList, function (inx, it) {
                        if (it.id === vm.transactTypeId) {
                            vm.transactionName = it.description;
                        }
                    });

                    vm.FileAttachTypesList.forEach(function (item) {
                        vm.uploadFile(angular.copy(item), function (file) {
                            vm.documentList.push(file);
                        });   
                    });
                    abp.ui.clearBusy(".modal-content");
                });
        };
        //-- FIN TRANSACTYPELIST --//

        //--Enlazar la subida de archivos--//
        vm.finishGeneral = true;
        vm.uploadFile = function (data,func) {
            var file = {};
            file.progress = 0;
            file.error = '';
            file.finish = true;
            file.data = data || {};
            file.fileInfo = null;

            file.upload = function (fileIn) {
                if (fileIn && !fileIn.$error && file.finish) {
                    file.finish = false;
                    vm.finishGeneral = file.finish;
                    $uploadService.uploadFile(fileIn, function (resdata) {
                        file.fileInfo = resdata;                        
                        file.fileInfo.title = file.data.isRequired + "@" + file.data.fileAttachTypeName;
                        file.finish = true;
                        vm.finishGeneral = file.finish;
                        vm.filesReqiered();
                    }, function (progress) {
                        file.progress = progress;
                    });
                }
            };
            if (func) {
                func(file);
            }
        };
        vm.openFile = function (file) {
            var url = file.path;
            $window.open(url, '_blank');
        };
        vm.deleteFile = function (file) {
            var inx = vm.documentList.indexOf(file);
            vm.documentList[inx].fileInfo = null;
            vm.filesReqiered();
        }
        //--Fin Enlazar la subida de archivos--//

        //-- ACTION BUTTONS WIZARD --//
        vm.searchPerson = function (event) {
            if (event.charCode !== 13) return;
            abp.ui.setBusy(".modal-content");
            $personService.getByDocIdentity(vm.transact.search).then(function (result) {
                vm.transact.person = result.data;
                vm.hasPersonData = true;
                abp.ui.clearBusy(".modal-content");
                if (result.data === null) {
                    vm.showMessage = true;
                    vm.readOnlyPerson = false;
                    vm.transact.person = { numberIdentity: vm.transact.search };
                    return;
                }
                vm.transact.person.fullName = result.data.surname + ' ' + result.data.name;
                vm.locationSelectedChange();                
            });
        };
        vm.searchOther = function () {
            vm.hasPersonData = false;
            vm.transact.search = '';
            vm.showMessage = false;
            vm.readOnlyPerson = true;
        };
        vm.resetTransactType = function () {
            vm.hasTransactTypeData = false;
            vm.documentList = [];
            vm.transact.observation = '';
            vm.transactTypeId = null;
        };
        //-- FINACTION BUTTONS WIZARD --//

        //-- ACTION FORM --//
        vm.cancel = function () {
            $uibModalInstance.dismiss({});
        };
        vm.save = function () {
            //abp.ui.setBusy(".modal-content");
            abp.ui.setBusy(".modal-content");
            vm.transactInput = {
                id: vm.transact.id,
                personId: vm.transact.person.id,
                name: vm.transact.person.name,
                surname: vm.transact.person.surname,
                numberIdentity: vm.transact.person.numberIdentity,
                adreess: vm.transact.person.adreess,
                phone: vm.transact.person.phone,
                email: vm.transact.person.email,
                locationId: vm.transact.person.locationId,
                transactLocationId: vm.transact.transactLocationId,
                transactType: [],
                observation: vm.transact.observation
            };
            $.each(vm.documentList, function (ind, it) {
                vm.transactInput.transactType.push({
                    transactTypeId: it.data.transactTypeId || vm.transact.transactType.id,
                    fileAttachTypeId: it.data.fileAttachTypeId,
                    file: it.fileInfo
                });
            });
            $transactService.addOrUpdate(vm.transactInput).then(function () {
                abp.ui.clearBusy(".modal-content");
                $uibModalInstance.close(true);
            }, function (error) {
                //  abp.ui.clearBusy(".modal-content");
            });
        };
        //-- FIN ACTION FORM --//

        //-- WIZARD --//
        $timeout(function () {
            $(document).ready(function () {
                //Initialize tooltips
                $('.nav-tabs > li a[title]').tooltip();

                //Wizard
                $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

                    var $target = $(e.target);

                    if ($target.parent().hasClass('disabled')) {
                        return false;
                    }
                });

                $(".next-step").click(function (e) {

                    var $active = $('.wizard .nav-tabs li.active');
                    $active.next().removeClass('disabled');
                    nextTab($active);

                });
                $(".prev-step").click(function (e) {

                    var $active = $('.wizard .nav-tabs li.active');
                    prevTab($active);

                });
            });

            function nextTab(elem) {
                $(elem).next().find('a[data-toggle="tab"]').click();
            }
            function prevTab(elem) {
                $(elem).prev().find('a[data-toggle="tab"]').click();
            }
        }, 500);
        //-- FIN WIZARD --//

        vm.nextStepTransact = function () {
            return vm.transact.person
                && vm.transact.person.locationId && vm.transact.person.locationId !== 0
                && vm.transact.person.email && vm.transact.person.email !== ''
                && vm.transact.person.phone && vm.transact.person.phone !== ''
                && vm.transact.person.adreess && vm.transact.person.adreess !== ''
                && vm.transact.person.numberIdentity && vm.transact.person.numberIdentity !== ''
                && vm.transact.person.surname && vm.transact.person.surname !== ''
                && vm.transact.person.name && vm.transact.person.name !== '';
        };

        vm.filesReqiered = function () {
            var visible = true;
            $.each(vm.documentList, function (inx, it) {
                if (it.data.isRequired && it.fileInfo == null) {
                    visible = false;
                }
            });

            vm.nextStep = visible;
        };
    }
})();