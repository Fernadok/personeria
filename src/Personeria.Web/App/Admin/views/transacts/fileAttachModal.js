﻿(function () {
    angular
        .module('admin')
        .controller('admin.views.transact.fileAttach', func);

    func.$inject = [
        '$scope',
        '$uibModalInstance',
        'abp.services.app.transactService',
        '$uibModal',
        '$window',
        '$uploadService',
        'id'];

    function func($scope, $uibModalInstance, $transactService, $uibModal, $window, $uploadService, id) {
        var vm = this;

        vm.isNew = (id === null);
        vm.id = id;

        vm.cancel = function () {
            $uibModalInstance.dismiss({});
        };

        vm.save = function () {
            abp.ui.setBusy(".modal-content");
            dto = {
                transactId: vm.id,
                files: vm.files
            };
            $transactService.fileAttachAdmin(dto).then(function (res) {
                abp.ui.clearBusy(".modal-content");
                $uibModalInstance.close(true);
            }, function (error) {
                abp.ui.clearBusy(".modal-content");
            });
        };

        // -- Enlazar la subida de archivos         
        vm.files = [];
        vm.progress = 0;
        vm.error = '';
        vm.finish = true;

        $scope.$watch('vm.file', function (file) {
            if (file && !file.$error && vm.finish) {
                vm.finish = false;
                file.save = false;
                $uploadService.uploadFile(file, function (result) {
                    var obj = result;
                    obj.store = "USER"
                    vm.files.push(obj);
                    vm.finish = true;
                }, function (progress) {
                    vm.progress = progress;
                });
            }
        });

        vm.openFile = function (file) {
            var url = file.path;
            $window.open(url, '_blank');
        };

        vm.deleteFile = function (file) {
            vm.files.remove(file);
        }
        // -- Enlazar la subida de archivos

        vm.getFilesAttach = function () {
            abp.ui.setBusy(".modal-content");
            $transactService.getFilesAttach(vm.id,"USER").then(function (res) {
                abp.ui.clearBusy(".modal-content");
                vm.files = res.data.files;
            }, function (error) {
                abp.ui.clearBusy(".modal-content");
            });
        };
        vm.getFilesAttach();
    }
})();