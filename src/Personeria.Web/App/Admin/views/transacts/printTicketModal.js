﻿
(function () {
    angular
        .module('admin')
        .controller('admin.views.transact.printTicket', [
            'appSession',
            'print',
            '$timeout',
            '$uibModalInstance',
            function (
                $appSession,
                print,
                $timeout,
                $uibModalInstance) {
                var vm = this;

                // -- Set vars
                vm.user = $appSession.user;

                // -- Print
                vm.printTicket = function () {
                    $timeout(function () {
                        var address = '//' + vm.user.printTicketDefaultIp + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=60000';

                        var builder = new epson.ePOSBuilder();
                        builder.addTextAlign(builder.ALIGN_CENTER);
                        builder.addFeedLine(1);
                        builder.addText('MINISTERIO DE JUSTICIA Y CULTO\n');
                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                        builder.addTextSize(2, 2);
                        builder.addText('TRAMITE REG. #' + vm.print.reg + '\n');
                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                        builder.addTextSize(1, 1);
                        builder.addText(vm.print.transactionName + '\n');
                        builder.addFeedLine(1);
                        builder.addSymbol(vm.print.url, builder.SYMBOL_PDF417_STANDARD, builder.LEVEL_DEFAULT, 3, 0, 0);
                        builder.addFeedLine(1);
                        builder.addText('GOBIERNO DE SAN LUIS\n');

                        var epos = new epson.ePOSPrint(address);
                        epos.onreceive = function (res) {
                            abp.notify.success("Se imprimio correctamente");
                            $uibModalInstance.close();
                        };
                        epos.onerror = function (err) {
                            abp.notify.error("No se pudo imprimir correctamente, revise la impresora si esta encendida y conectada en la red");
                            $uibModalInstance.close();
                        };
                        epos.oncoveropen = function () {
                            abp.notify.error("No se pudo imprimir correctamente, revise la impresora si esta encendida y conectada en la red");
                            $uibModalInstance.close();
                        };
                        epos.send(builder.toString());

                    }, 1000);
                };


                //-- Cancel
                vm.cancel = function () {
                    $uibModalInstance.dismiss({});
                };

                if (vm.user.printTicketDefaultId !== null) {
                    vm.status = 'Imprimiendo...';
                    vm.print = print;
                    vm.print.url = document.baseURI.replace('admin/', '') + 'tramites/' + vm.print.reg;

                    vm.printTicket();
                } else {                    
                    abp.notify.error('No puedes imprimir el ticket porque no has seleccionado una impresora como default');
                    $uibModalInstance.close();
                }
            }]);
})();