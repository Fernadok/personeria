﻿(function () {
    angular
        .module('admin')
        .controller('admin.views.transact.changeState', func);

    func.$inject = [
        '$scope',
        '$uibModalInstance',
        'abp.services.app.processFlowService',
        '$uibModal',
        'id',
        'statearr',
        'description'];

    function func($scope, $uibModalInstance, $processFlowService, $uibModal, id, statearr, description) {
        var vm = this;

        vm.isNew = (id === null);
        vm.id = id;
        vm.selectedState = null;
        vm.flow = '';
        vm.stateFrom = parseInt(statearr.split('|')[0]);
        vm.stateTo = parseInt(statearr.split('|')[1]);
        vm.open = vm.stateTo === -1;

        switch (vm.stateFrom) {
            case 0: vm.flow = "EN CURSO >> " + description; break;
            case 1: vm.flow = "PAUSADO >> " + description; break;
            case 2: vm.flow = "BLOQUEADO >> " + description; break;
            case 3: vm.flow = "FINALIZADO >> " + description; break;
            default:
        }
        vm.cancel = function () {
            $uibModalInstance.dismiss({});
        };

        // -- Save
        vm.save = function () {
            abp.ui.setBusy(".modal-content");
            var dto = {
                transactId: vm.id,
                state: vm.stateTo,
                observation: vm.observation,
                notify: vm.notify,
                revisionRequest: vm.revisionRequest
            };
            if (vm.stateTo === -2 || vm.stateTo === -3) {
                dto.state = vm.stateTo;
                $processFlowService.backArea(dto).then(function (res) {
                    abp.ui.clearBusy(".modal-content");
                    $uibModalInstance.close(true);
                }, function (error) {
                    abp.ui.clearBusy(".modal-content");
                });
                return;
            }
            if (!vm.open) {
                $processFlowService.changeState(dto).then(function (res) {
                    abp.ui.clearBusy(".modal-content");
                    $uibModalInstance.close(true);
                }, function (error) {
                    abp.ui.clearBusy(".modal-content");
                });
            }
            else {
                dto.state = 0;
                $processFlowService.openFlow(dto).then(function (res) {
                    abp.ui.clearBusy(".modal-content");
                    $uibModalInstance.close(true);
                }, function (error) {
                    abp.ui.clearBusy(".modal-content");
                });
            }
        };
    }
})();