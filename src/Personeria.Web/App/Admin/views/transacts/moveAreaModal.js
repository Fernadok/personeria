﻿(function () {
    angular
        .module('admin')
        .controller('admin.views.transact.moveAreaModal', func);

    func.$inject = [
        '$scope',
        'appSession',
        '$uibModalInstance',
        'abp.services.app.processFlowService',
        'abp.services.app.areaService',
        '$uibModal',
        'id'];

    function func($scope, $appSession, $uibModalInstance, $processFlowService, $areaService, $uibModal, id) {
        var vm = this;

        vm.isNew = (id === null);
        vm.id = id;
        vm.areas = $appSession.user.areas;

        $areaService.moveAreaList(vm.areas).then(function (result) {
            vm.areas = result.data;
        });

        vm.cancel = function () {
            $uibModalInstance.dismiss({});
        };

        vm.save = function () {
            abp.ui.setBusy(".modal-content");
            var dto = {
                transactId: vm.id,
                state: vm.stateTo,
                observation: vm.observation,
                notify: vm.notify,
                sendAreaId: vm.areaId
            };
            $processFlowService.sendOtherArea(dto).then(function (res) {
                abp.ui.clearBusy(".modal-content");
                $uibModalInstance.close(true);
            }, function (error) {
                abp.ui.clearBusy(".modal-content");
            });
        }
    }
})();