﻿(function () {
    var controllerId = 'admin.views.transact.index';
    angular
        .module('admin')
        .controller(controllerId, func);

    func.$inject = [
        '$scope',
        '$stateParams',
        'appSession',
        'config',
        '$uibModal',
        'abp.services.app.transactService',
        'abp.services.app.transactTypeService',
        'abp.services.app.processFlowService'
    ];

    function func($scope, $stateParams, $appSession, $config, $uibModal,
        $transactService,
        $transactTypeService,
        $processFlowService) {
        var vm = this;

        // -- Filters -- //
        vm.filters = {
            search: '',
            page: 1,
            pageSize: 10,
            orderColumn: 'Id',
            order: 'desc',
            dateStart: null,
            dateEnd: null,
            state: -1,
            typeTransact: null,
            itemsTotal: 0
        };
        vm.isAdmin = $appSession.user.isAdmin;

        vm.pageSizes = [10, 25, 50, 100];
        vm.options = {
            format: 'DD-MM-YYYY',
            lang: 'es',
            clearButton: true,
            clearText: 'Limpiar',
            cancelText: 'Cancelar',
            okText: 'Aceptar',
            nowButton: false,
            nowText: 'Hoy',
            switchOnClick: true,
            weekStart: 0,
            time: false
        };
        vm.orderAscDesc = [
            { Id: "asc", Text: "ASCENDENTE" },
            { Id: "desc", Text: "DESCENTENTE" }
        ];
        vm.states = [
            { Id: -1, Text: "TODOS" },
            { Id: 0, Text: "EN PROCESO" },
            { Id: 1, Text: "PAUSADO" },
            { Id: 2, Text: "BLOQUEADO" },
            { Id: 3, Text: "FINALIZADO" }
        ];

        vm.getTransactionType = function () {
            vm.transactTypeList = [];
            $transactTypeService.transactTypeList()
                .then(function (result) {
                    vm.transactTypeList = result.data;
                    vm.transactTypeList.push({
                        id: null,
                        description: 'TODOS'
                    });
                });
        };
        vm.getTransactionType();

        vm.showFilter = false;
        vm.filterAvan = function () {
            vm.showFilter = !vm.showFilter;
        };

        // --Fin Filters -- //

        vm.load = false;
        vm.getTransacts = function () {
            if (!vm.load) {
                vm.load = true;
                abp.ui.setBusy(".body",
                    $transactService.getAll(vm.filters).then(function (result) {
                        vm.dataContext = result.data;
                        vm.filters.itemsTotal = vm.dataContext.metaData.totalItemCount;

                        vm.dataContext.data.forEach(function (tran) {
                            $transactService.getStateList(tran.id).then(function (resultList) {
                                tran.statesList = resultList.data;
                            });
                            if (tran.lockedAreaId === null) {
                                $transactService.getProgress(tran.transactTypeId, tran.areaId, tran.state === 3).then(function (res) {
                                    tran.percent = res.data;
                                });
                            } else {
                                $transactService.getProgress(tran.transactTypeId, tran.lockedAreaId, tran.state === 3).then(function (res) {
                                    tran.percent = res.data;
                                });
                            }
                        });
                        vm.load = false;
                    })
                );
            }
        };

        // -- Modals
        vm.openModal = function (obj) {
            var modalInstance = $uibModal.open({
                templateUrl: '/App/Admin/views/transacts/addOrUpdateModal.cshtml',
                controller: 'admin.views.transact.addOrUpdate as vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                    id: function () {
                        return obj ? obj.id : null;
                    }
                }
            });

            modalInstance.rendered.then(function () {
                $.AdminBSB.input.activate();
            });

            modalInstance.result.then(function () {
                vm.getTransacts();
            });
        };

        vm.changeStateModal = function (id, stateFrom, stateTo, description) {
            var modalInstance = $uibModal.open({
                templateUrl: '/App/Admin/views/transacts/changeState.cshtml',
                controller: 'admin.views.transact.changeState as vm',
                backdrop: 'static',
                resolve: {
                    id: function () {
                        return id ? id : null;
                    },
                    statearr: function () {
                        return stateFrom + '|' + stateTo;
                    },
                    description: function () {
                        return description ? description : null;
                    }
                }
            });
            modalInstance.rendered.then(function () {
                $.AdminBSB.input.activate();
            });
            modalInstance.result.then(function () {
                vm.getTransacts();
            });
        };
        vm.moveAreaModal = function (id) {
            var modalInstance = $uibModal.open({
                templateUrl: '/App/Admin/views/transacts/moveAreaModal.cshtml',
                controller: 'admin.views.transact.moveAreaModal as vm',
                backdrop: 'static',
                resolve: {
                    id: function () {
                        return id ? id : null;
                    }
                }
            });
            modalInstance.rendered.then(function () {
                $.AdminBSB.input.activate();
            });
            modalInstance.result.then(function () {
                vm.getTransacts();
            });
        };
        vm.attachModal = function (id) {
            var modalInstance = $uibModal.open({
                templateUrl: '/App/Admin/views/transacts/fileAttachModal.cshtml',
                controller: 'admin.views.transact.fileAttach as vm',
                backdrop: 'static',
                resolve: {
                    id: function () {
                        return id ? id : null;
                    }
                }
            });
            modalInstance.rendered.then(function () {
                $.AdminBSB.input.activate();
            });
        };
        vm.delete = function (id) {
            if (vm.isAdmin) {
                abp.message.confirm(
                    "¿Seguro que quiere borrar ésta trámite?",
                    "Advertencia",
                    function (result) {
                        if (result) {
                            $transactService.delete(id)
                                .then(function () {
                                    abp.notify.info("trámite borrado");
                                    vm.getTransacts();
                                });
                        }
                    });
            };
        };

        //if ($stateParams.paramId) {
        //    var idTransaction = parseInt($stateParams.paramId);
        //    vm.openViewModal(idTransaction);
        //}else
        if ($stateParams.tipotramite) {
            vm.filters.typeTransact = parseInt($stateParams.tipotramite);
            vm.showFilter = true;
        }
        else if ($stateParams.estado) {
            vm.filters.state = parseInt($stateParams.estado);
            vm.showFilter = true;
        };

        $scope.$watch("vm.filters.search", function () {
            vm.filters.page = 1;
            vm.getTransacts();
        });
        $scope.$watch("vm.filters.typeTransact", function () {
            vm.filters.page = 1;
            vm.getTransacts();
        });
        $scope.$watch("vm.filters.pageSize", function () {
            vm.filters.page = 1;
            vm.getTransacts();
        });
        $scope.$watch("vm.filters.order", function () {
            vm.filters.page = 1;
            vm.getTransacts();
        });
        $scope.$watch("vm.filters.state", function () {
            vm.filters.page = 1;
            vm.getTransacts();
        });
        $scope.$watch("vm.filters.dateStart", function () {
            vm.filters.page = 1;
            vm.getTransacts();
        });
        $scope.$watch("vm.filters.dateEnd", function () {
            vm.filters.page = 1;
            vm.getTransacts();
        });

        vm.pageChanged = function () {
            vm.getTransacts();
        };

        vm.getTransacts();

        $('#dtStart')
            .bootstrapMaterialDatePicker(vm.options)
            .on('change', function (e, date) {
                vm.filters.dateStart = date ? new Date(date._d) : null;
                vm.filters.page = 1;
                vm.getTransacts();
            });

        $('#dtEnd')
            .bootstrapMaterialDatePicker(vm.options)
            .on('change', function (e, date) {
                vm.filters.dateEnd = date ? new Date(date._d) : null;
                vm.filters.page = 1;
                vm.getTransacts();
            });

        $.AdminBSB.input.activate();

    }

})();