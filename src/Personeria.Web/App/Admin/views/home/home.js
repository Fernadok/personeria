﻿(function () {
    var controllerId = 'admin.views.home';
    angular.module('admin').controller(controllerId, [
        '$scope',
        '$location',
        '$uibModal',
        '$timeout',
        '$location',
        'abp.services.app.transactService',
        function (
            $scope,
            $location,
            $uibModal,
            $timeout,
            $location,
            $transactService) {
            var vm = this;

            vm.filters = {
                search: '',
                page: 1,
                pageSize: 10,
                orderColumn: 'Id',
                order: 'desc',
                dateStart: null,
                dateEnd: null,
                state: -1,
                typeTransact: null,
                itemsTotal: 0
            };

            var init = function () {

                function initSummaries() {
                    //Widgets count
                    $('.count-to').countTo();

                    //Sales count to
                    $('.sales-count-to').countTo({
                        formatter: function (value, options) {
                            return '$' + value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, ' ').replace('.', ',');
                        }
                    });
                }

                var realtime = 'on';
                function initRealTimeChart() {
                    //Real time ==========================================================================================
                    var plot = $.plot('#real_time_chart', [getRandomData()], {
                        series: {
                            shadowSize: 0,
                            color: 'rgb(0, 188, 212)'
                        },
                        grid: {
                            borderColor: '#f3f3f3',
                            borderWidth: 1,
                            tickColor: '#f3f3f3'
                        },
                        lines: {
                            fill: true
                        },
                        yaxis: {
                            min: 0,
                            max: 100
                        },
                        xaxis: {
                            min: 0,
                            max: 100
                        }
                    });

                    function updateRealTime() {
                        plot.setData([getRandomData()]);
                        plot.draw();

                        var timeout;
                        if (realtime === 'on') {
                            timeout = setTimeout(updateRealTime, 320);
                        } else {
                            clearTimeout(timeout);
                        }
                    }

                    updateRealTime();

                    $('#realtime').on('change', function () {
                        realtime = this.checked ? 'on' : 'off';
                        updateRealTime();
                    });
                    //====================================================================================================
                }

                function initSparkline() {
                    $(".sparkline").each(function () {
                        var $this = $(this);
                        $this.sparkline('html', $this.data());
                    });
                }

                function initDonutChart() {
                    window.Morris.Donut({
                        element: 'donut_chart',
                        data: [{
                            label: 'Chrome',
                            value: 37
                        }, {
                            label: 'Firefox',
                            value: 30
                        }, {
                            label: 'Safari',
                            value: 18
                        }, {
                            label: 'Opera',
                            value: 12
                        }, {
                            label: 'Other',
                            value: 3
                        }],
                        colors: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)', 'rgb(255, 152, 0)', 'rgb(0, 150, 136)', 'rgb(96, 125, 139)'],
                        formatter: function (y) {
                            return y + '%';
                        }
                    });
                }

                var data = [], totalPoints = 110;
                function getRandomData() {
                    if (data.length > 0) data = data.slice(1);

                    while (data.length < totalPoints) {
                        var prev = data.length > 0 ? data[data.length - 1] : 50, y = prev + Math.random() * 10 - 5;
                        if (y < 0) { y = 0; } else if (y > 100) { y = 100; }

                        data.push(y);
                    }

                    var res = [];
                    for (var i = 0; i < data.length; ++i) {
                        res.push([i, data[i]]);
                    }

                    return res;
                }

                initSummaries();
                //initRealTimeChart();
                //initDonutChart();
                //initSparkline();
            };

            vm.totals = {};
            vm.init = function () {
                $transactService.getToTals().then(function (result) {
                    vm.totals = result.data;
                    $transactService.getGeneralTotals().then(function (resultGral) {
                        vm.gralTotals = resultGral.data;
                        $timeout(function () {
                            init();
                        }, 0);
                    });
                });
            };
            vm.init();

            vm.load = false;
            vm.getTransacts = function () {
                if (!vm.load) {
                    vm.load = true;
                    abp.ui.setBusy(".table-responsive",
                        $transactService.getAll(vm.filters).then(function (result) {
                            vm.dataContext = result.data;
                            vm.dataContext.data.forEach(function (tran) {
                                if (tran.lockedAreaId === null) {
                                    $transactService.getProgress(tran.transactTypeId, tran.areaId, tran.state === 3).then(function (res) {
                                        tran.percent = res.data;
                                    });
                                } else {
                                    $transactService.getProgress(tran.transactTypeId, tran.lockedAreaId, tran.state === 3).then(function (res) {
                                        tran.percent = res.data;
                                    });
                                }
                            });
                            vm.load = false;
                        })
                    );
                }
            };
            vm.getTransacts();

            vm.openViewModal = function (obj) {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Admin/views/transacts/detailModal.cshtml',
                    controller: 'admin.views.transact.detail as vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        id: function () {
                            return obj ? obj.id : null;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $.AdminBSB.input.activate();
                });
                modalInstance.result.then(function () {
                    vm.init();
                    vm.getTransacts();
                });
            };

            vm.openModalTransact = function (obj) {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Admin/views/transacts/addOrUpdateModal.cshtml',
                    controller: 'admin.views.transact.addOrUpdate as vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        id: function () {
                            return obj ? obj.id : null;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $.AdminBSB.input.activate();
                });
                modalInstance.result.then(function () {
                    vm.init();
                    vm.getTransacts();
                });
            };

            vm.goToTransact = function (parState) {
                var state = -1;
                switch (parState) {
                    case 'PROGRESO': state = 0; break;
                    case 'PAUSADOS': state = 1; break;
                    case 'BLOQUEADOS': state = 2; break;
                    case 'COMPLETADOS': state = 3; break;
                    default: state = -1;
                }

                $location.url('/tramites?estado=' + state);
            }

            vm.goTransact = function () {
                $location.url('/tramites');
            };
        }
    ]);
})();