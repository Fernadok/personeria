﻿(function () {
    angular
        .module('admin')
        .controller('admin.views.person.addOrUpdate', func);

    func.$inject = [
        '$scope',
        '$uibModalInstance',
        'abp.services.app.personService',
        'abp.services.app.locationService',
        '$uibModal',
        'id'];

    function func($scope, $uibModalInstance, $personService, $locationService, $uibModal, id) {
        var vm = this;

        vm.permission = {
            hasEdit: abp.auth.hasPermission("Pages.Peoples")
        }

        vm.isNew = (id === null);
        vm.person = {
            locationds: 0,
        };

        vm.cancel = function () {
            $uibModalInstance.dismiss({});
        };

        vm.save = function () {
            abp.ui.setBusy(".modal-content");
            $personService.addOrUpdate(vm.person).then(function () {
                abp.ui.clearBusy(".modal-content");
                $uibModalInstance.close(true);
            }, function (error) {
                abp.ui.clearBusy(".modal-content");
            });
        }

        vm.getLocations= function () {
            $locationService.locationList()
                .then(function (result) {
                    vm.locationList = result.data;
                    if (!vm.isNew) {
                        $personService.getById(id).then(function (result) {
                            vm.person = result.data;
                        });
                    }
                });
        }

        vm.getLocations();
    }
})();