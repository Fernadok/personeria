﻿(function () {
    var controllerId = 'admin.views.person.index';
    angular
        .module('admin')
        .controller(controllerId, func);

    func.$inject = ['$scope', '$timeout', '$uibModal', 'abp.services.app.personService'];

    function func($scope, $timeout, $uibModal, $personService) {
        var vm = this;

        vm.filters = {
            search: '',
            page: 1,
            pageSize: 10,
            orderColumn: 'Surname',
            order: 'asc',
            itemsTotal: 0
        };

        vm.load = false;
        vm.getPersons = function () {
            if (!vm.load) {
                vm.load = true;

                abp.ui.setBusy(
                    ".table-responsive",
                    $personService.getAll(vm.filters).then(function (result) {
                        vm.dataContext = result.data;

                        vm.filters.itemsTotal = vm.dataContext.metaData.totalItemCount;

                        //abp.ui.clearBusy(".table-responsive");
                        vm.load = false;
                    })
                );

            }
        };

        // -- Edit or Create
        vm.openModal = function (obj) {
            var modalInstance = $uibModal.open({
                templateUrl: '/App/Admin/views/persons/addOrUpdateModal.cshtml',
                controller: 'admin.views.person.addOrUpdate as vm',
                backdrop: 'static',
                resolve: {
                    id: function () {
                        return obj ? obj.id : null;
                    }
                }
            });

            modalInstance.rendered.then(function () {
                $.AdminBSB.input.activate();
            });

            modalInstance.result.then(function () {
                vm.getPersons();
            });
        };

        // -- Delete
        vm.delete = function (id) {
            abp.message.confirm(
                "¿Seguro que quiere borrar ésta persona?",
                "Advertencia",
                function (result) {
                    if (result) {
                        $personService.delete(id)
                            .then(function () {
                                abp.notify.info("persona borrado");
                                vm.getPersons();
                            });
                    }
                });
        };

        vm.search = function () {
            vm.filters.page = 1;
            vm.getPersons();
        };

        vm.pageChanged = function () {
            vm.getPersons();
        };

        $timeout(function () {
            vm.getPersons();
        }, 0);
        
    }

})();