﻿(function () {
    angular
        .module('admin')
        .controller('admin.views.users.index', func);

    func.$inject = ['$scope', '$timeout', '$uibModal', 'abp.services.app.user'];

    function func($scope, $timeout, $uibModal, userService) {
        var vm = this;

        vm.filters = {
            search: '',
            page: 1,
            pageSize: 10,
            orderColumn: 'UserId',
            order: 'asc',
            itemsTotal: 0
        };

        vm.users = [];
        vm.load = false;
        function getUsers() {
            if (!vm.load) {
                vm.load = true;
                abp.ui.setBusy(".table-responsive",
                    userService.getAllUsers(vm.filters).then(function (result) {
                        vm.users = result.data;
                        vm.filters.itemsTotal = vm.users.metaData.totalItemCount;
                        vm.load = false;
                    })
                );
            }
        };

        vm.openUserCreationModal = function () {
            var modalInstance = $uibModal.open({
                templateUrl: '/App/admin/views/users/createModal.cshtml',
                controller: 'admin.views.users.createModal as vm',
                backdrop: 'static'
            });

            modalInstance.rendered.then(function () {
                $timeout(function () {
                    $.AdminBSB.input.activate();
                }, 500);
            });

            modalInstance.result.then(function () {
                getUsers();
            });
        };

        vm.openUserEditModal = function (user) {
            var modalInstance = $uibModal.open({
                templateUrl: '/App/admin/views/users/editModal.cshtml',
                controller: 'admin.views.users.editModal as vm',
                backdrop: 'static',
                resolve: {
                    id: function () {
                        return user.id;
                    }
                }
            });

            modalInstance.rendered.then(function () {
                $timeout(function () {
                    $.AdminBSB.input.activate();
                }, 1000);
            });

            modalInstance.result.then(function () {
                getUsers();
            });
        };

        vm.delete = function (user) {
            abp.message.confirm(
                "Borrar usuario '" + user.userName + "'?",
                function (result) {
                    if (result) {
                        userService.delete({ id: user.id })
                            .then(function () {
                                abp.notify.info("Deleted user: " + user.userName);
                                getUsers();
                            });
                    }
                });
        };

        vm.refresh = function () {
            getUsers();
        };

        $scope.$watch("vm.filters.search", function () {
            vm.filters.page = 1;
            getUsers();
        });

        getUsers();
    }
})();