﻿(function () {
    angular
        .module('admin')
        .controller('admin.views.users.createModal', func);

    func.$inject = ['$scope', '$uibModalInstance', 'abp.services.app.user', 'abp.services.app.areaService'];

    function func($scope, $uibModalInstance, userService, $areaService) {
        var vm = this;

        vm.user = {
            areaIds: [],
            isActive: true
        };

        vm.roles = [];

        function getRoles() {
            userService.getRoles()
                .then(function (result) {
                    vm.roles = result.data.items;
                });
        }

        function getAreas() {
            $areaService.areaList()
                .then(function (result) {
                    vm.areaList = result.data;
                });
        }

        vm.save = function () {
            var assingnedRoles = [];

            for (var i = 0; i < vm.roles.length; i++) {
                var role = vm.roles[i];
                if (!role.isAssigned) {
                    continue;
                }

                assingnedRoles.push(role.name);
            }

            vm.user.roleNames = assingnedRoles;
            abp.ui.setBusy('.modal-content',
                userService.create(vm.user)
                    .then(function () {
                        abp.notify.info(App.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
                    })
            );
        };

        vm.cancel = function () {
            $uibModalInstance.dismiss({});
        };

        getRoles();
        getAreas();
    }
})();