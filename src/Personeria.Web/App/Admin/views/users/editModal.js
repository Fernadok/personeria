﻿(function () {
    angular
        .module('admin')
        .controller('admin.views.users.editModal', func);

    func.$inject = ['$scope', '$uibModalInstance', 'abp.services.app.user', 'abp.services.app.areaService', 'id'];

    function func($scope, $uibModalInstance, userService, $areaService, id) {
        var vm = this;

        vm.user = {
            areaIds: [],
            isActive: true
        };

        vm.roles = [];

        var setAssignedRoles = function (user, roles) {
            for (var i = 0; i < roles.length; i++) {
                var role = roles[i];
                role.isAssigned = $.inArray(role.name, user.roles) >= 0;
            }
        }


        var init = function () {
            userService.getRoles()
                .then(function (result) {
                    vm.roles = result.data.items;

                    userService.get({ id: id })
                        .then(function (result) {
                            vm.user = result.data;
                            setAssignedRoles(vm.user, vm.roles);
                        });
                });
        }

        function getAreas() {
            $areaService.areaList()
                .then(function (result) {
                    vm.areaList = result.data;
                    init();
                });
        }

        vm.save = function () {
            var assingnedRoles = [];

            for (var i = 0; i < vm.roles.length; i++) {
                var role = vm.roles[i];
                if (!role.isAssigned) {
                    continue;
                }

                assingnedRoles.push(role.name);
            }

            vm.user.roleNames = assingnedRoles;

            abp.ui.setBusy('.modal-content',
                userService.updateUser(vm.user)
                    .then(function () {
                        abp.notify.info(App.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
                    })
            );
        };

        vm.cancel = function () {
            $uibModalInstance.dismiss({});
        };

        getAreas();
    }
})();