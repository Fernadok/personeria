﻿(function () {
    var controllerId = 'admin.views.layout.rightSidebar';
    angular.module('admin').controller(controllerId, [
        '$rootScope',
        '$scope',
        '$timeout',
        '$state',
        'appSession',
        '$location',
        'abp.services.app.notification',
        'abp.services.app.configuration',
        function (
            $rootScope,
            $scope,
            $timeout,
            $state,
            $appSession,
            $location,
            $notiService,
            $configService) {
            var vm = this;

            vm.selectedThemeCssClass = "red";
            vm.user = $appSession.user;
            console.log(vm.user);

            // -- Is Suscribe New Transact
            vm.isNotificationNewTransact = false;
            vm.isSuscribeNewTransact = function () {
                $configService.isSuscribeNewTransact().then(function (res) {
                    vm.isNotificationNewTransact = res.data;
                });
            };

            // -- Save Suscribe New Transact
            vm.saveNotificationNewTransact = function () {
                $configService.saveNotificationNewTransact(vm.isNotificationNewTransact).then(function () {
                    vm.isSuscribeNewTransact();
                });
            };
            vm.isSuscribeNewTransact();

            // -- Is Suscribe Change Transact
            vm.isNotificationChangeTransact = false;
            vm.isSuscribeChangeTransact = function () {
                $configService.isSuscribeChangeTransact().then(function (res) {
                    vm.isNotificationChangeTransact = res.data;
                });
            };

            // -- Save Suscribe Change Transact
            vm.saveNotificationChangeTransact = function () {
                $configService.saveNotificationChangeTransact(vm.isNotificationChangeTransact).then(function () {
                    vm.isSuscribeChangeTransact();
                });
            };
            vm.isSuscribeChangeTransact();

            vm.notifications = [];
            vm.loadNotification = false;
            vm.getNotifications = function (func) {
                if (!vm.loadNotification) {
                    abp.ui.setBusy("#rightsidebar");
                    vm.loadNotification = true;
                    vm.notifications = [];
                    $notiService.getAllNotifications({ state: vm.selectState }).then(function (res) {

                        vm.notifications = res.data.map(function (not) {
                            var noti = not.content;
                            noti.id = not.id;
                            noti.stateName = not.stateName;
                            noti.state = not.state;
                            return noti;
                        });
                        vm.loadNotification = false;
                        abp.ui.clearBusy("#rightsidebar");

                        if (func) {
                            func();
                        }
                    });
                }
            };

            $rootScope.$on('onGetNotifications', function (event, opt) {
                vm.getNotifications();
            });


            vm.markAllAsRead = function () {
                if (vm.notifications.length > 0) {
                    abp.ui.setBusy("#rightsidebar");
                    $notiService.markAllAsRead().then(function (notifications) {
                        vm.getNotifications(function () {
                            $rootScope.$broadcast('onGetCountNotification');
                            abp.ui.clearBusy("#rightsidebar");
                        });
                    });
                }
            };

            vm.deleteAll = function () {
                if (vm.notifications.length > 0) {
                    abp.ui.setBusy("#rightsidebar");
                    $notiService.deleteAll().then(function (notifications) {
                        vm.getNotifications(function () {
                            $rootScope.$broadcast('onGetCountNotification');
                            abp.ui.clearBusy("#rightsidebar");
                        });
                    });
                }
            };

            vm.open = function (not) {
                abp.ui.setBusy("#rightsidebar");
                $("body").click();
                if (not.state === 0) {
                    $notiService.markAsRead(not.id).then(function () {
                        vm.getNotifications();
                        $rootScope.$broadcast('onGetCountNotification');
                        abp.ui.clearBusy("#rightsidebar");
                        $state.go(not.data.url, {paramId:not.data.transactId}, { reload: true });
                    });
                } else {                    
                    abp.ui.clearBusy("#rightsidebar");
                    $state.go(not.data.url, { paramId: not.data.transactId }, { reload: true });
                }
            };


            $timeout(function () {
                var $el = $('.tab-content');
                var height = $el.height();
                $el.slimscroll({
                    height: height + 'px',
                    color: 'rgba(0,0,0,0.5)',
                    size: '4px',
                    alwaysVisible: false,
                    borderRadius: '0',
                    railBorderRadius: '0'
                });
            }, 0);

            // -- Get Prints
            vm.prints = [];            
            vm.getPrints = function () {
                abp.ui.setBusy(".prints");
                $configService.getPrints().then(function (result) {
                    vm.prints = [{
                        id: null,
                        name: "Seleccionar impresora"
                    }];
                    result.data.forEach(function (print) {
                        vm.prints.push(print);
                    });
                    abp.ui.clearBusy(".prints");
                });
            };
            vm.getPrints();

            vm.changePrint = function (print) {
                var pr = vm.prints.find(function (pr) {
                    return pr.id === vm.user.printTicketDefaultId;
                });
                $configService.asignPrint(pr).then(function () {
                    $appSession.refresh();
                });
            };
        }
    ]);
})();