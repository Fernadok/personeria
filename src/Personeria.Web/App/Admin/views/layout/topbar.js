﻿(function () {
    var controllerId = 'admin.views.layout.topbar';
    angular.module('admin').controller(controllerId, [
        '$state',
        'appSession',
        '$rootScope',
        'abp.services.app.notification',
        '$notification',
        function (
            $state,
            appSession,
            $rootScope,
            $notiService,
            $notificationDesktop) {

            var vm = this;
            vm.isLogin = appSession.user !== null;


            // -- Open Notification Desktop
            vm.permi = "";
            $notificationDesktop.requestPermission()
                .then(function (permission) {
                    vm.permi = permission;
                });
            vm.permi = $notificationDesktop.getPermission();


            // -- Function Get Count Notification
            vm.notificationCount = 0;
            vm.getCountNotification = function () {
                $notiService.getCountNotifications().then(function (result) {
                    vm.notificationCount = result.data;
                });
            };
            vm.getCountNotification();

            // -- Function Get Count Notification
            vm.openNotification = function () {
                $rootScope.$broadcast("onGetNotifications");
            };

            // -- On Get Count Notification 
            $rootScope.$on("onGetCountNotification", function () {
                vm.getCountNotification();
            });

            // -- On Recived Notification
            abp.event.on("abp.notifications.received", function (userNotification) {
                console.log(userNotification);

                if (userNotification) {
                    var text = userNotification.notification.data.message;
                    var url = userNotification.notification.data.url;
                    var title = userNotification.notification.data.title;
                    var transactId = userNotification.notification.data.transactId;

                    abp.notify.info(text, title, {
                        onclick: function () {
                            if (url !== "") {
                                $state.go(url, { paramId: transactId }, { reload: true });
                            }
                        }
                    });

                    //Desktop notification
                    if (vm.permi) {
                        Push.create(title, {
                            body: text,
                            icon: abp.appPath + 'images/icons/favicon_72x72.png?width=60',
                            timeout: 6000,
                            onClick: function () {
                                window.focus();
                                this.close();
                            }
                        });
                    }
                }

                vm.notificationCount = vm.notificationCount + 1;
                $rootScope.$apply();
            });      
        }
    ]);
})();