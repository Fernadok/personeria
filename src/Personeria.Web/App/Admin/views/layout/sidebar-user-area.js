﻿(function () {
    var controllerId = 'admin.views.layout.sidebarUserArea';
    angular.module('admin').controller(controllerId, [
        '$rootScope',
        '$state',
        'appSession',
        function ($rootScope, $state, appSession) {
            var vm = this;
            
            vm.userEmailAddress = appSession.user.emailAddress;
            vm.getShownUserName = function () {
                if (!abp.multiTenancy.isEnabled) {
                    return appSession.user.userName;
                } else {
                    if (appSession.tenant) {
                        return appSession.tenant.tenancyName + '\\' + appSession.user.userName;
                    } else {
                        return '.\\' + appSession.user.userName;
                    }
                }
            };
        }
    ]);
})();