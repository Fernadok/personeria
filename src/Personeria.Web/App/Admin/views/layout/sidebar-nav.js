﻿(function () {
    var controllerId = 'admin.views.layout.sidebarNav';
    angular.module('admin').controller(controllerId, [
        '$rootScope', '$state', 'appSession',
        function ($rootScope, $state, appSession) {
            var vm = this;

            vm.menuItems = [
                createMenuItem(App.localize("HomePage"), "", "home", "home"),

                createMenuItem("Areas", "Pages.Areas", "business", "areas"),  
                createMenuItem("Personas", "Pages.Peoples", "assignment_ind", "persons"), 
                createMenuItem("Trámites", "Pages.Transacts", "art_track", "transacts"), 
                createMenuItem("Tipos de Trámite", "Pages.TransactTypes", "repeat", "transactTypes"), 
                createMenuItem("Tipos de Archivo", "Pages.FileAttachTypes", "perm_media", "fileAttachTypes"),

                createMenuItem("Usuarios", "Pages.Users", "people", "users"),
                createMenuItem("Roles", "Pages.Roles", "local_offer", "roles"),                
                createMenuItem("Configuraciones", "Pages.Config", "settings", "config"),     
                
                createMenuItem("Ir a Principal", "", "swap_horiz", "/")
            ];

            vm.showMenuItem = function (menuItem) {
                if (menuItem.permissionName) {
                    return abp.auth.isGranted(menuItem.permissionName);
                }
                return true;
            };

            function createMenuItem(name, permissionName, icon, route, childItems) {
                return {
                    name: name,
                    permissionName: permissionName,
                    icon: icon,
                    route: route,
                    items: childItems
                };
            }
        }
    ]);
})();