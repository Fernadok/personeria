﻿(function () {
    angular
        .module('admin')
        .controller('admin.views.transactType.addOrUpdate', func);

    func.$inject = [
        '$scope',
        '$timeout',
        '$uibModal',
        '$uibModalInstance',
        'abp.services.app.transactTypeService',
        'abp.services.app.transactTypeAreaService',
        'abp.services.app.areaService',
        'abp.services.app.fileAttachTypeService',
        '$uibModal',
        'id'];

    function func($scope, $timeout, $uibModal, $uibModalInstance, $transactTypeService, $transactTypeAreasService, $areaService, $fileAttachTypeService, $uibModal, id) {
        var vm = this;

        vm.permission = {
            hasEdit: abp.auth.hasPermission("Pages.TransactTypes")
        }

        vm.isNew = (id === null);
        vm.transactType = {
            id: 0,
            transactTypeAreaRelations: [],
            transactTypeFileAttachTypeRelations: []
        };

        vm.dataAttachs = [];
        vm.fileTypeList = [];

        vm.cancel = function () {
            $uibModalInstance.dismiss({});
        };

        // -- Save
        vm.save = function () {

            abp.ui.setBusy(".modal-content");
            
            if (vm.transactType.transactTypeAreaRelations && vm.transactType.transactTypeAreaRelations.length == 0) {
                abp.message.info("Tiene que agregar un área aunque sea");
                abp.ui.clearBusy(".modal-content");
                return;
            }

            vm.transactType.areas = [];
            vm.transactType.transactTypeAreaRelations.forEach(function (item, index) {
                item.order = index + 1;
                vm.transactType.areas.push(item);
            });
            vm.transactType.files = vm.transactType.transactTypeFileAttachTypeRelations;

            $transactTypeService.addOrUpdate(vm.transactType).then(function () {
                abp.ui.clearBusy(".modal-content");
                $uibModalInstance.close(true);
            }, function (error) {
                abp.ui.clearBusy(".modal-content");
            });

        }

        // -- Select Area
        var order = 0;
        vm.selectedArea = function (obj) {
            var selectedId = obj.areaId;
            $.each(vm.areaList, function (ind, it) {
                if (it.id === selectedId) {
                    order = order + 1;
                    var item = {
                        transactTypeId: vm.transactType.id,
                        areaId: it.id,
                        order: order,
                        areaName: it.description
                    };
                    vm.transactType.transactTypeAreaRelations.push(item);
                }
            });
        };

        // -- Delete Area
        vm.deleteFlujo = function (obj) {
            vm.transactType.transactTypeAreaRelations.remove(obj);
            order = order - 1;
        };

        // -- New Area
        vm.newArea = function () {
            var modalInstance = $uibModal.open({
                templateUrl: '/App/Admin/views/areas/addOrUpdateModal.cshtml',
                controller: 'admin.views.area.addOrUpdate as vm',
                backdrop: 'static',
                resolve: {
                    id: function () {
                        return null;
                    }
                }
            });

            modalInstance.rendered.then(function () {
                $.AdminBSB.input.activate();
            });

            modalInstance.result.then(function () {
                vm.getAreas();
            });
        };

        // -- Get Areas
        vm.getAreas = function () {
            $areaService.areaList().then(function (result) {
                vm.areaList = result.data;
            });
        };

        // -- Get FileTypes
        vm.getFileTypes = function () {
            $fileAttachTypeService.fileAttachTypeList().then(function (result) {
                vm.fileTypeList = result.data;                
            });
        };

        // -- Delete File
        vm.deleteAttach = function (attach) {
            vm.transactType.transactTypeFileAttachTypeRelations.remove(attach);
        };

        // -- New File
        vm.newFileAttach = function () {
            var modalInstance = $uibModal.open({
                templateUrl: '/App/Admin/views/fileAttachTypes/addOrUpdateModal.cshtml',
                controller: 'admin.views.fileAttachType.addOrUpdate as vm',
                backdrop: 'static',
                resolve: {
                    id: function () {
                        return null;
                    }
                }
            });

            modalInstance.rendered.then(function () {
                $.AdminBSB.input.activate();
            });

            modalInstance.result.then(function () {
                vm.getFileTypes();
            });
        };

        // -- Select File
        vm.selectedFile = function (obj) {
            var selectedId = obj.fileId;
            $.each(vm.fileTypeList, function (ind, it) {
                if (it.id === selectedId) {
                    var item = {
                        transactTypeId: vm.transactType.id,
                        fileAttachTypeId: it.id,
                        fileAttachTypeName: it.description,
                        isRequired: true
                    };
                    vm.transactType.transactTypeFileAttachTypeRelations.push(item);
                }
            });
        };

        // -- Init Load
        vm.getAreas();
        vm.getFileTypes();

        // -- Get data for Edit
        if (!vm.isNew) {
            $transactTypeService.getById(id).then(function (result) {
                vm.transactType = result.data;                
            });
        }
    }
})();