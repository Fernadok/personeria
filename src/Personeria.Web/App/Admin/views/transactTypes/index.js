﻿(function () {
    var controllerId = 'admin.views.transactType.index';
    angular
        .module('admin')
        .controller(controllerId, func);

    func.$inject = ['$scope', '$timeout', '$uibModal', 'abp.services.app.transactTypeService'];

    function func($scope, $timeout, $uibModal, $transactTypeService) {
        var vm = this;

        vm.filters = {
            search: '',
            page: 1,
            pageSize: 10,
            orderColumn: 'Name',
            order: 'asc',
            itemsTotal: 0
        };

        vm.load = false;
        function getTransactTypes() {
            if (!vm.load) {
                vm.load = true;
                abp.ui.setBusy(".table-responsive",
                    $transactTypeService.getAll(vm.filters).then(function (result) {
                        vm.dataContext = result.data;
                        vm.filters.itemsTotal = vm.dataContext.metaData.totalItemCount;
                        vm.load = false;
                    })
                );
            }
        };

        // -- Edit or Create
        vm.openModal = function (obj) {
            var modalInstance = $uibModal.open({
                templateUrl: '/App/Admin/views/transactTypes/addOrUpdateModal.cshtml',
                controller: 'admin.views.transactType.addOrUpdate as vm',
                backdrop: 'static',
                resolve: {
                    id: function () {
                        return obj ? obj.id : null;
                    }
                }
            });

            modalInstance.rendered.then(function () {
                $.AdminBSB.input.activate();
            });

            modalInstance.result.then(function () {
                getTransactTypes();
            });
        };

        // -- Delete
        vm.delete = function (id) {
            abp.message.confirm(
                "¿Seguro que quiere borrar éste tipo de transacción?",
                "Advertencia",
                function (result) {
                    if (result) {
                        $transactTypeService.delete(id)
                            .then(function () {
                                abp.notify.info("transactType borrado");
                                getTransactTypes();
                            });
                    }
                });
        };

        $scope.$watch("vm.filters.search", function () {
            $timeout(function () {
                vm.filters.page = 1;
                getTransactTypes();
            }, 0);

        });

        vm.pageChanged = function () {
            getTransactTypes();
        };

        getTransactTypes();

    }

})();