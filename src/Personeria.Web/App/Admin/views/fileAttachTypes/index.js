﻿(function () {
    var controllerId = 'admin.views.fileAttachType.index';
    angular
        .module('admin')
        .controller(controllerId, func);

    func.$inject = ['$scope', '$uibModal', 'abp.services.app.fileAttachTypeService'];

    function func($scope, $uibModal, $fileAttachTypeService) {
        var vm = this;

        vm.filters = {
            search: '',
            page: 1,
            pageSize: 10,
            orderColumn: 'Name',
            order: 'asc',
            itemsTotal: 0
        };

        vm.load = false;
        function getFileAttachTypes() {
            if (!vm.load) {
                vm.load = true;
                abp.ui.setBusy(".table-responsive",
                    $fileAttachTypeService.getAll(vm.filters).then(function (result) {
                        vm.dataContext = result.data;
                        vm.filters.itemsTotal = vm.dataContext.metaData.totalItemCount;
                        vm.load = false;
                    })
                );
            }
        };

        // -- Edit or Create
        vm.openModal = function (obj) {
            var modalInstance = $uibModal.open({
                templateUrl: '/App/Admin/views/fileAttachTypes/addOrUpdateModal.cshtml',
                controller: 'admin.views.fileAttachType.addOrUpdate as vm',
                backdrop: 'static',
                resolve: {
                    id: function () {
                        return obj ? obj.id : null;
                    }
                }
            });

            modalInstance.rendered.then(function () {
                $.AdminBSB.input.activate();
            });

            modalInstance.result.then(function () {
                getFileAttachTypes();
            });
        };

        // -- Delete
        vm.delete = function (id) {
            abp.message.confirm(
                "¿Seguro que quiere borrar éste tipo de archivo?",
                "Advertencia",
                function (result) {
                    if (result) {
                        $fileAttachTypeService.delete(id)
                            .then(function () {
                                abp.notify.info("fileAttachType borrado");
                                getFileAttachTypes();
                            });
                    }
                });
        };

        $scope.$watch("vm.filters.search", function () {
            vm.filters.page = 1;
            getFileAttachTypes();
        });

        vm.pageChanged = function () {
            getFileAttachTypes();
        };

    }

})();