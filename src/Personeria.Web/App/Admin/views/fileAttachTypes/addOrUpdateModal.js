﻿(function () {
    angular
        .module('admin')
        .controller('admin.views.fileAttachType.addOrUpdate', func);

    func.$inject = [
        '$scope',
        '$uibModalInstance',
        'abp.services.app.fileAttachTypeService',
        '$uibModal',
        'id'];

    function func($scope, $uibModalInstance, $fileAttachTypeService, $uibModal, id) {
        var vm = this;

        vm.permission = {
            hasEdit: abp.auth.hasPermission("Pages.fileAttachTypes")
        }

        vm.isNew = (id === null);
        vm.fileAttachType = {};

        vm.cancel = function () {
            $uibModalInstance.dismiss({});
        };

        vm.save = function () {
            abp.ui.setBusy(".modal-content");
            $fileAttachTypeService.addOrUpdate(vm.fileAttachType).then(function () {
                abp.ui.clearBusy(".modal-content");
                $uibModalInstance.close(true);
            }, function (error) {
                abp.ui.clearBusy(".modal-content");
            });
        }

        if (!vm.isNew) {
            $fileAttachTypeService.getById(id).then(function (result) {
                vm.fileAttachType = result.data;
            });
        }
    }
})();