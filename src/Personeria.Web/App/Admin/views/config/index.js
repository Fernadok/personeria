﻿(function () {
    var controllerId = 'admin.views.config';
    angular.module('admin').controller(controllerId, [
        '$scope',
        '$uploadService',
        '$timeout',
        'abp.services.app.configuration',
        function (
            $scope,
            $uploadService,
            $timeout,
            $config) {
            var vm = this;
            //Config logic...

            // -- Traigo la imagenes para la galeria configuradas
            vm.getConfig = function () {
                abp.ui.setBusy(".photos");
                $config.getHomePhotos().then(function (result) {
                    vm.files = result.data;
                    abp.ui.clearBusy(".photos");
                    $timeout(function () {
                        $.AdminBSB.input.activate();
                    }, 500);
                });
            };

            // -- Save
            vm.save = function (file) {  
                var photoId = ".photo-" + file.id;
                abp.ui.setBusy(photoId);
                $config.savePhoto(file).then(function (result) {
                    abp.ui.clearBusy(photoId);    
                });
            };

            // -- Delete
            vm.delete = function (file) {
                var photoId = ".photo-" + file.id;
                abp.ui.setBusy(photoId);
                $config.deletePhoto(file.id).then(function (result) {
                    abp.ui.clearBusy(photoId);
                    vm.files.remove(file);
                });
            };

            // -- Implemebnt Upload
            vm.finish = true;
            vm.file = null;
            vm.progress = 0;
            vm.upload = function (fileIn) {
                if (fileIn && !fileIn.$error && vm.finish) {
                    abp.ui.setBusy(".add-photo");
                    vm.finish = false;
                    fileIn.save = true;
                    fileIn.typePhoto = "CONFIG";
                    $uploadService.uploadFile(fileIn, function (resdata) {                                                
                        vm.finish = true;
                        abp.ui.clearBusy(".add-photo");
                        vm.getConfig();
                    }, function (progress) {
                        vm.progress = progress;
                    });
                }
            };

            // -- Get Prints
            vm.prints = [];
            vm.print = {};
            vm.getPrints = function () {
                abp.ui.setBusy(".prints");
                $config.getPrints().then(function (result) {
                    vm.prints = result.data;
                    abp.ui.clearBusy(".prints");
                });
            };

            // -- Add or Update Prints
            vm.addOrUpdatePrint = function () {
                $config.addOrUpdatePrint(vm.print).then(function (result) {
                    vm.print = {};
                    vm.getPrints();
                });
            };

            // -- Delete Prints
            vm.deletePrint = function (printId) {
                $config.deletePrint(printId).then(function (result) {
                    vm.getPrints();
                });
            };

            // -- Select Print
            vm.selectPrint = function (print) {
                vm.print = print;
            };

            vm.deSelectPrint = function (print) {
                vm.print = {};
            };


            vm.getConfig();
            vm.getPrints();
        }
    ]);
})();