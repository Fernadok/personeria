﻿(function () {
    angular
        .module('admin')
        .controller('admin.views.transactTypeAreas.addOrUpdate', func);

    func.$inject = [
        '$scope',
        '$uibModalInstance',
        'abp.services.app.transactTypeAreaService',
        'abp.services.app.areaService',
        'abp.services.app.transactTypeService',
        '$uibModal',
        'id'];

    function func($scope, $uibModalInstance, $transactTypeAreasService, $areaService, $transactTypeService, $uibModal, id) {
        var vm = this;

        vm.permission = {
            hasEdit: abp.auth.hasPermission("Pages.transactTypeAreass")
        }

        vm.isNew = (id === null);
        vm.transactTypeArea = {
        };

        vm.cancel = function () {
            $uibModalInstance.dismiss({});
        };

        vm.save = function () {
            abp.ui.setBusy(".modal-content");
            $transactTypeAreasService.addOrUpdate(vm.transactTypeArea).then(function () {
                abp.ui.clearBusy(".modal-content");
                $uibModalInstance.close(true);
            }, function (error) {
                abp.ui.clearBusy(".modal-content");
            });
        }

        vm.getAreas = function () {
            $areaService.areaList()
                .then(function (result) {
                    vm.areaList = result.data;

                    if (!vm.isNew) {
                        $transactTypeAreasService.getById(id).then(function (result) {
                            vm.transactTypeArea = result.data;
                        });
                    }
                });
        }

        vm.getTransactTypes = function () {
            $transactTypeService.transactTypeList()
                .then(function (result) {
                    vm.transactTypeList = result.data;
                    vm.getAreas();
                });
        }
        vm.getTransactTypes();

    }
})();