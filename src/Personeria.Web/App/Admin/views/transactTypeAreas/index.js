﻿(function () {
    var controllerId = 'admin.views.transactTypeAreas.index';
    angular
        .module('admin')
        .controller(controllerId, func);

    func.$inject = ['$scope', '$uibModal', 'abp.services.app.transactTypeAreaService'];

    function func($scope, $uibModal, $transactTypeAreasService) {
        var vm = this;

        vm.filters = {
            search: '',
            page: 1,
            pageSize: 10,
            orderColumn: 'Id',
            order: 'asc',
            itemsTotal: 0
        };

        vm.load = false;
        function gettransactTypeAreassa() {
            if (!vm.load) {
                vm.load = true;
                abp.ui.setBusy(".table-responsive",
                    $transactTypeAreasService.getAll(vm.filters).then(function (result) {
                        vm.dataContext = result.data;
                        vm.filters.itemsTotal = vm.dataContext.metaData.totalItemCount;
                        vm.load = false;
                    })
                );
            }
        }

        // -- Edit or Create
        vm.openModal = function (obj) {
            var modalInstance = $uibModal.open({
                templateUrl: '/App/Admin/views/transactTypeAreas/addOrUpdateModal.cshtml',
                controller: 'admin.views.transactTypeAreas.addOrUpdate as vm',
                backdrop: 'static',
                resolve: {
                    id: function () {
                        return obj ? obj.id : null;
                    }
                }
            });

            modalInstance.rendered.then(function () {
                $.AdminBSB.input.activate();
            });

            modalInstance.result.then(function () {
                gettransactTypeAreassa();
            });
        };

        // -- Delete
        vm.delete = function (id) {
            abp.message.confirm(
                "¿Seguro que quiere borrar ésta transacción?",
                "Advertencia",
                function (result) {
                    if (result) {
                        $transactTypeAreasService.delete(id)
                            .then(function () {
                                abp.notify.info("transacción borrado");
                                gettransactTypeAreassa();
                            });
                    }
                });
        };

        $scope.$watch("vm.filters.search", function () {
            vm.filters.page = 1;
            gettransactTypeAreassa();
        });

        vm.pageChanged = function () {
            gettransactTypeAreassa();
        };

        gettransactTypeAreassa();
    }

})();