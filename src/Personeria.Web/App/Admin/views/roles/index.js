﻿(function () {
    angular
        .module('admin')
        .controller('admin.views.roles.index', func);

    func.$inject = ['$scope', '$uibModal', 'abp.services.app.role', '$timeout'];

    function func($scope, $uibModal, roleService, $timeout) {

        var vm = this;

        vm.filters = {
            search: '',
            page: 1,
            pageSize: 10,
            orderColumn: 'RoleId',
            order: 'asc',
            itemsTotal: 0
        }

        vm.roles = [];

        function getRoles() {
            roleService.getAll({}).then(function (result) {
                vm.roles = result.data.items;
            });
        }

        vm.openRoleCreationModal = function () {
            var modalInstance = $uibModal.open({
                templateUrl: '/App/admin/views/roles/createModal.cshtml',
                controller: 'admin.views.roles.createModal as vm',
                backdrop: 'static'
            });

            modalInstance.rendered.then(function () {
                $timeout(function () {
                    $.AdminBSB.input.activate();
                }, 500);
            });

            modalInstance.result.then(function () {
                getRoles();
            });
        };

        vm.openRoleEditModal = function (role) {
            var modalInstance = $uibModal.open({
                templateUrl: '/App/admin/views/roles/editModal.cshtml',
                controller: 'admin.views.roles.editModal as vm',
                backdrop: 'static',
                resolve: {
                    id: function () {
                        return role.id;
                    }
                }
            });

            modalInstance.rendered.then(function () {
                $timeout(function () {
                    $.AdminBSB.input.activate();
                }, 500)
            });

            modalInstance.result.then(function () {
                getRoles();
            });
        };

        vm.delete = function (role) {
            abp.message.confirm(
                "Borrar rol '" + role.name + "'?",
                function (result) {
                    if (result) {
                        roleService.delete({ id: role.id })
                            .then(function () {
                                abp.notify.info("Deleted role: " + role.name);
                                getRoles();
                            });
                    }
                });
        }

        vm.refresh = function () {
            getRoles();
        };

        $scope.$watch("vm.filters.search", function () {
            vm.filters.page = 1;
            getRoles();
        });

        getRoles();
    }
})();