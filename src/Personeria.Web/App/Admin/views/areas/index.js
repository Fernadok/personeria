﻿(function () {
    var controllerId = 'admin.views.area.index';
    angular
        .module('admin')
        .controller(controllerId, func);

    func.$inject = ['$scope', '$uibModal', 'abp.services.app.areaService'];

    function func($scope, $uibModal, $areaService) {
        var vm = this;

        vm.filters = {
            search: '',
            page: 1,
            pageSize: 10,
            orderColumn: 'Name',
            order: 'asc',
            itemsTotal: 0
        };

        vm.load = false;
        function getAreasa() {
            if (!vm.load) {
                vm.load = true;
                abp.ui.setBusy(".table-responsive",
                    $areaService.getAll(vm.filters).then(function (result) {
                        vm.dataContext = result.data;
                        vm.filters.itemsTotal = vm.dataContext.metaData.totalItemCount;
                        vm.load = false;
                    })
                );
            }
        };

        // -- Edit or Create
        vm.openModal = function (obj) {
            var modalInstance = $uibModal.open({
                templateUrl: '/App/Admin/views/areas/addOrUpdateModal.cshtml',
                controller: 'admin.views.area.addOrUpdate as vm',
                backdrop: 'static',
                resolve: {
                    id: function () {
                        return obj ? obj.id : null;
                    }
                }
            });

            modalInstance.rendered.then(function () {
                $.AdminBSB.input.activate();
            });

            modalInstance.result.then(function () {
                getAreasa();
            });
        };

        // -- Delete
        vm.delete = function (id) {
            abp.message.confirm(
                "¿Seguro que quiere borrar ésta area?",
                "Advertencia",
                function (result) {
                    if (result) {
                        $areaService.delete(id)
                            .then(function () {
                                abp.notify.info("area borrado");
                                getAreasa();
                            });
                    }
                });
        };

        $scope.$watch("vm.filters.search", function () {
            vm.filters.page = 1;
            getAreasa();
        });

        vm.pageChanged = function () {
            getAreasa();
        };
    }
})();