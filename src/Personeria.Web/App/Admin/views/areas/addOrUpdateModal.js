﻿(function () {
    angular
        .module('admin')
        .controller('admin.views.area.addOrUpdate', func);

    func.$inject = [
        '$scope',
        '$uibModalInstance',
        'abp.services.app.areaService',
        '$uibModal',
        'id'];

    function func($scope, $uibModalInstance, $areaService, $uibModal, id) {
        var vm = this;

        vm.permission = {
            hasEdit: abp.auth.hasPermission("Pages.Areas")
        }

        vm.isNew = (id === null);
        vm.area = {};

        vm.cancel = function () {
            $uibModalInstance.dismiss({});
        };

        vm.save = function () {
            abp.ui.setBusy(".modal-content");
            $areaService.addOrUpdate(vm.area).then(function () {
                abp.ui.clearBusy(".modal-content");
                $uibModalInstance.close(true);
            }, function (error) {
                abp.ui.clearBusy(".modal-content");
            });
        }

        if (!vm.isNew) {
            $areaService.getById(id).then(function (result) {
                vm.area = result.data;
            });
        }
    }
})();