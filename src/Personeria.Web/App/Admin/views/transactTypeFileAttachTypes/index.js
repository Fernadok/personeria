﻿(function () {
    var controllerId = 'admin.views.TransactTypeFileAttachTypes.index';
    angular
        .module('admin')
        .controller(controllerId, func);

    func.$inject = ['$scope', '$uibModal', 'abp.services.app.transactTypeFileAttachTypeService'];

    function func($scope, $uibModal, $transactTypeFileAttachTypesService) {
        var vm = this;

        vm.filters = {
            search: '',
            page: 1,
            pageSize: 10,
            orderColumn: 'Id',
            order: 'asc',
            itemsTotal: 0
        };

        vm.load = false;
        function getTransactTypeFileAttachTypessa() {
            if (!vm.load) {
                vm.load = true;
                abp.ui.setBusy(".table-responsive",
                    $transactTypeFileAttachTypesService.getAll(vm.filters).then(function (result) {
                        vm.dataContext = result.data;
                        vm.filters.itemsTotal = vm.dataContext.metaData.totalItemCount;
                        vm.load = false;
                    })
                );
            }
        };

        // -- Edit or Create
        vm.openModal = function (obj) {
            var modalInstance = $uibModal.open({
                templateUrl: '/App/Admin/views/transactTypeFileAttachTypes/addOrUpdateModal.cshtml',
                controller: 'admin.views.transactTypeFileAttachTypes.addOrUpdate as vm',
                backdrop: 'static',
                resolve: {
                    id: function () {
                        return obj ? obj.id : null;
                    }
                }
            });

            modalInstance.rendered.then(function () {
                $.AdminBSB.input.activate();
            });

            modalInstance.result.then(function () {
                getTransactTypeFileAttachTypessa();
            });
        };

        // -- Delete
        vm.delete = function (id) {
            abp.message.confirm(
                "¿Seguro que quiere borrar éste registro?",
                "Advertencia",
                function (result) {
                    if (result) {
                        $transactTypeFileAttachTypesService.delete(id)
                            .then(function () {
                                abp.notify.info("transacción borrado");
                                getTransactTypeFileAttachTypessa();
                            });
                    }
                });
        };

        $scope.$watch("vm.filters.search", function () {
            vm.filters.page = 1;
            getTransactTypeFileAttachTypessa();
        });

        vm.pageChanged = function () {
            getTransactTypeFileAttachTypessa();
        };

        getTransactTypeFileAttachTypessa();
    }

})();