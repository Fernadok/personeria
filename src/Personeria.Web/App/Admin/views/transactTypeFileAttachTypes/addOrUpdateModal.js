﻿(function () {
    angular
        .module('admin')
        .controller('admin.views.transactTypeFileAttachTypes.addOrUpdate', func);

    func.$inject = [
        '$scope',
        '$uibModalInstance',
        'abp.services.app.transactTypeFileAttachTypeService',
        'abp.services.app.fileAttachTypeService',
        'abp.services.app.transactTypeService',
        '$uibModal',
        'id'];

    function func($scope, $uibModalInstance, $TransactTypeFileAttachTypesService, $fileAttachTypesService, $transactTypeService, $uibModal, id) {
        var vm = this;

        vm.permission = {
            hasEdit: abp.auth.hasPermission("Pages.TransactTypeFileAttachTypess")
        }

        vm.isNew = (id === null);
        vm.TransactTypeFileAttachType = {
            isRequired: false
        };

        vm.cancel = function () {
            $uibModalInstance.dismiss({});
        };

        vm.save = function () {
            abp.ui.setBusy(".modal-content");
            $TransactTypeFileAttachTypesService.addOrUpdate(vm.TransactTypeFileAttachType).then(function () {
                abp.ui.clearBusy(".modal-content");
                $uibModalInstance.close(true);
            }, function (error) {
                abp.ui.clearBusy(".modal-content");
            });
        }

        vm.getFileAttachs= function () {
            $fileAttachTypesService.fileAttachTypeList()
                .then(function (result) {
                    vm.fileAttachTypeList = result.data;

                    if (!vm.isNew) {
                        $TransactTypeFileAttachTypesService.getById(id).then(function (result) {
                            vm.TransactTypeFileAttachType = result.data;
                        });
                    }
                });
        }

        vm.getTransactTypes = function () {
            $transactTypeService.transactTypeList()
                .then(function (result) {
                    vm.transactTypeList = result.data;
                    vm.getFileAttachs();
                });
        }
        vm.getTransactTypes();

    }
})();