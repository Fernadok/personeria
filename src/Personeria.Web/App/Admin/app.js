﻿(function () {
    'use strict';

    var app = angular.module('admin', [
        'ngAnimate',
        'ngSanitize',
        'ngTouch',

        'ui.router',
        'ui.bootstrap',
        'ui.jq',
        'angular.chosen',
        'notification',

        'cfp.loadingBar',
        'ngFileUpload',
        
        'abp'
    ]);

    //-- Generando constante Config
    // http://personeria.sanluis.gov.ar/
    var config = {
        webDomain: location.origin
    };
    app.constant('config', config);

    //Configuration for Angular UI routing.
    app.config([
        '$stateProvider',
        '$urlRouterProvider',
        '$locationProvider',
        '$qProvider',
        'cfpLoadingBarProvider',
        function ($stateProvider,
            $urlRouterProvider,
            $locationProvider,
            $qProvider,
            cfpLoadingBarProvider) {

            $locationProvider.hashPrefix('');
            $urlRouterProvider.otherwise('/');
            $qProvider.errorOnUnhandledRejections(false);
            $locationProvider.html5Mode(true);

            // -- Config Loading Bar
            cfpLoadingBarProvider.includeSpinner = true;
            cfpLoadingBarProvider.latencyThreshold = 500;

            if (abp.auth.hasPermission('Pages.Users')) {
                $stateProvider
                    .state('users', {
                        url: '/usuarios',
                        templateUrl: '/App/admin/views/users/index.cshtml',
                        menu: 'Usuarios' //Matches to name of 'Users' menu in PersoneriaNavigationProvider
                    });
            }
            //Roles
            if (abp.auth.hasPermission('Pages.Roles')) {
                $stateProvider
                    .state('roles', {
                        url: '/roles',
                        templateUrl: '/App/admin/views/roles/index.cshtml',
                        menu: 'Roles' //Matches to name of 'Tenants' menu in PersoneriaNavigationProvider
                    });
            }
            //Areas
            if (abp.auth.hasPermission('Pages.Areas')) {
                $stateProvider
                    .state('areas', {
                        url: '/areas',
                        templateUrl: '/App/admin/views/areas/index.cshtml',
                        menu: 'Areas'
                    });
            }
            //TransactTypes
            if (abp.auth.hasPermission('Pages.TransactTypes')) {
                $stateProvider
                    .state('transactTypes', {
                        url: '/tipos-de-tramite',
                        templateUrl: '/App/admin/views/transactTypes/index.cshtml',
                        menu: 'Tipo de transacciones'
                    });
            }
            //FileAttachTypes
            if (abp.auth.hasPermission('Pages.FileAttachTypes')) {
                $stateProvider
                    .state('fileAttachTypes', {
                        url: '/tipos-de-archivo',
                        templateUrl: '/App/admin/views/fileAttachTypes/index.cshtml',
                        menu: 'Tipo de archivos'
                    });
            }
            //TransactTypeAreas
            if (abp.auth.hasPermission('Pages.TransactTypeAreas')) {
                $stateProvider
                    .state('transactTypeAreas', {
                        url: '/tipos-tramite-por-area',
                        templateUrl: '/App/admin/views/transactTypeAreas/index.cshtml',
                        menu: 'TransactTypeAreas'
                    });
            }
            //transactTypeFileAttachTypes
            if (abp.auth.hasPermission('Pages.TransactTypeAreas')) {
                $stateProvider
                    .state('transactTypeFileAttachTypes', {
                        url: '/tipos-archivo-por-tramite',
                        templateUrl: '/App/admin/views/transactTypeFileAttachTypes/index.cshtml',
                        menu: 'TransactTypeFileAttachTypes'
                    });
            }

            //Peoples
            if (abp.auth.hasPermission('Pages.Peoples')) {
                $stateProvider
                    .state('persons', {
                        url: '/personas',
                        templateUrl: '/App/admin/views/persons/index.cshtml',
                        menu: 'PERSONAS'
                    });
            }

            //Config
            if (abp.auth.hasPermission('Pages.Config')) {
                $stateProvider
                    .state('config', {
                        url: '/configuraciones',
                        templateUrl: '/App/admin/views/config/index.cshtml',
                        menu: 'Configuraciones' 
                    });
            }

            //Transacts
            $stateProvider
                .state('transactsWithParams', {
                    url: '/tramites?tipotramite&estado',
                    templateUrl: '/App/admin/views/transacts/index.cshtml',
                    menu: 'TRAMITES'
                })
                .state('transacts', {
                    url: '/tramites',
                    templateUrl: '/App/admin/views/transacts/index.cshtml',
                    menu: 'TRAMITES'
                })
                .state('transacts.detail', {
                    url: '/:paramId/detalle',                    
                    menu: 'DETALLE DEL TRAMITE',
                    views: {
                        'detail': {
                            templateUrl: '/App/admin/views/transacts/detailModal.cshtml'
                        }
                    }
                });
            

            $stateProvider
                .state('home', {
                    url: '/',
                    templateUrl: '/App/admin/views/home/home.cshtml',
                    menu: 'Admin' //Matches to name of 'Home' menu in PersoneriaNavigationProvider
                });
        }
    ])
        .run(['$rootScope', '$timeout', 'cfpLoadingBar', '$location', '$window', function ($rootScope, $timeout, cfpLoadingBar, $location, $window) {

            var loadTop = function () {
                $("html,body").animate({ scrollTop: 0 }, 1000);
            };

            var closedLeft = function () {
                $timeout(function () {
                    var isOpen = $("body.ls-closed").hasClass("overlay-open");
                    if (isOpen) {
                        $(".bars").click();
                    }
                }, 200);
            };

            var closedLoading = function () {
                $timeout(function () {
                    $('.page-loader').slideUp();
                }, 500);
            };

            if (!navigator.onLine) {
                location.href = "/offline.html";
            }
            $window.addEventListener("offline", function () {
                location.href = "/offline.html?returnUrl=" + $location.absUrl();
            }, false);

            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                cfpLoadingBar.start();
            });
            $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                document.title = "Personeria | " + toState.menu.toUpperCase();
                closedLeft();
                closedLoading();
                cfpLoadingBar.complete();
            });
            $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams) { });
            $rootScope.$on('$stateNotFound', function (event, toState, toParams, fromState, fromParams) { });
            $rootScope.$on('$viewContentLoading', function (event, viewConfig) { });
            $rootScope.$on('$viewContentLoaded', function (event, viewConfig) {
                //if (!$window.isDebug) {
                //    $window.ga('send', 'pageview', { page: $location.url() });
                //}
            });

        }]);

})();