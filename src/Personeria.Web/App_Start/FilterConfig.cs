﻿using Personeria.Web.Filters;
using System.Web.Mvc;

namespace Personeria.Web
{
    public static class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new CompressFilterAttribute());
            filters.Add(new WhitespaceFilterAttribute());
            //filters.Add(new ETagFilterAttribute());
        }
    }
}