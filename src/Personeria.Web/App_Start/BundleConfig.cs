﻿using Personeria.Web.Transforms;
using System.Web.Optimization;

namespace Personeria.Web
{
    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();

            //~/app/vendorcss
            bundles.Add(
                new StyleBundle("~/app/vendorcss")
                    .Include("~/fonts/roboto/roboto.css", new CssRewriteUrlTransform())
                    .Include("~/fonts/material-icons/materialicons.css", new CssRewriteUrlTransform())
                    .Include("~/lib/bootstrap/dist/css/bootstrap.css", new CssRewriteUrlTransform())
                    .Include("~/lib/toastr/toastr.css", new CssRewriteUrlTransform())
                    .Include("~/lib/famfamfam-flags/dist/sprite/famfamfam-flags.css", new CssRewriteUrlTransform())
                    .Include("~/lib/font-awesome/css/font-awesome.css", new CssRewriteUrlTransform())
                    .Include("~/lib/Waves/dist/waves.css", new CssRewriteUrlTransform())
                    .Include("~/lib/animate.css/animate.css", new CssRewriteUrlTransform())
                    .Include("~/css/materialize.css", new CssRewriteUrlTransform())
                    .Include("~/css/style.css", new CssRewriteUrlTransform())
                    .Include("~/css/themes/all-themes.css", new CssRewriteUrlTransform())
                    .Include("~/css/loading.css", new CssRewriteUrlTransform())
                    .Include("~/Scripts/chosen/chosen.min.css", new CssRewriteUrlTransform())
                    .Include("~/Content/jquery.steps.css", new CssRewriteUrlTransform())
                    .Include("~/lib/datetime-material/bootstrap-material-datetimepicker.css", new CssRewriteUrlTransform())
                );

            //~/app/vendorjs
            bundles.Add(
                new ScriptBundle("~/app/vendorjs")
                    .Include(

                        "~/lib/json2/json2.js",
                        "~/lib/jquery/dist/jquery.js",
                        "~/lib/bootstrap/dist/js/bootstrap.js",
                        "~/lib/moment/min/moment-with-locales.js",
                        "~/lib/jquery-validation/dist/jquery.validate.js",
                        "~/lib/blockUI/jquery.blockUI.js",
                        "~/lib/toastr/toastr.js",
                        "~/lib/sweetalert/dist/sweetalert.min.js",
                        "~/lib/spin.js/spin.js",
                        "~/lib/spin.js/jquery.spin.js",
                        "~/lib/bootstrap-select/dist/js/bootstrap-select.js",
                        "~/lib/jquery-slimscroll/jquery.slimscroll.js",
                        "~/lib/Waves/dist/waves.js",
                        "~/lib/push.js/push.js",
                        "~/lib/jquery.serializejson/jquery.serializejson.js",

                         "~/lib/jquery-countTo/jquery.countTo.js",
                        "~/lib/raphael/raphael.js",
                        "~/lib/morris.js/morris.js",
                        "~/lib/chart.js/dist/Chart.bundle.js",
                        "~/lib/Flot/jquery.flot.js",
                        "~/lib/Flot/jquery.flot.resize.js",
                        "~/lib/Flot/jquery.flot.pie.js",
                        "~/lib/Flot/jquery.flot.categories.js",
                        "~/lib/Flot/jquery.flot.time.js",
                        "~/lib/jquery-sparkline/dist/jquery.sparkline.js",

                        "~/Scripts/angular.min.js",
                        "~/Scripts/angular-animate.min.js",
                        "~/Scripts/angular-sanitize.min.js",
                        "~/Scripts/angular-touch.min.js",
                        "~/Scripts/angular-notification.min.js",
                        "~/Scripts/angular-ui-router.min.js",
                        "~/Scripts/angular-ui/ui-bootstrap.min.js",
                        "~/Scripts/angular-ui/ui-bootstrap-tpls.min.js",
                        "~/Scripts/angular-ui/ui-utils.min.js",

                        "~/Abp/Framework/scripts/abp.js",
                        "~/Abp/Framework/scripts/libs/abp.jquery.js",
                        "~/Abp/Framework/scripts/libs/abp.toastr.js",
                        "~/Abp/Framework/scripts/libs/abp.blockUI.js",
                        "~/Abp/Framework/scripts/libs/abp.spin.js",
                        "~/Abp/Framework/scripts/libs/abp.sweet-alert.js",
                        "~/Abp/Framework/scripts/libs/angularjs/abp.ng.js",

                        "~/Scripts/chosen/chosen.jquery.min.js",
                        "~/Scripts/chosen/angular-chosen.js",
                        "~/Scripts/loading-bar.js",

                        "~/Scripts/FileAPI.min.js",
                        "~/Scripts/ng-file-upload-shim.min.js",
                        "~/Scripts/ng-file-upload.js",

                        "~/js/admin.js",
                        "~/Scripts/jquery.slimscroll.js",
                        "~/Scripts/jquery.steps.js",
                        "~/Scripts/form-wizard.js",
                        "~/Scripts/demo.js",
                        "~/Scripts/epos-print-5.0.0.js",
                        "~/Scripts/epos-print-editor-en.js",

                        "~/lib/datetime-material/bootstrap-material-datetimepicker.js",

                        "~/Scripts/jquery.signalR-2.2.3.min.js",
                        "~/js/main.js"
                    )
                );

            //~/app/maincss
            bundles.Add(
                new StyleBundle("~/app/maincss")
                    .IncludeDirectory("~/App/main", "*.css", true)
                );

            //~/app/mainjs
            bundles.Add(
                new ScriptBundle("~/app/mainjs")
                    .IncludeDirectory("~/Common/Scripts", "*.js", true)
                    .IncludeDirectory("~/App/main", "*.js", true)
                );

            //~/app/admincss
            bundles.Add(
                new StyleBundle("~/app/admincss")
                    .IncludeDirectory("~/App/admin", "*.css", true)
                );

            //~/app/adminjs
            bundles.Add(
                new ScriptBundle("~/app/adminjs")
                    .IncludeDirectory("~/Common/Scripts", "*.js", true)
                    .IncludeDirectory("~/App/admin", "*.js", true)
                );

            bundles.Add(
              new ScriptBundle("~/app/signalrlib")
                  .Include("~/abp/framework/scripts/libs/abp.signalr.js"));
        }
    }
}