﻿using System;
using System.Configuration;
using Abp.Owin;
using Personeria.Api.Controllers;
using Personeria.Web;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using Microsoft.AspNet.SignalR;
using Owin.ServiceWorker;
using Microsoft.Owin.Cors;

[assembly: OwinStartup(typeof(Startup))]

namespace Personeria.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseAbp();

            app.UseOAuthBearerAuthentication(AccountController.OAuthBearerOptions);

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/iniciar"),
                // evaluate for Persistent cookies (IsPermanent == true). Defaults to 14 days when not set.
                ExpireTimeSpan = new TimeSpan(int.Parse(ConfigurationManager.AppSettings["AuthSession.ExpireTimeInDays.WhenPersistent"] ?? "14"), 0, 0, 0),
                SlidingExpiration = true
            });

            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // -- Service Worker Config
            var config = new DefaultServiceWorkerConfig(ConfigurationManager.AppSettings);            
            app.UseServiceWorker(config,cacheId: AppVersionHelper.ReleaseDate.ToString("yyyyMMddhhmm"));

            // -- SignalR Config
            app.Map("/app/signalr", map =>
            {
                map.UseCors(CorsOptions.AllowAll);
                var hubConfiguration = new HubConfiguration { };
                map.RunSignalR(hubConfiguration);
            });
        }
    }
}
