﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace Personeria.Web.Helpers
{
    public static class GeneralHelpers
    {
        public static bool IsDebug(this HtmlHelper htmlHelper)
        {
            #if DEBUG
               return true;
            #else
               return false;
            #endif
        }

        public static bool IsDebug()
        {            
            #if DEBUG
                 return true;
            #else
                 return false;
            #endif
        }

        public static byte[] ToBytes(this HttpPostedFileBase file)
        {
            byte[] data;
            using (Stream inputStream = file.InputStream)
            {
                MemoryStream memoryStream = inputStream as MemoryStream;
                if (memoryStream == null)
                {
                    memoryStream = new MemoryStream();
                    inputStream.CopyTo(memoryStream);
                }
                data = memoryStream.ToArray();
            }
            return data;
        }
    }
}
