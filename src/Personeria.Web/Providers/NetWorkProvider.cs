﻿using Abp.Dependency;
using Personeria.Providers;
using Personeria.Web.Helpers;
using System.Configuration;
using System.Web;

namespace Personeria.Web.Providers
{
    public class NetWorkProvider : INetWorkProvider, ISingletonDependency
    {
        public string GetAppSettings(string key)
        {
            return ConfigurationManager.AppSettings[key].ToString();
        }

        public string GetCodeQr(string text, int height = 250, int width = 250, int margin = 0)
        {
            return text.GenerateQrCodeBase64(height, width, margin);
        }

        public string GetConnetionString()
        {
            return ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
        }

        public string MapPath(string path)
        {
            if (path.StartsWith("http")) return "";
            return HttpContext.Current.Server.MapPath(path);
        }
    }
}
