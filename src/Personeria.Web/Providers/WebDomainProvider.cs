﻿using Abp.Dependency;
using Abp.Extensions;
using System.Web;
using Personeria.Providers;
using Personeria.Web.Helpers;

namespace Personeria.Web.Providers
{
    public class WebDomainProvider : IWebDomainProvider, ISingletonDependency
    {
        public string CurrentDomain
        {
            get
            {
                return HttpContext.Current.Request.Url.AbsoluteUri.ToLower().Replace(HttpContext.Current.Request.Url.AbsolutePath.ToLower(), "").Split("?")[0];
            }
        }

        public bool IsDebug
        {
            get
            {
                return GeneralHelpers.IsDebug();
            }
        }
    }
}
