﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Abp.Auditing;
using Abp.AutoMapper;
using Abp.Web.Mvc.Authorization;
using Abp.WebApi.Controllers.Dynamic.Scripting;
using Microsoft.Ajax.Utilities;
using Personeria.Domain;
using Personeria.FileAttachs;
using Personeria.FileAttachs.Dto;
using Personeria.Web.Helpers;

namespace Personeria.Web.Controllers
{
    //[AbpMvcAuthorize]
    public class HomeController : PersoneriaControllerBase
    {
        public ActionResult Index()
        {
            return View("~/App/Main/views/layout/layout.cshtml");
        }

        [AbpMvcAuthorize]
        [Route("admin")]
        public ActionResult Admin()
        {
            return View("~/App/admin/views/layout/layout.cshtml");
        }

        //[AbpMvcAuthorize]
        [DisableAuditing]
        [Route("upload")]
        [HttpPost, ValidateInput(false)]
        public JsonResult Upload(bool save = false, string typePhoto = PersoneriaConsts.PersonKey)
        {
            FileAttach result = null;

            if (Request.Files.Count > 0)
            {                
                var file = Request.Files[0];
                if (typePhoto.ToUpper() == PersoneriaConsts.ConfigKey && AbpSession.UserId.HasValue)
                {
                    result = FileAttachService.Upload(file.FileName, file.ToBytes(), save, PersoneriaConsts.ConfigKey);
                }
                else
                {
                    result = FileAttachService.Upload(file.FileName, file.ToBytes(), save);
                }
            }

            return Json(result?.MapTo<FileAttachDto>());
        }

        [Route("app/jsproxy")]
        [DisableAuditing]
        public async Task<ActionResult> GetProxy(string types = "")
        {
            try
            {
                var sb = new StringBuilder();

                //sb.AppendLine(_sessionScriptManager.GetScript());
                //sb.AppendLine();

                sb.AppendLine(LocalizationScriptManager.GetScript());
                sb.AppendLine();

                //sb.AppendLine(await _navigationScriptManager.GetScriptAsync());
                //sb.AppendLine();

                if (AbpSession.UserId.HasValue && AbpSession.UserId.Value != 0)
                {
                    try
                    {
                        sb.AppendLine(SessionScriptManager.GetScript());
                        sb.AppendLine();

                        sb.AppendLine(await AuthorizationScriptManager.GetScriptAsync());
                        sb.AppendLine();

                    }
                    catch (Exception)
                    {
                        return Redirect("/cerrar");
                    }

                }

                sb.AppendLine(await SettingScriptManager.GetScriptAsync());
                sb.AppendLine();

                if (types == "" || types.ToLower().Contains("jquery"))
                {
                    sb.AppendLine(ScriptProxyManager.GetAllScript(ProxyScriptType.JQuery));
                    sb.AppendLine();
                }
                if (types == "" || types.ToLower().Contains("angular"))
                {
                    sb.AppendLine(ScriptProxyManager.GetAllScript(ProxyScriptType.Angular));
                    sb.AppendLine();
                }

                Minifier minifier = new Minifier();
                var codeSettings = new CodeSettings()
                {
                    EvalTreatment = EvalTreatment.MakeImmediateSafe,
                    PreserveImportantComments = false,
                    MinifyCode = true
                };
                string str = GeneralHelpers.IsDebug() ? sb.ToString() : minifier.MinifyJavaScript(sb.ToString(), codeSettings);

                return JavaScript(str);

            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error inesperado ", ex);
            }
        }
    }
}