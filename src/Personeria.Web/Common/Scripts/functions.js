﻿// -- Get Context App and Template
var App = App || {};
App.template = App.template || {};

(function () {
    $(function () {

        /*
         * Link prevent
         */
        $('body').on('click', '.a-prevent', function (e) {
            e.preventDefault();
        });


        /*
         * Tooltips
         */
        if ($('[data-toggle="tooltip"]')[0]) {
            $('[data-toggle="tooltip"]').tooltip();
        }

        /*
         * Popover
         */
        if ($('[data-toggle="popover"]')[0]) {
            $('[data-toggle="popover"]').popover();
        }

        /*
         * IE 9 Placeholder
         */
        if ($('html').hasClass('ie9')) {
            $('input, textarea').placeholder({
                customClass: 'ie9-placeholder'
            });
        }
    });
})(App);
