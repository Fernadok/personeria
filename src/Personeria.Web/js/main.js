﻿(function ($) {

    //Notification handler
    //abp.event.on('abp.notifications.received', function (userNotification) {
    //    abp.notifications.showUiNotifyForUserNotification(userNotification);

    //    //Desktop notification
    //    Push.create("AbpZeroTemplate", {
    //        body: userNotification.notification.data.message,
    //        icon: abp.appPath + 'images/app-logo-small.png',
    //        timeout: 6000,
    //        onClick: function () {
    //            window.focus();
    //            this.close();
    //        }
    //    });
    //});

    //serializeFormToObject plugin for jQuery
    $.fn.serializeFormToObject = function () {
        var $form = $(this);
        var fields = $form.find('[disabled]');
        fields.prop('disabled', false);
        var json = $form.serializeJSON();
        fields.prop('disabled', true);
        return json;
    };

    //Configure blockUI
    if ($.blockUI) {
        $.blockUI.defaults.baseZ = 2000;
    }


    var $loginForm = $('#LoginForm');

    if ($loginForm.length > 0) {
        $loginForm.submit(function (e) {
            e.preventDefault();

            if (!$loginForm.valid()) {
                return;
            }

            abp.ui.setBusy('.body');
            abp.ajax({
                contentType: 'application/x-www-form-urlencoded',
                url: $loginForm.attr('action'),
                data: $loginForm.serialize()
            }).then(function () {
                console.log("Inicio OK")
            }, function () {
                abp.ui.clearBusy('.body');
            });

        });

        $('a.social-login-link').click(function () {
            var $a = $(this);
            var $form = $a.closest('form');
            $form.find('input[name=provider]').val($a.attr('data-provider'));
            $form.submit();
        });

        $('input[name=returnUrlHash]').val(location.hash);
        $('#LoginForm input:first-child').focus();
    }


})(jQuery);