﻿using Owin.ServiceWorker;
using System.Web;
using System.Web.Mvc;

namespace Personeria.Web.Extensions
{
    public static class ServiceWorkerHtmlHelperExtensions
    {
        public static HtmlString RegisterServiceWorker(this HtmlHelper html) =>
            new HtmlString(HttpContext.Current.GetOwinContext().ServiceWorkerRegistration().RenderScriptTag());
    }
}